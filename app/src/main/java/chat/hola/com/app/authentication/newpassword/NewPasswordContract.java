package chat.hola.com.app.authentication.newpassword;

import java.util.Map;

public interface NewPasswordContract {

    interface View {
        void message(String message);

        void success();
    }

    interface Presenter {

        void changePassword(Map<String, String> params);


        void updatePassword(String password);
    }
}
