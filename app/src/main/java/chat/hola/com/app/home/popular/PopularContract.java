package chat.hola.com.app.home.popular;

import java.util.ArrayList;
import java.util.List;

import chat.hola.com.app.Utilities.BasePresenter;
import chat.hola.com.app.Utilities.BaseView;
import chat.hola.com.app.home.contact.Friend;
import chat.hola.com.app.home.model.Data;
import chat.hola.com.app.models.PostUpdateData;

/**
 * <h1>{@link PopularContract}</h1>
 */

public interface PopularContract {

  interface View extends BaseView {

    /**
     * <p>dismisses the dialog</p>
     */
    void dismissDialog();

    /**
     * <p>add reports to the array list</p>
     *
     * @param data : list of report
     */
    void addToReportList(ArrayList<String> data);

    /**
     * <p>sets details to the screen</p>
     *
     * @param data : details
     */
    void setData(Data data);

    void liked(boolean b, boolean hasError, String postId);

    void deleted(String postId);

    void followers(List<Friend> data);

    void setDataList(List<Data> data, boolean entirelyNewList, boolean refreshRequest);

    /*Used to show progress while background data operation*/
    void showProgress(boolean show);

    /*To show empty UI*/
    void showEmpty(boolean show);

    void bookMarkPostResponse(int pos, boolean bookMarked);

    void collectionCreated();

    void postAddedToCollection();

    /**
     * <p>Used to update saved status of post from subscribe observer</p>
     *
     * @param savedData*/
      void updateSavedOnObserve(PostUpdateData savedData);
  }

  interface Presenter extends BasePresenter<View> {

    /**
     * <p>gets details by postId </p>
     *
     * @param postId :posts's Id
     * @param isJustForView : says its just to for view the post
     */
    void getPostById(String postId, boolean isJustForView);

    /**
     * <p>get list of report reasons</p>
     */
    void getReportReasons();

    void reportPost(String postId, String reason, String message);

    void deletePost(String postId);

    void unlike(String postId);

    void like(String postId);

    void follow(String followingId);

    void unfollow(String followingId);

    void saveToBookmark(int position, String postId);

    void deleteToBookmark(int position, String postId);

    void createCollection(String trim, String collectionImage, String addToCollectionPostId);

    void addPostToCollection(String id, String id1);

    void getCollections();
  }
}
