package chat.hola.com.app.dagger;

import android.app.Application;
import android.content.Context;

import dagger.Binds;
import dagger.Module;

/**
 * Created by ankit on 20/2/18.
 */

//@Singleton
@Module
public interface AppModule {

    @Binds
    Context bindContext(Application application);
}
