package chat.hola.com.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Error implements Serializable {

    @SerializedName(value = "statusCode", alternate = "status")
    @Expose
    int statusCode;

    @SerializedName("following")
    @Expose
    boolean following;

    @SerializedName("isPrivate")
    @Expose
    boolean isPrivate;

    @SerializedName("message")
    @Expose
    String message;

    @SerializedName("verificationStatus")
    @Expose
    Integer verificationStatus;

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public Integer getVerificationStatus() {
        return verificationStatus;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public void setPrivate(boolean aPrivate) {
        isPrivate = aPrivate;
    }

    public boolean isFollowing() {
        return following;
    }

    public void setFollowing(boolean following) {
        this.following = following;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
