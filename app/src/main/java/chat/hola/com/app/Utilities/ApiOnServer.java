package chat.hola.com.app.Utilities;

import com.lighthusky.dingdong.BuildConfig;

/**
 * <h1>ApiOnServer</h1>
 *
 * <p>Contains server folder names, posts, ip address and constants</p>
 *
 * @author Shaktisinh Jadeja
 * @version 1.0
 * @since 20/06/17.
 */
public class ApiOnServer {
    //folders
    public static final String APP_NAME = "dubly";
    public static final String CHAT_RECEIVED_THUMBNAILS_FOLDER = "/" + APP_NAME + "/receivedThumbnails";
    public static final String CHAT_UPLOAD_THUMBNAILS_FOLDER = "/" + APP_NAME + "/upload";
    public static final String IMAGE_CAPTURE_URI = "/" + APP_NAME;

    public static final String HOST_API = BuildConfig.HOST_API;
    public static final String TRENDING_HOST_API = BuildConfig.TRENDING_HOST_API;
    public static final String TYPE = "https://";

    public static final boolean ALLOW_DEFAULT_OTP = true;


    // sup specific apis
    public static final String SUP_API_MAIN_LINK = TYPE + HOST_API;
    public static final String FRIEND_API = SUP_API_MAIN_LINK + "friend";
    public static final String VERIFY_OTP_EMAIL = SUP_API_MAIN_LINK + "verifyOTPByEmail";
    public static final String REQUEST_OTP = SUP_API_MAIN_LINK + "RequestOTP";
    public static final String VERIFY_REFERRAL_CODE = SUP_API_MAIN_LINK + "referralCode";
    public static final String VERIFY_OTP = SUP_API_MAIN_LINK + "verifyOtp";

    public static final String REQUEST_OTP_EMAIL = SUP_API_MAIN_LINK + "requestEmailVerification";
    public static final String OTHER_USER_PROFILE = SUP_API_MAIN_LINK + "profile/member"/*"User/Profile"*/;
    public static final String NEW_TOKEN = SUP_API_MAIN_LINK + "accessToken";
    public static final String WALLET_CHECK = SUP_API_MAIN_LINK + "wallet/check";

}
