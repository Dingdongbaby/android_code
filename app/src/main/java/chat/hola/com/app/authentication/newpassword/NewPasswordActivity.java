package chat.hola.com.app.authentication.newpassword;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.lighthusky.dingdong.R;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import androidx.annotation.Nullable;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import chat.hola.com.app.Utilities.TypefaceManager;
import chat.hola.com.app.authentication.login.LoginActivity;
import dagger.android.support.DaggerAppCompatActivity;

public class NewPasswordActivity extends DaggerAppCompatActivity implements NewPasswordContract.View {


    @Inject
    TypefaceManager typefaceManager;
    @Inject
    NewPasswordContract.Presenter presenter;

    @BindView(R.id.etNewPassword)
    EditText etNewPassword;
    @BindView(R.id.etConfirmPassword)
    EditText etConfirmPassword;
    private InputMethodManager imm;
    private String call, email = "", countryCode, phoneNumber, password, userName;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_password_activity);
        ButterKnife.bind(this);
        imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        call = getIntent().getStringExtra("call");
        email = getIntent().getStringExtra("email");
        userName = getIntent().getStringExtra("userName");
        countryCode = getIntent().getStringExtra("countryCode");
        phoneNumber = getIntent().getStringExtra("phoneNumber");

        etNewPassword.setTypeface(typefaceManager.getRegularFont());
        etConfirmPassword.setTypeface(typefaceManager.getRegularFont());
        showKeyboard();
    }

    @OnClick(R.id.iV_back)
    public void back() {
        hideKeyBoard();
        super.onBackPressed();
    }

    @OnClick(R.id.rL_next)
    public void resend() {

        if (validate()) {
            Map<String, String> params = new HashMap<>();
            params.put("password", password);
            params.put("confirmPassword", password);
            if (userName != null && !userName.isEmpty()) {
                params.put("userName", userName);
                presenter.changePassword(params);
            } else if (email != null && !email.isEmpty()) {
                params.put("email", email);
                presenter.changePassword(params);
            } else if (phoneNumber != null && !phoneNumber.isEmpty()) {
                params.put("countryCode", countryCode);
                params.put("phoneNumber", phoneNumber);
                presenter.changePassword(params);
            } else {
                presenter.updatePassword(password);
            }
        }
    }

    private boolean validate() {
        String newPwd = etNewPassword.getText().toString();
        String cnfrmPwd = etConfirmPassword.getText().toString();
        if (newPwd.isEmpty()) {
            message("Please enter new password");
            return false;
        }

        if (newPwd.length() < 6) {
            message("Please enter at least 6 letters of password");
            return false;
        }

        if (!newPwd.equals(cnfrmPwd)) {
            message("The confirm password does not match the password set");
            return false;
        }

        password = newPwd;
        return true;
    }

    @Override
    public void message(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void success() {
        hideKeyBoard();
//        if (email != null && !email.isEmpty()) {
//        message("Password changed successfully, please login again");
        startActivity(new Intent(this, LoginActivity.class).putExtra("call", call));
        finish();
//        } else {

//        }
    }

    private void showKeyboard() {
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                etNewPassword.requestFocus();
                if (imm != null) {
                    imm.showSoftInput(etNewPassword, InputMethodManager.SHOW_FORCED);
                }
            }
        }, 200);
    }

    private void hideKeyBoard() {
        if (imm != null) {
            imm.hideSoftInputFromWindow(etNewPassword.getWindowToken(), 0);
            imm.hideSoftInputFromWindow(etConfirmPassword.getWindowToken(), 0);
        }
    }
}
