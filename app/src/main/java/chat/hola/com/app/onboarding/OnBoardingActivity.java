package chat.hola.com.app.onboarding;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;

import com.lighthusky.dingdong.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import chat.hola.com.app.Utilities.TypefaceManager;
import chat.hola.com.app.profileScreen.discover.DiscoverActivity;

public class OnBoardingActivity extends AppCompatActivity implements ClickListner {

    private ViewPager viewPager;
    private TabLayout indicator;

    private List<Integer> color;
    private List<Integer> image;
    private List<String> title;
    private List<String> description;
    private TypefaceManager typefaceManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_onboard);
        typefaceManager = new TypefaceManager(this);

        viewPager = findViewById(R.id.viewPager);
        indicator = findViewById(R.id.indicator);

        color = new ArrayList<>();
        color.add(R.color.onboarding_color1);
        color.add(R.color.onboarding_color2);

        image = new ArrayList<>();
        image.add(R.drawable.onboarding1);
        image.add(R.drawable.onboarding2);

        title = new ArrayList<>();
        title.add(getString(R.string.onboarding_title1));
        title.add(getString(R.string.onboarding_title2));

        description = new ArrayList<>();
        description.add(getString(R.string.onboarding_description1));
        description.add(getString(R.string.onboarding_description2));

        viewPager.setAdapter(new SliderAdapter(this, color, image, title, description, typefaceManager, this,true));
        indicator.setupWithViewPager(viewPager, true);

        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new SliderTimer(), 4000, 6000);
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public void next(boolean isLast) {
        startActivity(new Intent(this, DiscoverActivity.class).putExtra("caller", "SaveProfile"));
        finish();
    }

    private class SliderTimer extends TimerTask {

        @Override
        public void run() {
            OnBoardingActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (viewPager.getCurrentItem() < color.size() - 1) {
                        viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
                    } else {
                        viewPager.setCurrentItem(0);
                    }
                }
            });
        }
    }
}
