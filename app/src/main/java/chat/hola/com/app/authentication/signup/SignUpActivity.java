package chat.hola.com.app.authentication.signup;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.InputFilter;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.LinkMovementMethod;
import android.text.method.PasswordTransformationMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import chat.hola.com.app.AppController;
import chat.hola.com.app.Database.CouchDbController;
import chat.hola.com.app.Dialog.ImageSourcePicker;
import chat.hola.com.app.NumberVerification.ChooseCountry;
import chat.hola.com.app.Utilities.Constants;
import chat.hola.com.app.Utilities.CountryCodeUtil;
import chat.hola.com.app.Utilities.ImageFilePath;
import chat.hola.com.app.Utilities.Loader;
import chat.hola.com.app.Utilities.RoundedImageView;
import chat.hola.com.app.Utilities.TypefaceManager;
import chat.hola.com.app.Utilities.UploadFileAmazonS3;
import chat.hola.com.app.Utilities.Utilities;
import chat.hola.com.app.authentication.login.LoginActivity;
import chat.hola.com.app.manager.session.SessionManager;
import chat.hola.com.app.models.Login;
import chat.hola.com.app.models.Register;
import chat.hola.com.app.profileScreen.discover.DiscoverActivity;
import chat.hola.com.app.user_verification.phone.VerifyNumberOTPActivity;
import com.lighthusky.dingdong.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;
import com.google.firebase.messaging.FirebaseMessaging;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import dagger.android.support.DaggerAppCompatActivity;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.inject.Inject;
import org.json.JSONArray;
import org.json.JSONException;

public class SignUpActivity extends DaggerAppCompatActivity implements SignUpContract.View {

  public static final int REQUEST_CODE = 123;
  private static final int RESULT_LOAD_PROFILE_IMAGE = 555;
  private static final int RESULT_CAPTURE_PROFILE_IMAGE = 444;
  private static final int FACEBOOK_APP_REQUEST_CODE = 61351;

  Register register;
  @Inject
  TypefaceManager typefaceManager;
  @Inject
  SessionManager sessionManager;
  @Inject
  SignUpContract.Presenter presenter;
  @Inject
  ImageSourcePicker imageSourcePicker;

  @BindView(R.id.ivBack)
  ImageView ivBack;
  @BindView(R.id.ibAdd)
  ImageButton ibAdd;
  @BindView(R.id.ivProfilePic)
  RoundedImageView ivProfilePic;
  @BindView(R.id.textRegion)
  TextView textRegion;
  @BindView(R.id.tvRegion)
  TextView tvRegion;
  @BindView(R.id.textPhone)
  TextView textPhone;
  @BindView(R.id.textUserName)
  TextView textUserName;
  @BindView(R.id.textPassword)
  TextView textPassword;
  @BindView(R.id.textConfirmPassword)
  TextView textConfirmPassword;
  @BindView(R.id.textReferralCode)
  TextView textReferralCode;
  @BindView(R.id.etPhone)
  EditText etPhone;
  @BindView(R.id.etName)
  EditText etName;
  @BindView(R.id.etUserName)
  EditText etUserName;
  @BindView(R.id.etPassword)
  EditText etPassword;
  @BindView(R.id.etConfirmPassword)
  EditText etConfirmPassword;
  @BindView(R.id.etReferralCode)
  EditText etReferralCode;
  @BindView(R.id.etEmail)
  EditText etEmail;
  @BindView(R.id.tvAlreadyMember)
  TextView tvAlreadyMember;
  @BindView(R.id.tvSignIn)
  TextView tvSignIn;
  @BindView(R.id.tvTitle)
  TextView tvTitle;
  @BindView(R.id.textName)
  TextView textName;
  @BindView(R.id.textEmail)
  TextView textEmail;
  @BindView(R.id.isPrivate)
  Switch isPrivate;
  @BindView(R.id.done)
  Button done;
  @BindView(R.id.tbPassword)
  ToggleButton tbPassword;
  @BindView(R.id.tbConfirmPassword)
  ToggleButton tbConfirmPassword;
  @BindView(R.id.cbTerms)
  CheckBox cbTerms;
  @BindView(R.id.root)
  RelativeLayout root;
  @BindView(R.id.iV_emailClear)
  ImageView iV_emailClear;

  protected String countryCode;
  private String profilePic;
  private String userName;
  private String phoneNumber;
  private String referralCode;
  private String confirmPassword;
  private String firstName;
  private String lastName;
  private String emailAddress;
  protected int max_digits = 20;
  private UploadFileAmazonS3 uploadFileAmazonS3;
  private Loader loader;
  private boolean uploadProfilePic = false;

  @SuppressLint("SetTextI18n")
  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_signup);
    ButterKnife.bind(this);

    uploadFileAmazonS3 = new UploadFileAmazonS3(this);
    register = new Register();

    String cCode = getIntent().getStringExtra("countryCode");
    String cName = getIntent().getStringExtra("country");
    String phone = getIntent().getStringExtra("phoneNumber");

    etUserName.setFilters(new InputFilter[] { ignoreFirstWhiteSpace() });

    SpannableString ss = new SpannableString(cbTerms.getText().toString());
    ClickableSpan clickableSpan = new ClickableSpan() {
      @Override
      public void onClick(View textView) {
        String url = getString(R.string.termsUrl);
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
      }

      @Override
      public void updateDrawState(TextPaint ds) {
        super.updateDrawState(ds);
        ds.setColor(getResources().getColor(R.color.color_black));
        ds.setUnderlineText(true);
        ds.setFakeBoldText(true);
      }
    };
    ss.setSpan(clickableSpan, 20, cbTerms.getText().toString().length(),
        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

    cbTerms.setText(ss);
    cbTerms.setMovementMethod(LinkMovementMethod.getInstance());
    cbTerms.setHighlightColor(Color.BLUE);

    if (cCode != null && !cCode.isEmpty() && cName != null && !cName.isEmpty()) {
      countryCode = cCode;
      tvRegion.setText(cName + " (" + countryCode + ")");
    } else {
      loadCurrentCountryCode();
    }

    if (phone != null && !phone.isEmpty()) {
      etPhone.setText(phone);
    }

    tbPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        etPassword.setTransformationMethod(!isChecked ? PasswordTransformationMethod.getInstance()
            : HideReturnsTransformationMethod.getInstance());
        etPassword.setSelection(etPassword.getText().toString().length());
      }
    });

    tbConfirmPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        etConfirmPassword.setTransformationMethod(
            !isChecked ? PasswordTransformationMethod.getInstance()
                : HideReturnsTransformationMethod.getInstance());
        etConfirmPassword.setSelection(etConfirmPassword.getText().toString().length());
      }
    });

    isPrivate.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        register.setPrivate(isChecked);
        isPrivate.setText(isChecked ? "Private" : "Public");
      }
    });

    applyFont();

    loader = new Loader(this);

    FirebaseDynamicLinks.getInstance()
        .getDynamicLink(getIntent())
        .addOnSuccessListener(this, new OnSuccessListener<PendingDynamicLinkData>() {
          @Override
          public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
            // Get deep link from result (may be null if no link is found)
            Uri deepLink = null;
            if (pendingDynamicLinkData != null) {
              deepLink = pendingDynamicLinkData.getLink();
              String referralCode = deepLink.getLastPathSegment();
              etReferralCode.setText(referralCode);
            }
          }
        })
        .addOnFailureListener(this, new OnFailureListener() {
          @Override
          public void onFailure(@NonNull Exception e) {
            // Log.w(TAG, "getDynamicLink:onFailure", e);
          }
        });
  }

  private InputFilter ignoreFirstWhiteSpace() {
    return (source, start, end, dest, dstart, dend) -> {
      for (int i = start; i < end; i++) {
        if (Character.isWhitespace(source.charAt(i))) {
          if (dstart == 0) return "";
        }
      }
      return null;
    };
  }

  ImageSourcePicker.OnSelectImageSource callback = new ImageSourcePicker.OnSelectImageSource() {
    @Override
    public void onCamera() {
      Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
      startActivityForResult(cameraIntent, RESULT_CAPTURE_PROFILE_IMAGE);
    }

    @Override
    public void onGallary() {
      Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
      intent.addCategory(Intent.CATEGORY_DEFAULT);
      intent.setType("image/*");
      startActivityForResult(intent, RESULT_LOAD_PROFILE_IMAGE);
    }

    @Override
    public void onCancel() {
      //nothing to do.
    }
  };

  private void loadCurrentCountryCode() {
    String locale;
    TelephonyManager tm = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
    String countryCodeValue = tm.getNetworkCountryIso();
    if (countryCodeValue == null || countryCodeValue.isEmpty()) {
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        locale = getResources().getConfiguration().getLocales().get(0).getCountry().toUpperCase();
      } else {
        locale = getResources().getConfiguration().locale.getCountry().toUpperCase();
      }
    } else {
      locale = countryCodeValue.toUpperCase();
    }

    try {
      String allCountriesCode = CountryCodeUtil.readEncodedJsonString(this);
      JSONArray countryArray = new JSONArray(allCountriesCode);
      for (int i = 0; i < countryArray.length(); i++) {
        if (locale.equals(countryArray.getJSONObject(i).getString("code"))) {
          countryCode = countryArray.getJSONObject(i).getString("dial_code");
          String country = countryArray.getJSONObject(i).getString("name");
          //                    max_digits = countryArray.getJSONObject(i).getInt("max_digits");
          etPhone.setText("");
          etPhone.setFilters(new InputFilter[] { new InputFilter.LengthFilter(max_digits) });
          tvRegion.setText(country + " (" + countryCode + ")");
          return;
        }
      }
    } catch (IOException | JSONException e) {
      e.printStackTrace();
    }
  }

  private Uri getImageUri(Bitmap photo) {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    photo.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
    String path = MediaStore.Images.Media.insertImage(getContentResolver(), photo, "Title", null);
    return Uri.parse(path);
  }

  public String getPath(Uri uri) {

    String mediaPath;//= ImageFilePath.getPath(this, uri);
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
      return ImageFilePath.getPathAboveN(this, uri);
    } else {

      return ImageFilePath.getPath(this, uri);
    }
  }

  @OnClick(R.id.tvRegion)
  public void selectRegion() {
    Intent intent = new Intent(SignUpActivity.this, ChooseCountry.class);
    startActivityForResult(intent, 0);
  }

  @OnClick(R.id.ibAdd)
  public void add() {
    photoPermission();
  }

  private void photoPermission() {
    Dexter.withActivity(SignUpActivity.this)
        .withPermissions(Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE)
        .withListener(new MultiplePermissionsListener() {
          @Override
          public void onPermissionsChecked(MultiplePermissionsReport report) {
            if (report.areAllPermissionsGranted()) {
              imageSourcePicker.setOnSelectImageSource(callback);
              imageSourcePicker.show();
            } else if (report.isAnyPermissionPermanentlyDenied()) {
              Snackbar snackbar = Snackbar.make(root,
                  "This app needs permission to use this feature. You can grant them in app settings.",
                  Snackbar.LENGTH_INDEFINITE)
                  .setAction("GOTO SETTINGS", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                      openSettings();
                    }
                  });
              snackbar.setActionTextColor(getResources().getColor(R.color.base_color));
              snackbar.show();
            } else {
              Snackbar snackbar = Snackbar.make(root,
                  "Need permission to access your photo please accept the permission",
                  Snackbar.LENGTH_INDEFINITE).setAction("OK", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                  photoPermission();
                }
              });
              snackbar.setActionTextColor(getResources().getColor(R.color.base_color));
              snackbar.show();
            }
          }

          @Override
          public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions,
              PermissionToken token) {
            token.continuePermissionRequest();
          }
        })
        .onSameThread()
        .check();
  }

  private void openSettings() {
    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
    Uri uri = Uri.fromParts("package", getPackageName(), null);
    intent.setData(uri);
    startActivityForResult(intent, 101);
  }

  @OnClick(R.id.tvSignIn)
  public void gotoSignIn() {
    startActivity(new Intent(this, LoginActivity.class));
  }

  @OnClick(R.id.ivBack)
  public void back() {
    super.onBackPressed();
  }

  //apply fonts
  private void applyFont() {
    tvAlreadyMember.setTypeface(typefaceManager.getSemiboldFont());
    tvSignIn.setTypeface(typefaceManager.getSemiboldFont());
    tvTitle.setTypeface(typefaceManager.getSemiboldFont());
    textConfirmPassword.setTypeface(typefaceManager.getSemiboldFont());
    textPassword.setTypeface(typefaceManager.getSemiboldFont());
    textPhone.setTypeface(typefaceManager.getSemiboldFont());
    textReferralCode.setTypeface(typefaceManager.getSemiboldFont());
    textRegion.setTypeface(typefaceManager.getSemiboldFont());
    textUserName.setTypeface(typefaceManager.getSemiboldFont());
    textName.setTypeface(typefaceManager.getSemiboldFont());
    textEmail.setTypeface(typefaceManager.getSemiboldFont());

    etEmail.setTypeface(typefaceManager.getRegularFont());
    etPhone.setTypeface(typefaceManager.getRegularFont());
    etUserName.setTypeface(typefaceManager.getRegularFont());
    etConfirmPassword.setTypeface(typefaceManager.getRegularFont());
    etPassword.setTypeface(typefaceManager.getRegularFont());
    etName.setTypeface(typefaceManager.getRegularFont());
    etReferralCode.setTypeface(typefaceManager.getRegularFont());
    done.setTypeface(typefaceManager.getSemiboldFont());
  }

  @OnClick(R.id.done)
  public void signUp() {
    if (validate()) {
      register.setPrivate(isPrivate.isChecked());
      register.setCountryCode(countryCode);
      register.setFirstName(firstName);
      register.setLastName(lastName);
      register.setUserName(userName);
      register.setMobileNumber(phoneNumber);
      register.setPassword(confirmPassword);
//      register.setUploadProfilePic(uploadProfilePic);
      register.setProfilePic(profilePic);
      register.setReferralCode(referralCode);
      register.setEmail(emailAddress);

      ivBack.setEnabled(false);
      ibAdd.setEnabled(false);
      done.setEnabled(false);
      presenter.validate(register);//, uploadFileAmazonS3, new File(profilePic));
    } else {
      enableSave(true);
    }
  }

  @OnClick({ R.id.iV_emailClear })
  public void emailClear() {
    etEmail.setText(getString(R.string.double_inverted_comma));
    iV_emailClear.setVisibility(View.GONE);
  }

  /**
   * Validates all the required fields
   *
   * @return true if all fields are valid otherwise false
   */
  private boolean validate() {
    String fullName = etName.getText().toString().trim();
    String password = etPassword.getText().toString().trim();

    referralCode = etReferralCode.getText().toString().trim();
    phoneNumber = etPhone.getText().toString().trim();
    confirmPassword = etConfirmPassword.getText().toString().trim();
    userName = etUserName.getText().toString().trim();
    emailAddress = etEmail.getText().toString().trim();

    if (countryCode == null || countryCode.isEmpty()) {
      message("Please select region");
      return false;
    }

    if (fullName.isEmpty()) {
      message("Please enter your name");
      return false;
    }

    if (userName.isEmpty()) {
      message("Please enter user name");
      return false;
    }

    if (phoneNumber.isEmpty()) {
      message("Please enter phone number");
      return false;
    }

    if (phoneNumber.length() > max_digits) {
      message("Please enter valid mobile number");
      return false;
    }

    if (!Utilities.isValidEmail(emailAddress)) {
      message("Please enter valid email address");
      iV_emailClear.setVisibility(View.VISIBLE);
      return false;
    }

    if (password.isEmpty()) {
      message("Please enter password");
      return false;
    }

    if (password.length() < 6) {
      message("Please enter at least 6 letter password");
      return false;
    }

    if (confirmPassword.isEmpty()) {
      message("Please confirm password");
      return false;
    }

    if (!confirmPassword.equals(password)) {
      message("Confirm password must match your password");
      return false;
    }

    if (referralCode.isEmpty()) {
      message("Please enter referral code");
      return false;
    }

    if (profilePic == null) {
      uploadProfilePic = true;
    }

    if (!cbTerms.isChecked()) {
      message("Please accept DingDong Terms of Service and Privacy policy");
      return false;
    }

    String[] fullname = fullName.split("\\s+");
    firstName = fullname[0];
    if (fullname.length > 1 && !fullname[1].isEmpty()) {
      lastName = fullname[1];
    } else {
      lastName = "";
    }

    return true;
  }

  @Override
  public void registered(Login.LoginResponse response) {
    hideLoader();
    //        nextFlow(response);
    enableSave(true);
    Intent intent = new Intent(this, DiscoverActivity.class);
    intent.putExtra("call", "SaveProfile");
    startActivity(intent);
    finish();
  }

  private void nextFlow(Login.LoginResponse response) {
    /*
     *
     * To start the Profile screen
     */
    try {

      //response = response.getJSONObject("response");

      String profilePic = "", userName = "", firstName = "", lastName = "";
      // String token = response.getString("token");

      CouchDbController db = AppController.getInstance().getDbController();

      Map<String, Object> map = new HashMap<>();

      if (response.getProfilePic() != null) {
        profilePic = response.getProfilePic();
        map.put("userImageUrl", response.getProfilePic());
      } else {
        map.put("userImageUrl", "");
      }
      if (response.getUserName() != null) {
        userName = response.getUserName();
        map.put("userName", response.getUserName());
      }

      if (response.getFirstName() != null) {
        firstName = response.getFirstName();
        map.put("firstName", response.getFirstName());
      }

      if (response.getLastName() != null) {
        lastName = response.getLastName();
        map.put("lastName", response.getLastName());
      }

      String userStatus = getString(R.string.default_status);

      map.put("userId", response.getUserId());

      try {
        map.put("private", response.get_private());
      } catch (Exception ignored) {
      }


      /*
       * To save the social status as the text value in the properties
       *
       */

      if (response.getSocialStatus() != null) {

        userStatus = response.getSocialStatus();
      }
      map.put("socialStatus", userStatus);
      map.put("userIdentifier", phoneNumber);
      map.put("apiToken", response.getToken());

      AppController.getInstance()
          .getSharedPreferences()
          .edit()
          .putString("token", response.getToken())
          .apply();

      /*
       * By phone number verification
       */

      map.put("userLoginType", 1);
      map.put("excludedFilterIds", new ArrayList<Integer>());
      if (!db.checkUserDocExists(AppController.getInstance().getIndexDocId(),
          response.getUserId())) {

        String userDocId = db.createUserInformationDocument(map);

        db.addToIndexDocument(AppController.getInstance().getIndexDocId(), response.getUserId(),
            userDocId);
      } else {

        db.updateUserDetails(
            db.getUserDocId(response.getUserId(), AppController.getInstance().getIndexDocId()),
            map);
      }
      if (!userName.isEmpty()) {

        db.updateIndexDocumentOnSignIn(AppController.getInstance().getIndexDocId(),
            response.getUserId(), 1, true);
      } else {
        db.updateIndexDocumentOnSignIn(AppController.getInstance().getIndexDocId(),
            response.getUserId(), 1, false);
      }
      /*
       * To update myself as available for call
       */

      AppController.getInstance().setSignedIn(true, response.getUserId(), userName, phoneNumber, 1);
      AppController.getInstance().setSignStatusChanged(true);

      String topic = "/topics/" + response.getUserId();
      FirebaseMessaging.getInstance().subscribeToTopic(topic);

      SessionManager sessionManager = new SessionManager(this);
      sessionManager.setUserName(userName);
      sessionManager.setFirstName(firstName);
      sessionManager.setLastsName(lastName);
      //            sessionManager.setFacebookAccessToken(AccessToken.getCurrentAccessToken());
      sessionManager.setUserProfilePic(profilePic, true);

      Intent i = new Intent(this, DiscoverActivity.class);
      i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
          | Intent.FLAG_ACTIVITY_CLEAR_TASK
          | Intent.FLAG_ACTIVITY_CLEAR_TOP);
      i.putExtra("caller", "SaveProfile");
      startActivity(i);
      finish();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * Display toast message
   */
  @Override
  public void message(String message) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
  }

  @Override
  public void completed() {
    ivBack.setEnabled(true);
    ibAdd.setEnabled(true);
    done.setEnabled(true);
  }

  @Override
  public void setProfilePic(String url) {
    profilePic = url;
    sessionManager.setUserProfilePic(url, true);
  }

  @Override
  public void enableSave(boolean enable) {
    done.setEnabled(enable);
    done.setVisibility(enable ? View.VISIBLE : View.INVISIBLE);
  }

  @Override
  public void showProgress(boolean show) {
    if (show) {
      loader.show();
    } else {
      loader.dismiss();
    }
  }

  @Override
  public void verifyPhone() {
    Intent intent = new Intent(this, VerifyNumberOTPActivity.class);
    intent.putExtra("phoneNumber", phoneNumber);
    intent.putExtra("countryCode", countryCode);
    intent.putExtra("isFromSignUp", true);
    intent.putExtra("profilePic", profilePic);
    intent.putExtra("register", register);
    startActivity(intent);
    //        startActivityForResult(intent, Constants.Verification.NUMBER_VERIFICATION_REQ);
  }

  @Override
  public void showLoader() {
    loader.show();
  }

  @Override
  public void hideLoader() {
    if (loader != null && loader.isShowing()) loader.hide();
  }

  @SuppressLint("SetTextI18n")
  @Override
  protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    try {
      if (requestCode == 101) {
        photoPermission();
      } else if (requestCode == Constants.Verification.NUMBER_VERIFICATION_REQ) {
        enableSave(true);
        if (data.getBooleanExtra("isVerified", true)) {
          presenter.signUp(register, uploadFileAmazonS3, profilePic);
        }
      } else if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {
        profilePic = data.getStringExtra("profilePic");
        Bitmap bitmap = BitmapFactory.decodeFile(profilePic);
        ivProfilePic.setImageBitmap(bitmap);
      } else if (requestCode == 0 && resultCode == RESULT_OK) {
        String message = data.getStringExtra("MESSAGE");
        String code = data.getStringExtra("CODE");
        countryCode = "+" + code.substring(1);
        //                max_digits = data.getIntExtra("MAX", 10);
        etPhone.setText(getString(R.string.double_inverted_comma));
        etPhone.setFilters(new InputFilter[] { new InputFilter.LengthFilter(max_digits) });
        tvRegion.setText(message + " (" + countryCode + ")");
      } else if (requestCode == RESULT_LOAD_PROFILE_IMAGE) {
        Uri uri = data.getData();
        profilePic = getPath(uri);
        if (profilePic == null ) {

          Toast.makeText(this, getString(R.string.image_selection_failed), Toast.LENGTH_SHORT)
              .show();
        } else {

          Bitmap bitmap = BitmapFactory.decodeFile(profilePic);
          ivProfilePic.setImageBitmap(bitmap);
        }
      } else if (requestCode == RESULT_CAPTURE_PROFILE_IMAGE) {
        try {
          Bitmap photo = (Bitmap) Objects.requireNonNull(data.getExtras()).get("data");
          if (photo != null) {
            ivProfilePic.setImageBitmap(photo);
            Uri uri = getImageUri(photo);
            profilePic = getPath(uri);
            if (profilePic == null ) {

              Toast.makeText(this, getString(R.string.image_selection_failed), Toast.LENGTH_SHORT)
                  .show();
            }
          }
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
