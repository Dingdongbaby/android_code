package chat.hola.com.app.home.popular;

import android.os.Handler;
import android.util.Log;

import com.lighthusky.dingdong.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import chat.hola.com.app.AppController;
import chat.hola.com.app.Networking.HowdooService;
import chat.hola.com.app.Networking.HowdooServiceTrending;
import chat.hola.com.app.Utilities.Constants;
import chat.hola.com.app.Utilities.SessionApiCall;
import chat.hola.com.app.Utilities.Utilities;
import chat.hola.com.app.home.contact.GetFriends;
import chat.hola.com.app.home.model.Posts;
import chat.hola.com.app.models.PostObserver;
import chat.hola.com.app.models.PostUpdateData;
import chat.hola.com.app.models.SessionObserver;
import chat.hola.com.app.post.ReportReason;
import chat.hola.com.app.profileScreen.discover.contact.pojo.FollowResponse;
import chat.hola.com.app.socialDetail.model.PostResponse;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

public class PopularPresenter implements PopularContract.Presenter {

    private static final String TAG = PopularPresenter.class.getSimpleName();
    public static final int PAGE_SIZE = Constants.PAGE_SIZE;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    public int page = 0;
    private SessionApiCall sessionApiCall = new SessionApiCall();

    //@Inject
    PopularContract.View mView;
    @Inject
    HowdooService service;
    @Inject
    HowdooServiceTrending serviceTrending;
    @Inject
    PopularModel model;
    @Inject
    PostObserver postObserver;

    @Inject
    public PopularPresenter() {

    }

    public void subscribeSavedObserver() {
        postObserver.getSavedObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<PostUpdateData>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        System.out.println(TAG+":subscribe:"+d.isDisposed());
                    }

                    @Override
                    public void onNext(PostUpdateData postUpdateData) {
                        if (mView != null && !postUpdateData.getFrom().equals(Constants.PostUpdate.POPULAR_FRAGMENT)) {
                            mView.updateSavedOnObserve(postUpdateData);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        System.out.println(TAG+":"+e.getLocalizedMessage());
                    }

                    @Override
                    public void onComplete() {
                        System.out.println(TAG+"On Complete");
                    }
                });
    }

    @Override
    public void getPostById(String postId, boolean isJustForView) {
        if(AppController.getInstance().isGuest())return;
        Map<String, Object> params = model.getParams();
        service.postDetail(AppController.getInstance().getApiToken(), Constants.LANGUAGE, postId,
                String.valueOf(params.get("city")), String.valueOf(params.get("country")),
                String.valueOf(params.get("ip")),
                Double.parseDouble(String.valueOf(params.get("latitude"))),
                Double.parseDouble(String.valueOf(params.get("longitude"))))

                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<PostResponse>>() {
                    @Override
                    public void onNext(Response<PostResponse> response) {
                        switch (response.code()) {
                            case 200:
                                if (!isJustForView) {
                                    postObserver.postData(true);
                                    if (mView != null) mView.setData(response.body().getData());
                                }
                                break;
                            case 406:
                                SessionObserver sessionObserver = new SessionObserver();
                                sessionObserver.getObservable()
                                        .subscribeOn(Schedulers.io())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(new DisposableObserver<Boolean>() {
                                            @Override
                                            public void onNext(Boolean flag) {
                                                Handler handler = new Handler();
                                                handler.postDelayed(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        getPostById(postId, isJustForView);
                                                    }
                                                }, 1000);
                                            }

                                            @Override
                                            public void onError(Throwable e) {

                                            }

                                            @Override
                                            public void onComplete() {
                                            }
                                        });
                                sessionApiCall.getNewSession(service, sessionObserver);
                                break;
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.i(TAG, "onError: ");
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    @Override
    public void getReportReasons() {
        if(AppController.getInstance().isGuest())return;
        service.getReportReason(AppController.getInstance().getApiToken(), Constants.LANGUAGE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ReportReason>>() {
                    @Override
                    public void onNext(Response<ReportReason> response) {
                        switch (response.code()) {
                            case 200:
                                if (mView != null) mView.addToReportList(response.body().getData());
                                break;
                            case 406:
                                SessionObserver sessionObserver = new SessionObserver();
                                sessionObserver.getObservable()
                                        .subscribeOn(Schedulers.io())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(new DisposableObserver<Boolean>() {
                                            @Override
                                            public void onNext(Boolean flag) {
                                                Handler handler = new Handler();
                                                handler.postDelayed(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        getReportReasons();
                                                    }
                                                }, 1000);
                                            }

                                            @Override
                                            public void onError(Throwable e) {

                                            }

                                            @Override
                                            public void onComplete() {
                                            }
                                        });
                                sessionApiCall.getNewSession(service, sessionObserver);
                                break;
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    @Override
    public void like(String postId) {
        service.like(AppController.getInstance().getApiToken(), Constants.LANGUAGE,
                model.getParams(postId))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> response) {
                        switch (response.code()) {
                            case 200:
                                if (mView != null) mView.liked(true, false, postId);
                                //postObserver.postData(true);
                                postObserver.postLikeUnlikeObject(new PostUpdateData(true, postId));
                                break;
                            case 406:
                                SessionObserver sessionObserver = new SessionObserver();
                                sessionObserver.getObservable()
                                        .subscribeOn(Schedulers.io())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(new DisposableObserver<Boolean>() {
                                            @Override
                                            public void onNext(Boolean flag) {
                                                Handler handler = new Handler();
                                                handler.postDelayed(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        like(postId);
                                                    }
                                                }, 1000);
                                            }

                                            @Override
                                            public void onError(Throwable e) {

                                            }

                                            @Override
                                            public void onComplete() {
                                            }
                                        });
                                sessionApiCall.getNewSession(service, sessionObserver);
                                break;
                            default:
                                if (mView != null) mView.liked(true, true, postId);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (mView != null) mView.liked(true, true, postId);
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    @Override
    public void unlike(String postId) {
        service.unlike(AppController.getInstance().getApiToken(), Constants.LANGUAGE,
                model.getParams1(postId))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> response) {
                        switch (response.code()) {
                            case 200:
                                if (mView != null) mView.liked(false, false, postId);
                                postObserver.postData(true);
                                postObserver.postLikeUnlikeObject(new PostUpdateData(false, postId));
                                break;
                            case 406:
                                SessionObserver sessionObserver = new SessionObserver();
                                sessionObserver.getObservable()
                                        .subscribeOn(Schedulers.io())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(new DisposableObserver<Boolean>() {
                                            @Override
                                            public void onNext(Boolean flag) {
                                                Handler handler = new Handler();
                                                handler.postDelayed(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        unlike(postId);
                                                    }
                                                }, 1000);
                                            }

                                            @Override
                                            public void onError(Throwable e) {

                                            }

                                            @Override
                                            public void onComplete() {
                                            }
                                        });
                                sessionApiCall.getNewSession(service, sessionObserver);
                                break;
                            default:
                                if (mView != null) mView.liked(false, true, postId);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (mView != null) mView.liked(false, true, postId);
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    @Override
    public void deletePost(String postId) {
        service.deletePost(AppController.getInstance().getApiToken(), Constants.LANGUAGE, postId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> response) {
                        switch (response.code()) {
                            case 200:
                                if (mView != null) {
                                    mView.deleted(postId);
                                    mView.showMessage(null, R.string.deleted);
                                }
                                postObserver.postData(true);
                                break;
                            case 406:
                                SessionObserver sessionObserver = new SessionObserver();
                                sessionObserver.getObservable()
                                        .subscribeOn(Schedulers.io())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(new DisposableObserver<Boolean>() {
                                            @Override
                                            public void onNext(Boolean flag) {
                                                Handler handler = new Handler();
                                                handler.postDelayed(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        deletePost(postId);
                                                    }
                                                }, 1000);
                                            }

                                            @Override
                                            public void onError(Throwable e) {

                                            }

                                            @Override
                                            public void onComplete() {
                                            }
                                        });
                                sessionApiCall.getNewSession(service, sessionObserver);
                                break;
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    @Override
    public void reportPost(String postId, String reason, String message) {
        service.reportPost(AppController.getInstance().getApiToken(), Constants.LANGUAGE,
                model.getReasonParams(postId, reason, message))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {

                    @Override
                    public void onNext(Response<ResponseBody> response) {
                        switch (response.code()) {
                            case 200:
                                if (mView != null) mView.showMessage(null, R.string.reported_post);
                                break;
                            case 406:
                                SessionObserver sessionObserver = new SessionObserver();
                                sessionObserver.getObservable()
                                        .subscribeOn(Schedulers.io())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(new DisposableObserver<Boolean>() {
                                            @Override
                                            public void onNext(Boolean flag) {
                                                Handler handler = new Handler();
                                                handler.postDelayed(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        reportPost(postId, reason, message);
                                                    }
                                                }, 1000);
                                            }

                                            @Override
                                            public void onError(Throwable e) {

                                            }

                                            @Override
                                            public void onComplete() {
                                            }
                                        });
                                sessionApiCall.getNewSession(service, sessionObserver);
                                break;
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    @Override
    public void follow(String followingId) {
        Map<String, Object> map = new HashMap<>();
        map.put("followingId", followingId);
        service.follow(AppController.getInstance().getApiToken(), Constants.LANGUAGE, map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<FollowResponse>>() {
                    @Override
                    public void onNext(Response<FollowResponse> response) {
                        switch (response.code()) {
                            case 200:
                                postObserver.postData(true);
                                break;
                            case 406:
                                SessionObserver sessionObserver = new SessionObserver();
                                sessionObserver.getObservable()
                                        .subscribeOn(Schedulers.io())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(new DisposableObserver<Boolean>() {
                                            @Override
                                            public void onNext(Boolean flag) {
                                                Handler handler = new Handler();
                                                handler.postDelayed(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        follow(followingId);
                                                    }
                                                }, 1000);
                                            }

                                            @Override
                                            public void onError(Throwable e) {

                                            }

                                            @Override
                                            public void onComplete() {
                                            }
                                        });
                                sessionApiCall.getNewSession(service, sessionObserver);
                                break;
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void unfollow(String followingId) {
        Map<String, Object> map = new HashMap<>();
        map.put("followingId", followingId);
        service.unfollow(AppController.getInstance().getApiToken(), Constants.LANGUAGE, map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<FollowResponse>>() {
                    @Override
                    public void onNext(Response<FollowResponse> response) {
                        switch (response.code()) {
                            case 200:
                                postObserver.postData(true);
                                break;
                            case 406:
                                SessionObserver sessionObserver = new SessionObserver();
                                sessionObserver.getObservable()
                                        .subscribeOn(Schedulers.io())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(new DisposableObserver<Boolean>() {
                                            @Override
                                            public void onNext(Boolean flag) {
                                                Handler handler = new Handler();
                                                handler.postDelayed(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        unfollow(followingId);
                                                    }
                                                }, 1000);
                                            }

                                            @Override
                                            public void onError(Throwable e) {

                                            }

                                            @Override
                                            public void onComplete() {
                                            }
                                        });
                                sessionApiCall.getNewSession(service, sessionObserver);
                                break;
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    @Override
    public void saveToBookmark(int pos, String postId) {
        if (mView != null) mView.showProgress(true);
        Map<String, Object> params = new HashMap<>();
        params.put("postId", postId);
        service.postBookmark(AppController.getInstance().getApiToken(), Constants.LANGUAGE, params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> response) {
                        if (mView != null) mView.showProgress(false);
                        switch (response.code()) {
                            case 200:
                                assert mView != null;
                                mView.bookMarkPostResponse(pos, true);
                                postObserver.postSavedUpdate(new PostUpdateData(true, postId, Constants.PostUpdate.POPULAR_FRAGMENT));
                                break;
                            case 406:
                                SessionObserver sessionObserver = new SessionObserver();
                                sessionObserver.getObservable()
                                        .subscribeOn(Schedulers.io())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(new DisposableObserver<Boolean>() {
                                            @Override
                                            public void onNext(Boolean flag) {
                                                Handler handler = new Handler();
                                                handler.postDelayed(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        saveToBookmark(pos, postId);
                                                    }
                                                }, 1000);
                                            }

                                            @Override
                                            public void onError(Throwable e) {
                                                if (mView != null) mView.showProgress(false);
                                            }

                                            @Override
                                            public void onComplete() {
                                                if (mView != null) mView.showProgress(false);
                                            }
                                        });
                                sessionApiCall.getNewSession(service, sessionObserver);
                                break;
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (mView != null) mView.showProgress(false);
                    }

                    @Override
                    public void onComplete() {
                        if (mView != null) mView.showProgress(false);
                    }
                });
    }

    @Override
    public void deleteToBookmark(int pos, String postId) {
        if (mView != null) mView.showProgress(true);
        service.deleteToBookmark(AppController.getInstance().getApiToken(), Constants.LANGUAGE, postId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> response) {
                        if (mView != null) mView.showProgress(false);
                        switch (response.code()) {
                            case 200:
                                assert mView != null;
                                mView.bookMarkPostResponse(pos, false);
                                postObserver.postSavedUpdate(new PostUpdateData(false, postId, Constants.PostUpdate.POPULAR_FRAGMENT));
                                break;
                            case 406:
                                SessionObserver sessionObserver = new SessionObserver();
                                sessionObserver.getObservable()
                                        .subscribeOn(Schedulers.io())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(new DisposableObserver<Boolean>() {
                                            @Override
                                            public void onNext(Boolean flag) {
                                                Handler handler = new Handler();
                                                handler.postDelayed(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        deleteToBookmark(pos, postId);
                                                    }
                                                }, 1000);
                                            }

                                            @Override
                                            public void onError(Throwable e) {
                                                if (mView != null) mView.showProgress(false);
                                            }

                                            @Override
                                            public void onComplete() {
                                                if (mView != null) mView.showProgress(false);
                                            }
                                        });
                                sessionApiCall.getNewSession(service, sessionObserver);
                                break;
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (mView != null) mView.showProgress(false);
                    }

                    @Override
                    public void onComplete() {
                        if (mView != null) mView.showProgress(false);
                    }
                });
    }

    @Override
    public void createCollection(String collectionName, String collectionImage, String postId) {

    }

    @Override
    public void addPostToCollection(String collectionId, String postId) {
        if (mView != null) mView.showProgress(true);
        Map<String, Object> map = new HashMap<>();
        ArrayList<String> postIds = new ArrayList<>();
        postIds.add(postId);
        map.put("postIds", postIds);
        map.put("collectionId", collectionId);

    }

    @Override
    public void getCollections() {
        if(AppController.getInstance().isGuest())return;
    }

    public void getFollowUsers() {
        if(AppController.getInstance().isGuest())return;
        service.getFollowersFollowee(AppController.getInstance().getApiToken(), Constants.LANGUAGE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<GetFriends>>() {
                    @Override
                    public void onNext(Response<GetFriends> response) {
                        switch (response.code()) {
                            case 200:
                                if (mView != null) mView.followers(response.body().getData());
                                break;
                            case 406:
                                SessionObserver sessionObserver = new SessionObserver();
                                sessionObserver.getObservable()
                                        .subscribeOn(Schedulers.io())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(new DisposableObserver<Boolean>() {
                                            @Override
                                            public void onNext(Boolean flag) {
                                                Handler handler = new Handler();
                                                handler.postDelayed(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        getFollowUsers();
                                                    }
                                                }, 1000);
                                            }

                                            @Override
                                            public void onError(Throwable e) {

                                            }

                                            @Override
                                            public void onComplete() {
                                            }
                                        });
                                sessionApiCall.getNewSession(service, sessionObserver);
                                break;
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    public void callSocialApi(int offset, int limit, boolean entirelyNewList,
                              boolean refreshRequest) {

        if (entirelyNewList) {
            page = 0;
        }
        if (mView != null) {


            if (refreshRequest || entirelyNewList)
                mView.showProgress(true);
        }

        isLoading = true;
        if (!AppController.getInstance().isGuest()) {
            serviceTrending.getPopularPosts(AppController.getInstance().getApiToken(), Constants.LANGUAGE, offset, limit)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new DisposableObserver<Response<Posts>>() {
                        @Override
                        public void onNext(Response<Posts> response) {
                            isLoading = false;
                            if (mView != null) mView.showProgress(false);
                            switch (response.code()) {
                                case 200:
                                    if (!refreshRequest) {
                                        assert response.body() != null;
                                        isLastPage = response.body().getData().size() < PAGE_SIZE;
                                    }

                                    if (mView != null) {
                                        mView.setDataList(response.body().getData(), entirelyNewList, refreshRequest);
                                    }
                                    break;
                                case 204:
                                    isLastPage = true;
                                    if (entirelyNewList) {
                                        assert mView != null;
                                        mView.showEmpty(true);
                                    }
                                    break;
                                case 406:
                                    SessionObserver sessionObserver = new SessionObserver();
                                    sessionObserver.getObservable()
                                            .subscribeOn(Schedulers.io())
                                            .observeOn(AndroidSchedulers.mainThread())
                                            .subscribe(new DisposableObserver<Boolean>() {
                                                @Override
                                                public void onNext(Boolean flag) {
                                                    Handler handler = new Handler();
                                                    handler.postDelayed(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            callSocialApi(offset, limit, entirelyNewList, refreshRequest);
                                                        }
                                                    }, 1000);
                                                }

                                                @Override
                                                public void onError(Throwable e) {

                                                }

                                                @Override
                                                public void onComplete() {
                                                }
                                            });
                                    sessionApiCall.getNewSession(service, sessionObserver);
                                    break;
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            isLoading = false;
                            if (mView != null) mView.showProgress(false);
                        }

                        @Override
                        public void onComplete() {
                            isLoading = false;
                            if (mView != null) mView.showProgress(false);
                        }
                    });

        } else {
            service.getGuestPosts(Utilities.getThings(), Constants.LANGUAGE, offset, limit)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new DisposableObserver<Response<Posts>>() {
                        @Override
                        public void onNext(Response<Posts> response) {
                            isLoading = false;
                            if (mView != null) mView.showProgress(false);
                            switch (response.code()) {
                                case 200:
                                    if (!refreshRequest) {
                                        assert response.body() != null;
                                        isLastPage = response.body().getData().size() < PAGE_SIZE;
                                    }

                                    if (mView != null) {
                                        mView.setDataList(response.body().getData(), entirelyNewList, refreshRequest);
                                    }
                                    break;
                                case 204:
                                    isLastPage = true;
                                    if (entirelyNewList) {
                                        assert mView != null;
                                        mView.showEmpty(true);
                                    }
                                    break;
                                case 406:
                                    SessionObserver sessionObserver = new SessionObserver();
                                    sessionObserver.getObservable()
                                            .subscribeOn(Schedulers.io())
                                            .observeOn(AndroidSchedulers.mainThread())
                                            .subscribe(new DisposableObserver<Boolean>() {
                                                @Override
                                                public void onNext(Boolean flag) {
                                                    Handler handler = new Handler();
                                                    handler.postDelayed(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            callSocialApi(offset, limit, entirelyNewList, refreshRequest);
                                                        }
                                                    }, 1000);
                                                }

                                                @Override
                                                public void onError(Throwable e) {

                                                }

                                                @Override
                                                public void onComplete() {
                                                }
                                            });
                                    sessionApiCall.getNewSession(service, sessionObserver);
                                    break;
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            isLoading = false;
                            if (mView != null) mView.showProgress(false);
                        }

                        @Override
                        public void onComplete() {
                            isLoading = false;
                            if (mView != null) mView.showProgress(false);
                        }
                    });
        }

    }

    public void callApiOnScroll(int firstVisibleItemPosition, int visibleItemCount,
                                int totalItemCount) {
        if (!isLoading && !isLastPage) {
            if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                    && firstVisibleItemPosition >= 0
                    && totalItemCount >= PAGE_SIZE) {
                page++;
                callSocialApi(PAGE_SIZE * page, PAGE_SIZE, false, false);
            }
        }
    }

    @Override
    public void attachView(PopularContract.View view) {
        this.mView = view;
    }

    @Override
    public void detachView() {
        this.mView = null;
    }
}
