package chat.hola.com.app.authentication.forgotpassword;

import android.os.Handler;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import chat.hola.com.app.Networking.HowdooService;
import chat.hola.com.app.Utilities.SessionApiCall;
import chat.hola.com.app.Utilities.Utilities;
import chat.hola.com.app.models.Error;
import chat.hola.com.app.models.SessionObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

public class ForgotPasswordPresenter implements ForgotPasswordContract.Presenter {

    private SessionApiCall sessionApiCall = new SessionApiCall();

    @Inject
    HowdooService service;
    @Inject
    ForgotPasswordContract.View view;

    @Inject
    ForgotPasswordPresenter() {
    }


    @Override
    public void forgotPassword(String userName, String phone, String email) {
        view.progress(true);
        Map<String, String> params = new HashMap<>();
        params.put("email", email);
        service.forgotPassword(Utilities.getThings(), params).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<Error>>() {
                    @Override
                    public void onNext(Response<Error> response) {
                        switch (response.code()) {
                            case 200:
                                view.success();
                                break;
                            case 406:
                                break;
                            default:
                                view.message(chat.hola.com.app.Utilities.Error.getErrorMessage(response.errorBody()));
                                break;
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                        view.progress(false);
                    }
                });
    }

    @Override
    public void requestOtp(String email) {
        view.progress(true);
        Map<String, Object> params = new HashMap<>();
        params.put("starUserEmailId", email);
        params.put("type", 2);
        service.requestOtpForEmail(Utilities.getThings(), params).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> response) {
                        switch (response.code()) {
                            case 200:
                                view.success();
                                break;
                            case 204:
                                view.message("Your email address is not matching, please check it");
                                break;
                            case 406:
                                break;
                            default:
                                view.message(chat.hola.com.app.Utilities.Error.getErrorMessage(response.errorBody()));
                                break;
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                        view.progress(false);
                    }
                });
    }
}
