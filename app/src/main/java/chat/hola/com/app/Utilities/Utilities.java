package chat.hola.com.app.Utilities;

/**
 * Created by moda on 04/05/17.
 */

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatDrawableManager;
import androidx.core.graphics.drawable.DrawableCompat;

import chat.hola.com.app.AppController;

import com.lighthusky.dingdong.BuildConfig;
import com.lighthusky.dingdong.R;
import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Objects;
import java.util.TimeZone;
import java.util.TreeMap;

import io.card.payment.CardType;
import okhttp3.Credentials;

import org.json.JSONArray;

public class Utilities {
    private static Handler sUiThreadHandler;
    public static DecimalFormat df = new DecimalFormat("#.##");

    /**
     * Run the {@code Runnable} on the UI main thread.
     *
     * @param runnable the runnable
     */
    public static void runOnUiThread(Runnable runnable) {
        if (sUiThreadHandler == null) {
            sUiThreadHandler = new Handler(Looper.getMainLooper());
        }
        sUiThreadHandler.post(runnable);
    }

    public static String tsInGmt() {

        Date localTime =
                new Date(System.currentTimeMillis() - AppController.getInstance().getTimeDelta());

        SimpleDateFormat formater = new SimpleDateFormat("yyyyMMddHHmmssSSS z", Locale.US);
        formater.setTimeZone(TimeZone.getTimeZone("GMT"));

        String s = formater.format(localTime);

        return s;
    }

    //converting time to localtime zone from gmt time

    public static String tsFromGmt(String tsingmt) {

        Date d = null;
        String s = null;

        SimpleDateFormat formater = new SimpleDateFormat("yyyyMMddHHmmssSSS z", Locale.US);

        try {
            d = formater.parse(tsingmt);
            TimeZone tz = TimeZone.getDefault();
            formater.setTimeZone(tz);
            assert d != null;
            s = formater.format(d);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return s;
    }

    public static String getThings() {
        return Credentials.basic(BuildConfig.abba, BuildConfig.dabba);
    }

    public static void showMessage(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static String getModifiedImageLink(String url) {
        if (url != null && url.contains("cloudinary")) {
            String extension = url.substring(url.lastIndexOf("."));
            url = url.replace(extension, Constants.IMAGE_EXT);
            return url;
        } else {
            return url;
        }
    }

    public static String getModifiedThumbnailLink(String url) {
        if (url.contains("cloudinary")) {
            return url.replace("upload", "upload/t_media_lib_thumb")
                    .replace(".jpg", ".webp")
                    .replace(".jpeg", ".webp")
                    .replace(".png", ".webp");
        } else {
            return url;
        }
    }

    public static String getModifiedVideoLink(String url) {
        return url.replace("upload/", "upload/vc_auto/");
    }

    public static int getDrawableByString(Context mContext, String name) {
        return mContext.getResources().getIdentifier(name, "drawable", mContext.getPackageName());
    }

    public String gmtToEpoch(String tsingmt) {

        Date d = null;

        long epoch = 0;

        SimpleDateFormat formater = new SimpleDateFormat("yyyyMMddHHmmssSSS z", Locale.US);

        try {
            d = formater.parse(tsingmt);

            epoch = d.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return String.valueOf(epoch);
    }

    public static long Daybetween(String date1, String date2, String pattern) {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern, Locale.US);

        sdf.setTimeZone(TimeZone.getDefault());

        Date startDate = null, endDate = null;
        try {
            startDate = sdf.parse(date1);
            endDate = sdf.parse(date2);
        } catch (Exception e) {
            e.printStackTrace();

            sdf = new SimpleDateFormat(pattern.substring(0, 19), Locale.US);
            try {
                startDate = sdf.parse(date1);
                endDate = sdf.parse(date2);
            } catch (ParseException ef) {
                ef.printStackTrace();
            }
        }

        Calendar sDate = getDatePart(startDate);
        Calendar eDate = getDatePart(endDate);

        long daysBetween = 0;
        while (sDate.before(eDate)) {
            sDate.add(Calendar.DAY_OF_MONTH, 1);
            daysBetween++;
        }
        return daysBetween;
    }

    public static Calendar getDatePart(Date date) {
        Calendar cal = Calendar.getInstance();       // get calendar instance
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);            // set hour to midnight
        cal.set(Calendar.MINUTE, 0);                 // set minute in hour
        cal.set(Calendar.SECOND, 0);                 // set second in minute
        cal.set(Calendar.MILLISECOND, 0);            // set millisecond in second

        return cal;                                  // return the date part
    }

    public static String formatDate(String ts) {

        String s = null;
        Date d = null;

        SimpleDateFormat formater = new SimpleDateFormat("yyyyMMddHHmmssSSS z", Locale.US);

        try {
            d = formater.parse(ts);

            formater = new SimpleDateFormat("HH:mm:ss EEE dd/MMM/yyyy z", Locale.US);
            s = formater.format(d);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return s;
    }

    public static String epochtoGmt(String tsingmt) {

        Date d = null;
        String s = null;
        long epoch = 0;

        SimpleDateFormat formater = new SimpleDateFormat("yyyyMMddHHmmssSSS z", Locale.US);
        formater.setTimeZone(TimeZone.getTimeZone("GMT"));
        epoch = Long.parseLong(tsingmt);

        d = new Date(epoch);
        s = formater.format(d);

        return s;
    }

    public static String changeStatusDateFromGMTToLocal(String ts) {

        String s = null;
        Date d;

        SimpleDateFormat formater = new SimpleDateFormat("yyyyMMddHHmmssSSS z", Locale.US);

        try {
            d = formater.parse(ts);

            TimeZone tz = TimeZone.getDefault();
            formater.setTimeZone(tz);

            s = formater.format(d);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return s;
    }

    /**
     * To get the epoch time of the current time in gmt
     */

    public static long getGmtEpoch() {

        Date localTime =
                new Date(System.currentTimeMillis() - AppController.getInstance().getTimeDelta());

        Date d;

        SimpleDateFormat formater = new SimpleDateFormat("yyyyMMddHHmmssSSS z", Locale.US);
        formater.setTimeZone(TimeZone.getTimeZone("GMT"));

        String tsingmt = formater.format(localTime);

        long epoch = 0;

        try {
            d = formater.parse(tsingmt);

            epoch = d.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return epoch;
    }

    /*
     * Get calls time based on the timezone
     */
    public static String tsFromGmtToLocalTimeZone(String tsingmt) {

        String s = null;
        Date d = null;

        SimpleDateFormat formater = new SimpleDateFormat("yyyyMMddHHmmssSSS z", Locale.US);

        try {
            d = formater.parse(tsingmt);

            formater = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z", Locale.US);
            formater.setTimeZone(TimeZone.getDefault());

            s = formater.format(d);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return s;
    }

    /*
     * to convert string from the 24 hour format to 12 hour format
     */
    public static String convert24to12hourformat(String d) {

        String datein12hour = null;

        try {
            final SimpleDateFormat sdf = new SimpleDateFormat("H:mm", Locale.US);
            final Date dateObj = sdf.parse(d);

            datein12hour = new SimpleDateFormat("h:mm a", Locale.US).format(dateObj);
        } catch (final ParseException e) {
            e.printStackTrace();
        }

        return datein12hour;
    }

    public static String findOverlayDate(String date) {
        try {

            SimpleDateFormat sdf = new SimpleDateFormat("EEE dd/MMM/yyyy", Locale.US);

            String m1 = "", m2 = "";

            String month1, month2;

            String d1, d2;

            d1 = sdf.format(
                    new Date(System.currentTimeMillis() - AppController.getInstance().getTimeDelta()));

            d2 = date;

            month1 = d1.substring(7, 10);

            month2 = d2.substring(7, 10);

            if (month1.equals("Jan")) {
                m1 = "01";
            } else if (month1.equals("Feb")) {
                m1 = "02";
            } else if (month2.equals("Mar")) {
                m2 = "03";
            } else if (month1.equals("Apr")) {
                m1 = "04";
            } else if (month1.equals("May")) {
                m1 = "05";
            } else if (month1.equals("Jun")) {
                m1 = "06";
            } else if (month1.equals("Jul")) {
                m1 = "07";
            } else if (month1.equals("Aug")) {
                m1 = "08";
            } else if (month1.equals("Sep")) {
                m1 = "09";
            } else if (month1.equals("Oct")) {
                m1 = "10";
            } else if (month1.equals("Nov")) {
                m1 = "11";
            } else if (month1.equals("Dec")) {
                m1 = "12";
            }

            if (month2.equals("Jan")) {
                m2 = "01";
            } else if (month2.equals("Feb")) {
                m2 = "02";
            } else if (month1.equals("Mar")) {
                m1 = "03";
            } else if (month2.equals("Apr")) {
                m2 = "04";
            } else if (month2.equals("May")) {
                m2 = "05";
            } else if (month2.equals("Jun")) {
                m2 = "06";
            } else if (month2.equals("Jul")) {
                m2 = "07";
            } else if (month2.equals("Aug")) {
                m2 = "08";
            } else if (month2.equals("Sep")) {
                m2 = "09";
            } else if (month2.equals("Oct")) {
                m2 = "10";
            } else if (month2.equals("Nov")) {
                m2 = "11";
            } else if (month2.equals("Dec")) {
                m2 = "12";
            }
            month1 = null;
            month2 = null;

            if (sdf.format(
                    new Date(System.currentTimeMillis() - AppController.getInstance().getTimeDelta()))
                    .equals(date)) {

                m2 = null;
                m1 = null;
                d2 = null;
                d1 = null;
                sdf = null;
                return "Today";
            } else if ((Integer.parseInt(d1.substring(11) + m1 + d1.substring(4, 6)) - Integer.parseInt(
                    d2.substring(11) + m2 + d2.substring(4, 6))) == 1) {

                m2 = null;
                m1 = null;
                d2 = null;
                d1 = null;
                sdf = null;
                return "Yesterday";
            } else {

                m2 = null;
                m1 = null;
                d2 = null;
                d1 = null;
                sdf = null;
                return date;
            }
        } catch (Exception e) {
            e.printStackTrace();

            return date;
        }
    }

    /**
     * To remove an element from the jsonarray even below api level 19
     */
    public static JSONArray removeElementFromJSONArray(JSONArray jarray, int pos) {

        if (Build.VERSION.SDK_INT >= 19) {

            jarray.remove(pos);
            return jarray;
        }

        JSONArray Njarray = new JSONArray();
        try {
            for (int i = 0; i < jarray.length(); i++) {
                if (i != pos) Njarray.put(jarray.get(i));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Njarray;
    }

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }


    private static final NavigableMap<Double, String> suffixes = new TreeMap<>();

    static {
        suffixes.put(1_000D, "K");
        suffixes.put(1_000_000D, "M");
        suffixes.put(1_000_000_000D, "G");
        suffixes.put(1_000_000_000_000D, "T");
        suffixes.put(1_000_000_000_000_000D, "P");
        suffixes.put(1_000_000_000_000_000_000D, "E");
    }

    public static String formatMoney(double value) {
        //Long.MIN_VALUE == -Long.MIN_VALUE so we need an adjustment here
        if (value == Long.MIN_VALUE) return formatMoney(Long.MIN_VALUE + 1);
        if (value < 0) return "-" + formatMoney(-value);
        if (value < 1000) return Double.toString(value); //deal with easy case

        Map.Entry<Double, String> e = suffixes.floorEntry(value);
        Double divideBy = e.getKey();
        String suffix = e.getValue();

        double truncated = value / (divideBy / 10); //the number part of the output times 10
        boolean hasDecimal = truncated < 100 && (truncated / 10d) != (truncated / 10);
        String b = hasDecimal ? (truncated / 10d) + suffix : (truncated / 10) + suffix;
        String lastChar = b.substring(b.length() - 1);

        double balance = Double.parseDouble(b.replace(lastChar, ""));
        return df.format(balance) + "" + lastChar;
    }

    public static String stringDateFormat(String date) {
        try {

            TimeZone tz = TimeZone.getDefault();

            SimpleDateFormat output = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");
            SimpleDateFormat input = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");
            Date d = input.parse(date.replace("T", " ").replace("Z", " "));
            output.setTimeZone(tz);
            return output.format(d);
        } catch (Exception e) {
            return date;
        }
    }

    public static String getDate(String tsingmt) {
        Date d = null;
        String s = null;
        long epoch = 0;

        SimpleDateFormat formater = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");
//    formater.setTimeZone(TimeZone.getTimeZone("GMT"));
        formater.setTimeZone(TimeZone.getDefault());
        epoch = Long.parseLong(tsingmt);

        d = new Date(epoch);
        s = formater.format(d);

        return s;
    }

    @SuppressLint("PrivateResource")
    public static Bitmap setCreditCardLogo(String cardMethod, Context context) {
        Bitmap anImage;
        switch (cardMethod) {
            case "Visa":
            case "visa":
                anImage = CardType.VISA.imageBitmap(context);//getBitmapFromVectorDrawable(context, CardBrand.Visa.getIcon());
                break;
            case "MasterCard":
            case "mastercard":
                anImage = CardType.MASTERCARD.imageBitmap(context);//getBitmapFromVectorDrawable(context, CardBrand.MasterCard.getIcon());
                break;
            case "American Express":
            case "amex":
                anImage = CardType.AMEX.imageBitmap(context);//getBitmapFromVectorDrawable(context, CardBrand.AmericanExpress.getIcon());
                break;
            case "Discover":
            case "discover":
                anImage = CardType.DISCOVER.imageBitmap(context); //getBitmapFromVectorDrawable(context, CardBrand.Discover.getIcon());
                break;
            case "Diners Club":
            case "diners":
                anImage = CardType.DINERSCLUB.imageBitmap(context);//getBitmapFromVectorDrawable(context, CardBrand.DinersClub.getIcon());
                break;

            case "JCB":
            case "jcb":
                anImage = CardType.JCB.imageBitmap(context);//getBitmapFromVectorDrawable(context, CardBrand.JCB.getIcon());
                break;

            case "unionpay":
                anImage = CardType.MAESTRO.imageBitmap(context);//getBitmapFromVectorDrawable(context, CardBrand.UnionPay.getIcon());
                break;

            case "Cash":
                anImage = getBitmapFromVectorDrawable(context, R.drawable.ic_cash_icon);
                break;
            case "Wallet":
                anImage = getBitmapFromVectorDrawable(context,
                        R.drawable.ic_account_balance_wallet_black_24dp);
                break;

            default:
                anImage = CardType.UNKNOWN.imageBitmap(context);//getBitmapFromVectorDrawable(context, R.drawable.visa_card);
                break;
        }
        return anImage;
    }

    @SuppressLint("RestrictedApi")
    public static Bitmap getBitmapFromVectorDrawable(Context context, int drawableId) {
        Drawable drawable = AppCompatDrawableManager.get().getDrawable(context, drawableId);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            drawable = (DrawableCompat.wrap(drawable)).mutate();
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    public static int bitrateValue() {
        int bitrate = 0;
        NetworkInfo info = getNetworkInfo(AppController.getInstance());
        if (info.getType() == ConnectivityManager.TYPE_MOBILE) {

            switch (info.getSubtype()) {

                case TelephonyManager.NETWORK_TYPE_1xRTT:
                case TelephonyManager.NETWORK_TYPE_EDGE:
                    bitrate = 128000;
                    break; // ~ 50-100 -Kbps slow-----
                case TelephonyManager.NETWORK_TYPE_CDMA:
                    bitrate = 128000;
                    break; // ~ 14-64 -Kbps slow-----
                case TelephonyManager.NETWORK_TYPE_EVDO_0:
                    bitrate = 128000;
                    break; // ~ 400-1000 -Kbps
                case TelephonyManager.NETWORK_TYPE_EVDO_A:
                    bitrate = 128000;
                    break; // ~ 600-1400 -Kbps
                case TelephonyManager.NETWORK_TYPE_GPRS:
                    bitrate = 128000;
                    break; // ~ 100 -Kbps slow-----
                case TelephonyManager.NETWORK_TYPE_HSDPA:
                    bitrate = 300000;
                    break; // ~ 2000-1400 -Kbps
                case TelephonyManager.NETWORK_TYPE_HSPA:
                    bitrate = 128000;
                    break; // ~ 700-1700 -Kbps
                case TelephonyManager.NETWORK_TYPE_HSUPA:
                    bitrate = 200000;
                    break; // ~ 1000-2300 -Kbps
                case TelephonyManager.NETWORK_TYPE_UMTS:
                    bitrate = 200000;
                    break; // ~ 400-7000 -Kbps
                case TelephonyManager.NETWORK_TYPE_EHRPD:
                    bitrate = 200000;
                    break; // ~ 1000-2000 -Kbps // API level 11
                case TelephonyManager.NETWORK_TYPE_EVDO_B:
                    bitrate = 200000;
                    break; // ~ 5000 -Kbps // API level 9
                case TelephonyManager.NETWORK_TYPE_HSPAP:
                    bitrate = 200000;
                    break; // ~ 10000-20000-Kbps // API level 13
                case TelephonyManager.NETWORK_TYPE_IDEN:
                    bitrate = 128000;
                    break; // ~ 25 -Kbps // API level 8
                case TelephonyManager.NETWORK_TYPE_LTE:
                    bitrate = 200000;
                    break; // ~ 10000+ -Kbps // API level 11
                case TelephonyManager.NETWORK_TYPE_UNKNOWN: // ~ Unknown
                    bitrate = 128000;
                    break;
                default:
                    break;
            }
        } else {
            bitrate = 2000000;
        }

        return bitrate;
    }

    /**
     * Get network info
     *
     * @param context - For getting connectivity service
     * @return NetworkInfo
     */
    public static NetworkInfo getNetworkInfo(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return Objects.requireNonNull(connectivityManager).getActiveNetworkInfo();
    }

    public static String getHashCode(Context context){
        AppSignatureHelper appSignature = new AppSignatureHelper(context);
        Log.e("asdfghjkl",""+appSignature.getAppSignatures());
        Log.e(" asdfghjkl",""+appSignature.getAppSignatures().get(0));
        return appSignature.getAppSignatures().get(0);
    }

    public static void initSmsRetriever(Activity activity) {
        // Get an instance of SmsRetrieverClient, used to start listening for a matching
// SMS message.
        SmsRetrieverClient client = SmsRetriever.getClient(activity /* context */);

// Starts SmsRetriever, which waits for ONE matching SMS message until timeout
// (5 minutes). The matching SMS message will be sent via a Broadcast Intent with
// action SmsRetriever#SMS_RETRIEVED_ACTION.
        Task<Void> task = client.startSmsRetriever();

// Listen for success/failure of the start Task. If in a background thread, this
// can be made blocking using Tasks.await(task, [timeout]);
        task.addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                // Successfully started retriever, expect broadcast intent
                // ...
                Log.d("SMS RETRIEVER", "onSuccess: ");
            }
        });

        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                // Failed to start retriever, inspect Exception for more details
                // ...
                Log.d("SMS RETRIEVER", "onFailed: "+e.getLocalizedMessage());
                e.printStackTrace();
            }
        });
    }

}