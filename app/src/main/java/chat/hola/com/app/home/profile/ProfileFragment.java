package chat.hola.com.app.home.profile;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.signature.StringSignature;
import com.lighthusky.dingdong.R;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;
import com.squareup.otto.Bus;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import chat.hola.com.app.AppController;
import chat.hola.com.app.Dialog.BlockDialog;
import chat.hola.com.app.ImageCropper.CropImage;
import chat.hola.com.app.Utilities.ConnectivityReceiver;
import chat.hola.com.app.Utilities.TypefaceManager;
import chat.hola.com.app.Utilities.Utilities;
import chat.hola.com.app.home.LandingActivity;
import chat.hola.com.app.manager.session.SessionManager;
import chat.hola.com.app.models.WalletResponse;
import chat.hola.com.app.my_qr_code.MyQRCodeActivity;
import chat.hola.com.app.profileScreen.ProfilePageAdapter;
import chat.hola.com.app.profileScreen.addChannel.AddChannelActivity;
import chat.hola.com.app.profileScreen.channel.ChannelFragment;
import chat.hola.com.app.profileScreen.discover.DiscoverActivity;
import chat.hola.com.app.profileScreen.editProfile.EditProfileActivity;
import chat.hola.com.app.profileScreen.followers.FollowersActivity;
import chat.hola.com.app.profileScreen.liked.LikedPostFragment;
import chat.hola.com.app.profileScreen.model.Data;
import chat.hola.com.app.profileScreen.model.Profile;
import chat.hola.com.app.profileScreen.star_video.PlayVideoActivity;
import chat.hola.com.app.profileScreen.story.StoryFragment;
import chat.hola.com.app.profileScreen.tag.TagFragment;
import chat.hola.com.app.settings.SettingsActivity;
import dagger.android.support.DaggerFragment;

public class ProfileFragment extends DaggerFragment
        implements ProfileContract.View, ConnectivityReceiver.ConnectivityReceiverListener,
        SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = ProfileFragment.class.getSimpleName();
    private final int CAMERA_REQUEST = 222;
    private final int READ_STORAGE_REQ_CODE = 26;
    private final int RESULT_LOAD_IMAGE = 1;
    public static boolean isPrivate = false;
    public static int followStatus = 0;

    @Inject
    ProfilePresenter presenter;
    @Inject
    ChannelFragment channelFragment;
    @Inject
    TagFragment tagFragment;
    @Inject
    TypefaceManager typefaceManager;
    @Inject
    SessionManager sessionManager;
    @Inject
    BlockDialog dialog1;
    @Inject
    StoryFragment storyFragment;
    @Inject
    LikedPostFragment likedPostFragment;

    @BindView(R.id.ivEditProfile)
    ImageView ibEditProfile;
    @BindView(R.id.ivProfile)
    ImageView ivProfile;
    @BindView(R.id.ivProfileBg)
    ImageView ivProfileBg;
    @BindView(R.id.tvProfileName)
    TextView tvProfileName;
    @BindView(R.id.tvProfileMob)
    TextView tvProfileMob;
    @BindView(R.id.tvProfileStatus)
    TextView tvProfileStatus;
    @BindView(R.id.tvPostCount)
    TextView tvPostCount;
    @BindView(R.id.tvFollowingCount)
    TextView tvFollowingCount;
    @BindView(R.id.tvFollowersCount)
    TextView tvFollowersCount;
    @BindView(R.id.tvPostTitle)
    TextView tvPostTitle;
    @BindView(R.id.tvFollowingTitle)
    TextView tvFollowingTitle;
    @BindView(R.id.tvFollowersTitle)
    TextView tvFollowersTitle;
    @BindView(R.id.tabLayoutProfile)
    TabLayout tabLayoutProfile;
    @BindView(R.id.viewPagerProfile)
    ViewPager viewPagerProfile;
    @BindView(R.id.faButton)
    FloatingActionButton faButton;
    @BindView(R.id.layoutAppBar)
    AppBarLayout appBarLayout;
    @BindView(R.id.btnFollow)
    public ToggleButton btnFollow;
    //  @BindView(R.id.collapseToolbarLayout)
//  CollapsingToolbarLayout collapseToolbarLayout;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.profileRoot)
    CoordinatorLayout root;
    @BindView(R.id.llwallet)
    LinearLayout llwallet;
    @BindView(R.id.iV_wallet)
    ImageView iV_wallet;
    @BindView(R.id.iV_scan)
    ImageView iV_scan;
    @BindView(R.id.rl_balance)
    RelativeLayout rl_balance;
    @BindView(R.id.tV_balance)
    TextView tV_balance;
    @BindView(R.id.iV_star)
    ImageView iV_star;
    @BindView(R.id.rL_star_connect)
    RelativeLayout rL_star_connect;
    @BindView(R.id.iV_email)
    ImageView iV_email;
    @BindView(R.id.iV_chat)
    ImageView iV_chat;
    @BindView(R.id.iV_phone)
    ImageView iV_phone;
    @BindView(R.id.cV_starVideo)
    CardView cV_starVideo;
    @BindView(R.id.iV_starVideo)
    ImageView iV_starVideo;
    @BindView(R.id.tV_starDesc)
    TextView tV_starDesc;

    @BindView(R.id.tvPrivateTitle)
    TextView tvPrivateTitle;
    @BindView(R.id.tvPrivateMessage)
    TextView tvPrivateMessage;
    @BindView(R.id.llPrivate)
    LinearLayout llPrivate;
    @BindView(R.id.tvUserName)
    TextView tvUserName;

    @BindView(R.id.ibBusinessCall)
    ImageButton ibBusinessCall;
    @BindView(R.id.ibBusinessChat)
    ImageButton ibBusinessChat;
    @BindView(R.id.ibBusinessEmail)
    ImageButton ibBusinessEmail;
    @BindView(R.id.ibBusinessLocation)
    ImageButton ibBusinessLocation;
    @BindView(R.id.llBusinessProfileContact)
    LinearLayout llBusinessProfileContact;
    @BindView(R.id.tvBusinessType)
    TextView tvBusinessType;
    @BindView(R.id.iV_settings)
    ImageView iV_settings;

    AlertDialog.Builder reportDialog;
    AlertDialog.Builder blockDialog;
    ArrayAdapter<String> arrayAdapter;
    ArrayAdapter<String> blockReasons;

    private Unbinder unbinder;
    private ProgressDialog dialog;
    public String userId;
    public String userName = "";
    private Data profileData;
    private Data.BusinessProfile businessProfile;
    //private Menu menu;
    //private ActionBar actionBar;
    //    private Drawable backDrawableBlack;
    //    private Drawable backDrawableWhite;
    private String myid;
    boolean isBlocked = false;
    //private MenuItem block, unfriend;
    //    private Drawable menudots;
    //    private Drawable menudotsWhite;

    Bus bus = AppController.getBus();
    private boolean isFriend = false;
    private boolean isBusiness = false;

    private Activity mActivity;
    private boolean isVpSetUp = false;

    private TextView tVTitle;
    private ImageView iVDiscover, iVMenu;
    private LinearLayout linearTitle;

    @Inject
    public ProfileFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //bus.register(this);
        mActivity = getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.profile_fragment, container, false);

        unbinder = ButterKnife.bind(this, view);
        presenter.attachView(this);

        //changeVisibilityOfViews();

        //        backDrawableBlack = getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp);
        //        backDrawableWhite = getResources().getDrawable(R.drawable.ic_arrow_back_white_24dp);
        //        menudots = ContextCompat.getDrawable(mActivity.getApplicationContext(), R.drawable.ic_more_black);
        //        menudotsWhite = ContextCompat.getDrawable(mActivity.getApplicationContext(), R.drawable.ic_more_vert);

        myid = AppController.getInstance().getUserId();

        linearTitle = toolbar.findViewById(R.id.linearTitle);
        tVTitle = toolbar.findViewById(R.id.tvTitle);
        iVDiscover = toolbar.findViewById(R.id.iVDiscover);
        iVMenu = toolbar.findViewById(R.id.iVMenu);

        //collapseToolbarLayout.setTitle("Profile");
        //collapseToolbarLayout.setCollapsedTitleTypeface(typefaceManager.getSemiboldFont());
        //collapseToolbarLayout.setExpandedTitleTypeface(typefaceManager.getSemiboldFont());
        //collapseToolbarLayout.setExpandedTitleColor(Color.TRANSPARENT);

        userId = AppController.getInstance().getUserId();
        //preferences = getSharedPreferences("userProfile", MODE_PRIVATE);
        presenter.init();
        reportDialog = new AlertDialog.Builder(mActivity);
        reportDialog.setTitle(R.string.report);
        presenter.getReportReasons();

        blockDialog = new AlertDialog.Builder(mActivity);
        blockDialog.setTitle(R.string.Block);
        presenter.getBlockReasons();

        dialog = new ProgressDialog(mActivity);
        dialog.setMessage(getResources().getString(R.string.please_wait));
        dialog.setCancelable(false);

        btnFollow.setChecked(false); //default

        btnFollow.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (btnFollow.isChecked()) {
                    presenter.follow(userId);
                } else {
                    presenter.unfollow(userId);
                }
            }
        });

        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                changeColor(verticalOffset > -600);
            }
        });
        viewPagerProfile.setOffscreenPageLimit(5);
        viewPagerProfile.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                sessionManager.setProfileCurrentTab(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        //load profile data
        //isBusiness = getIntent().getBooleanExtra("isBusiness", false);
        changeVisibilityOfViews();
        //loadData();
        ibEditProfile.setEnabled(false);

        iVMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settings();
            }
        });

        iVDiscover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                discover();
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        loadData();
        viewPagerProfile.setCurrentItem(sessionManager.getProfileCurrentTab());
    }


    @Override
    public void onDestroy() {
        presenter.detachView();
        sessionManager.setProfileCurrentTab(0);
        if (unbinder != null) unbinder.unbind();
        //bus.unregister(this);
        super.onDestroy();
    }

    public void changeVisibilityOfViews() {
        ((LandingActivity) mActivity).hideActionBar();
        ((LandingActivity) mActivity).removeFullScreenFrame();
        ((LandingActivity) mActivity).linearPostTabs.setVisibility(View.GONE);
        ((LandingActivity) mActivity).drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        loadData();
    }

    /**
     * Set Business profile details
     *
     * @param isChecked : true= business profile, false= non business profile
     */
    private void switchToBusiness(boolean isChecked) {

        Data.BusinessProfile businessProfile = profileData.getBusinessProfiles().get(0);

        boolean isMine = userId.equals(AppController.getInstance().getUserId());

        String fname = profileData.getFirstName();
        String lname = profileData.getLastName();
        String name = fname + " " + lname;

        tvBusinessType.setVisibility(isChecked ? View.VISIBLE : View.GONE);
        tvBusinessType.setText(profileData.getBusinessProfiles().get(0).getBusinessCategory());
        llBusinessProfileContact.setVisibility(!isMine && isChecked ? View.VISIBLE : View.GONE);
        tvProfileMob.setVisibility(isMine && isChecked ? View.VISIBLE : View.GONE);
        tvProfileMob.setText(isChecked ? businessProfile.getAddress() : profileData.getNumber());
        tvUserName.setText(isChecked ? businessProfile.getWebsite() : name);
        tvProfileName.setText(
                isChecked ? businessProfile.getBusinessName() : profileData.getUserName());

        ibBusinessCall.setVisibility(
                isChecked && businessProfile.getPhone().getVisible() == 1 ? View.VISIBLE : View.GONE);
        ibBusinessEmail.setVisibility(
                isChecked && businessProfile.getEmail().getVisible() == 1 ? View.VISIBLE : View.GONE);

        String profileStatus = profileData.getStatus();
        if (profileStatus == null || profileStatus.isEmpty()) {
            profileStatus = "";
        }

        tvProfileStatus.setText(isChecked ? businessProfile.getBusinessBio() : profileStatus);

        Glide.with(mActivity.getBaseContext())
                .load(businessProfile.getBusinessProfilePic())
                .asBitmap()
                .signature(new StringSignature(
                        AppController.getInstance().getSessionManager().getUserProfilePicUpdateTime()))
                .centerCrop()
                //.placeholder(R.drawable.profile_one)
                //.signature(new StringSignature(String.valueOf(System.currentTimeMillis())))
                //.diskCacheStrategy(DiskCacheStrategy.NONE)
                //.skipMemoryCache(true)
                .into(ivProfile);

        Glide.clear(ivProfileBg);
        Glide.with(mActivity.getBaseContext())
                .load(businessProfile.getBusinessProfileCoverImage() != null
                        && !businessProfile.getBusinessProfilePic().isEmpty()
                        ? businessProfile.getBusinessProfilePic() : businessProfile.getBusinessProfilePic())
                .asBitmap()
                .signature(new StringSignature(
                        AppController.getInstance().getSessionManager().getUserProfilePicUpdateTime()))
                .centerCrop()
                //.signature(new StringSignature(String.valueOf(System.currentTimeMillis())))
                //.diskCacheStrategy(DiskCacheStrategy.NONE)
                //.skipMemoryCache(true)
                .into(ivProfileBg);
    }

    @OnClick(R.id.cV_starVideo)
    public void openVideo() {
        Intent intent = new Intent(mActivity, PlayVideoActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.ivCloseAd)
    public void closeAd() {
        cV_starVideo.setVisibility(View.GONE);
    }

    @OnClick(R.id.rl_balance)
    public void openTransaction() {
        presenter.kycVerification();
    }

    @OnClick(R.id.ibBusinessLocation)
    public void location() {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:0,0?q="
                + businessProfile.getBusinessLat()
                + ","
                + businessProfile.getBusinessLng()
                + " ( )"));
        startActivity(intent);
    }

    @OnClick(R.id.iV_scan)
    public void openMyQRCode() {
        if (profileData != null) {
            Intent intent = new Intent(mActivity, MyQRCodeActivity.class);
            intent.putExtra("profileData", profileData);
            startActivity(intent);
        }
    }

    @OnClick({R.id.iV_email, R.id.ibBusinessEmail})
    public void showEmail() {

        Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
        emailIntent.setData(Uri.parse(
                "mailto:" + (profileData.isActiveBussinessProfile() ? businessProfile.getEmail().getId()
                        : profileData.getVerified().getEmailId())));
        try {
            startActivity(emailIntent);
        } catch (ActivityNotFoundException e) {
            //TODO: Handle case where no email app is available
        }

        //openContactDialog(1);
    }

    @OnClick({R.id.iV_phone, R.id.ibBusinessCall})
    public void showPhoneNumber() {

        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse(
                "tel:" + (profileData.isActiveBussinessProfile() ? businessProfile.getPhone()
                        .getCountryCode() + "" + businessProfile.getPhone().getNumber()
                        : profileData.getVerified().getNumber())));
        startActivity(intent);

        //openContactDialog(2);
    }

    @OnClick({R.id.iV_chat, R.id.ibBusinessChat})
    public void openChat() {
        if (userId != null && !userId.equals(myid)) {
        }
    }

    public void loadData() {
        //load data
        if (userId != null && !userId.equals(myid)) {
            ibEditProfile.setVisibility(View.GONE);
            setFabButtonVisible(false);
            presenter.loadMemberData(userId);
        } else {
            ibEditProfile.setVisibility(View.VISIBLE);
            setFabButtonVisible(true);
            presenter.loadProfileData();
            presenter.getWalletBalance();
        }
    }

    //@Subscribe
    //public void getMessage(JSONObject object) {
    //    try {
    //        if (object.getString("eventName").equals("profileUpdated")) {
    //            loadData();
    //        }
    //    } catch (JSONException e) {
    //        e.printStackTrace();
    //    }
    //}

    private void changeColor(boolean white) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                if (white) {
                    //                    MenuItem discover = menu.findItem(R.id.actionDiscoverPpl);
                    //                    discover.setIcon(R.drawable.ic_discover);
                    //                    MenuItem settings = menu.findItem(R.id.actionSettings);
                    //                    settings.setIcon(R.drawable.ic_settings_white_24dp);
                    //actionBar.setHomeAsUpIndicator(backDrawableWhite);
                    //if (toolbar != null)
                    iV_settings.setColorFilter(ContextCompat.getColor(mActivity, R.color.color_white),
                            android.graphics.PorterDuff.Mode.SRC_IN);
                    //toolbar.setOverflowIcon(menudotsWhite);
                } else {
                    //                    MenuItem discover = menu.findItem(R.id.actionDiscoverPpl);
                    //                    discover.setIcon(R.drawable.ic_discover_black);
                    //                    MenuItem settings = menu.findItem(R.id.actionSettings);
                    //                    settings.setIcon(R.drawable.ic_settings_black_24dp);
                    //actionBar.setHomeAsUpIndicator(backDrawableBlack);
                    //if (toolbar != null)
                    iV_settings.setColorFilter(ContextCompat.getColor(mActivity, R.color.color_black),
                            android.graphics.PorterDuff.Mode.SRC_IN);
                    //toolbar.setOverflowIcon(menudots);
                }
            }
        });
    }

    public static void setOverflowButtonColor(final Toolbar toolbar, final int color) {
        Drawable drawable = toolbar.getOverflowIcon();
        if (drawable != null) {
            drawable = DrawableCompat.wrap(drawable);
            DrawableCompat.setTint(drawable.mutate(), color);
            toolbar.setOverflowIcon(drawable);
        }
    }

    //    @Override
    //    public boolean onCreateOptionsMenu(Menu menu) {
    //        this.menu = menu;
    //        MenuInflater menuInflater = getMenuInflater();
    //        menuInflater.inflate(R.menu.new_profile_menu, menu);
    //        return true;
    //    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.actionUnFriend:
                presenter.unfriend(userId);
            case android.R.id.home:
                //back();
                return true;
            //            case R.id.actionDiscoverPpl:
            //                discover();
            //                return true;
            case R.id.actionSettings:
                settings();
                return true;
            case R.id.actionReport:
                reportDialog.show();
                return true;
            case R.id.actionBlock:
                if (isBlocked) {
                    AlertDialog.Builder confirm = new AlertDialog.Builder(mActivity);
                    confirm.setMessage("Are you sure you want to " + " unblock " + userName + "?");
                    confirm.setPositiveButton(R.string.confirm,
                            (dialog, w) -> presenter.block(userId, "unblock", "unblock"));
                    confirm.setNegativeButton(R.string.cancel, (dialog, w) -> dialog.dismiss());
                    confirm.create().show();
                } else {
                    AlertDialog.Builder confirm = new AlertDialog.Builder(mActivity);
                    confirm.setMessage("Are you sure you want to " + " block " + userName + "?");
                    confirm.setPositiveButton(R.string.confirm,
                            (dialog, w) -> presenter.block(userId, "block", "block"));
                    confirm.setNegativeButton(R.string.cancel, (dialog, w) -> dialog.dismiss());
                    confirm.create().show();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void unblockDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setMessage(R.string.unblock_user_msg);
        builder.setPositiveButton(R.string.unblock, (dialog, which) -> {
            presenter.block(userId, "unblock", "");
        });

        builder.setNegativeButton(R.string.cancel, (dialog, which) -> {
            dialog.dismiss();
        });
        builder.create();
        builder.show();
    }

    public void discover() {
        startActivity(
                new Intent(mActivity, DiscoverActivity.class).putExtra("caller", "ProfileActivity"));
    }

    @OnClick(R.id.tvPostCount)
    public void post() {
        //        viewPagerProfile.setCurrentItem(0);
        appBarLayout.setExpanded(false, true);
    }

    @OnClick({R.id.tvFollowersCount, R.id.tvFollowersTitle})
    public void followers() {
        try {
            if (Integer.parseInt(tvFollowersCount.getText().toString()) != 0) {
                Intent intent = new Intent(mActivity, FollowersActivity.class);
                intent.putExtra("title", getResources().getString(R.string.followers));
                intent.putExtra("following", profileData.getFollowers());
                intent.putExtra("userId", profileData.getId());
                startActivity(intent);
            }
        } catch (Exception ignored) {
        }
    }

    @OnClick({R.id.tvFollowingCount, R.id.tvFollowingTitle})
    public void following() {
        try {
            String followingCount = tvFollowingCount.getText().toString();
            if (!followingCount.isEmpty()) {
                if (Integer.parseInt(followingCount) != 0) {
                    Intent intent = new Intent(mActivity, FollowersActivity.class);
                    intent.putExtra("title", getResources().getString(R.string.following));
                    intent.putExtra("following", profileData.getFollowing());
                    intent.putExtra("userId", profileData.getId());
                    startActivity(intent);
                }
            }
        } catch (Exception ignored) {
            ignored.printStackTrace();
        }
    }

    @OnClick(R.id.iV_settings)
    public void settings() {
        Intent intent = new Intent(mActivity, SettingsActivity.class);
        intent.putExtra("profileData", profileData);
        startActivity(intent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //if (requestCode == 0) {

        //if (resultCode == RESULT_OK) {

        //boolean imageUpdated = data.getBooleanExtra("imageUpdated", false);
        //
        //if (imageUpdated) {
        //try {
        //  new ClearGlideCacheAsyncTask().execute();
        //} catch (Exception e) {
        //  e.printStackTrace();
        //}
        //AppController.getInstance().getSessionManager().setUserProfilePicUpdateTime();
        //}
        //presenter.loadProfileData();
        //}
        //} else

        if (requestCode == RESULT_LOAD_IMAGE) {
            presenter.parseMedia(requestCode, resultCode, data);
        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            presenter.parseCropedImage(requestCode, resultCode, data);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void applyFont() {
        tvProfileName.setTypeface(typefaceManager.getMediumFont());
        tvUserName.setTypeface(typefaceManager.getSemiboldFont());
        tvProfileMob.setTypeface(typefaceManager.getRegularFont());
        tvProfileStatus.setTypeface(typefaceManager.getRegularFont());
        tvPostCount.setTypeface(typefaceManager.getMediumFont());
        tvFollowingCount.setTypeface(typefaceManager.getMediumFont());
        tvFollowersCount.setTypeface(typefaceManager.getMediumFont());
        tvPostTitle.setTypeface(typefaceManager.getRegularFont());
        tvFollowingTitle.setTypeface(typefaceManager.getRegularFont());
        tvFollowersTitle.setTypeface(typefaceManager.getRegularFont());
        btnFollow.setTypeface(typefaceManager.getMediumFont());
        tvBusinessType.setTypeface(typefaceManager.getMediumFont());
    }

    @Override
    public void setupViewPager() {
        tabLayoutProfile.setupWithViewPager(viewPagerProfile);
        ProfilePageAdapter fragmentPageAdapter = new ProfilePageAdapter(getChildFragmentManager());
        try {
            fragmentPageAdapter.addFragment(storyFragment, "", "grid", userId, userName);
            fragmentPageAdapter.addFragment(likedPostFragment, "", "nogrid", userId, userName);
            fragmentPageAdapter.addFragment(tagFragment, "", "", userId, userName);
            fragmentPageAdapter.addFragment(channelFragment, "", "", userId, userName);
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
        viewPagerProfile.setAdapter(fragmentPageAdapter);
        //viewPagerProfile.setOffscreenPageLimit(5);

        Drawable drawableOne = getResources().getDrawable(R.drawable.story_on);
        Drawable drawableTwo = getResources().getDrawable(R.drawable.ic_liked_selected);
        Drawable drawableThree = getResources().getDrawable(R.drawable.ic_tag_selected);
        Drawable drawableSix = getResources().getDrawable(R.drawable.ic_channel_black);

        final Drawable[] tabDrawableOn =
                {drawableOne, drawableTwo, drawableThree, drawableSix};

        Drawable drawableOneOff = getResources().getDrawable(R.drawable.story_off);
        Drawable drawableTwoOff = getResources().getDrawable(R.drawable.ic_liked_nonselected);
        Drawable drawableThreeOff = getResources().getDrawable(R.drawable.ic_tag_nonselected);
        Drawable drawableSixOff = getResources().getDrawable(R.drawable.ic_channel_gray);

        final Drawable[] tabDrawableOff = {
                drawableOneOff, drawableTwoOff, drawableThreeOff, drawableSixOff
        };

        tabLayoutProfile.getTabAt(0).setIcon(tabDrawableOn[0]);
        tabLayoutProfile.getTabAt(1).setIcon(tabDrawableOff[1]);
        tabLayoutProfile.getTabAt(2).setIcon(tabDrawableOff[2]);
        tabLayoutProfile.getTabAt(3).setIcon(tabDrawableOff[3]);

        tabLayoutProfile.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                tab.setIcon(tabDrawableOn[tab.getPosition()]);
                sessionManager.setProfileCurrentTab(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                tab.setIcon(tabDrawableOff[tab.getPosition()]);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        isVpSetUp = true;
    }

    @Override
    public void isLoading(boolean flag) {
    }

    @OnClick(R.id.faButton)
    public void fab() {
        startActivity(new Intent(mActivity, AddChannelActivity.class));
    }

    @OnClick(R.id.ivEditProfile)
    public void editProfile() {
        Intent intent = new Intent(mActivity, EditProfileActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("profile_data", profileData);
        intent.putExtra("bundle", bundle);
        intent.putExtra("call", "ProfileActivity");
        startActivity(intent);
    }

    public void setFabButtonVisible(boolean show) {
        //  btnFollow.setVisibility(show ? View.GONE : View.VISIBLE);
        faButton.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    public void setActionBarExpand(boolean expand) {
        appBarLayout.setExpanded(expand, true);
    }

    @Override
    public void showMessage(String msg, int msgId) {
        //        Toast.makeText(this, msg != null && !msg.isEmpty() ? msg : getResources().getString(msgId), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void sessionExpired() {
        sessionManager.sessionExpired(mActivity);
    }

    @Override
    public void isInternetAvailable(boolean flag) {
        //   llNetworkError.setVisibility(flag ? View.GONE : View.VISIBLE);
    }

    @Override
    public void userBlocked() {
        dialog1.show();
    }

    @Override
    public void showProfileData(Profile profile) {
        try {
            userId = profile.getData().get(0).getId();
            showProfile(profile);
        } catch (Exception ignored) {
        }
    }

    private void showProfile(Profile profile) {
        try {
            if (!profile.getData().isEmpty()) {
                ibEditProfile.setEnabled(true);
                followStatus = profile.getData().get(0).getFollowStatus();
                isBlocked = profile.getData().get(0).getBlock() == 1; // 1=blocked
                //this.block.setTitle(getResources().getString(isBlocked ? R.string.Block : R.string.ubBlock));
                isPrivate = profile.getData().get(0).getPrivate().equals("1");
                isFriend = profile.getData().get(0).getFriendStatusCode() == 2;

                //block.setVisible(!AppController.getInstance().getUserId().equals(userId));
                //unfriend.setVisible(!AppController.getInstance().getUserId().equals(userId) && isFriend);

                this.profileData = profile.getData().get(0);

                String fname = profileData.getFirstName();
                String lname = profileData.getLastName();

                sessionManager.setBusinessProfileAvailable(this.profileData.isActiveBussinessProfile());
                sessionManager.setBusinessProfileApproved(profileData.isBusinessProfileApproved());

                if (this.profileData.getBusinessProfiles() != null
                        && this.profileData.getBusinessProfiles().size() > 0) {
                    businessProfile = this.profileData.getBusinessProfiles().get(0);
                    sessionManager.setBusinessCategoryId(businessProfile.getBussinessId());
                    sessionManager.businessProfile(businessProfile != null);
                }

                if (profile.getData().get(0).getId().equals(myid)) {
                    sessionManager.setUserProfilePic(profile.getData().get(0).getProfilePic(), false);
                    sessionManager.setFirstName(fname);
                    sessionManager.setLastsName(lname);
                }

                userName = profileData.getUserName();
                if (!isVpSetUp) setupViewPager();
                //                1)type: { status: 0, message: "unfollowed" }
                //                2)type: { status: 1, message: ' started following' }
                //                3)type: { status: 2, message: 'requested' }
                //                4)type: { status: 3, message: 'reject' }
                boolean isChecked;
                if (btnFollow != null) {

                    switch (profileData.getFollowStatus()) {
                        case 0:
                            //public - unfollow
                            isPrivate = profileData.getPrivate().equals("1");
                            isChecked = false;
                            llPrivate.setVisibility(View.GONE);
                            viewPagerProfile.setVisibility(View.VISIBLE);
                            break;
                        case 1:
                            //public - follow
                            isPrivate = false;
                            isChecked = true;
                            llPrivate.setVisibility(View.GONE);
                            viewPagerProfile.setVisibility(View.VISIBLE);
                            break;
                        case 2:
                            //private - requested
                            isPrivate = true;
                            isChecked = true;
                            llPrivate.setVisibility(View.VISIBLE);
                            viewPagerProfile.setVisibility(View.GONE);
                            break;
                        case 3:
                            //private - request
                            isPrivate = true;
                            isChecked = false;
                            llPrivate.setVisibility(View.VISIBLE);
                            viewPagerProfile.setVisibility(View.GONE);
                            break;
                        default:
                            isChecked = false;
                            break;
                    }
                    btnFollow.setTextOn(
                            getResources().getString(isPrivate ? R.string.requested : R.string.following));
                    btnFollow.setTextOff(getResources().getString(R.string.follow));
                    btnFollow.setChecked(isChecked);
                }

                Data data = profile.getData().get(0);

                Glide.with(mActivity)
                        .load(data.getProfilePic())
                        .asBitmap()
                        .signature(new StringSignature(
                                AppController.getInstance().getSessionManager().getUserProfilePicUpdateTime()))
                        .centerCrop()
                        //.placeholder(R.drawable.profile_one)
                        .into(ivProfile);

                Glide.with(mActivity)
                        .load(
                                data.getProfileCoverImage() != null && !data.getProfileCoverImage().isEmpty() ? data
                                        .getProfileCoverImage() : data.getProfilePic())
                        .asBitmap()
                        .signature(new StringSignature(
                                AppController.getInstance().getSessionManager().getUserProfilePicUpdateTime()))
                        .centerCrop()
                        .into(ivProfileBg);

                String profileStatus = data.getStatus();
                if (profileStatus == null || profileStatus.isEmpty()) {
                    profileStatus = "";
                }
                tvProfileStatus.setText(profileStatus);
                //                tvProfileStatus.setMovementMethod(LinkMovementMethod.getInstance());

                String name = (fname != null ? fname : "") + " " + (lname != null ? lname : "");
                //collapseToolbarLayout.setTitle(name);
                tVTitle.setText(name);
                tvProfileName.setText(profileData.getUserName());
                sessionManager.setIsStar(data.isStar());
                sessionManager.setUserName(profileData.getUserName());
                tvUserName.setText(name);
                tvProfileMob.setText(data.getNumber());
                tvProfileMob.setVisibility(View.GONE);
                tvPostCount.setText(String.valueOf(data.getPostsCount()));
                tvFollowingCount.setText(String.valueOf(data.getFollowing()));
                tvFollowersCount.setText(String.valueOf(data.getFollowers()));

                if (data.isStar()) {
                    iV_star.setVisibility(View.VISIBLE);
                    //                    cV_starVideo.setVisibility(View.VISIBLE);
                    //                    iV_starVideo.setImageDrawable(getResources().getDrawable(R.drawable.trailer1));
                    //                    Glide.with(this)
                    //                            .load("https://res.cloudinary.com/dafszph29/image/upload/v1552033376/default/trailer1.png")
                    //                            .asBitmap()
                    //                            .into(iV_starVideo);

                    if (!profileData.isActiveBussinessProfile() && userId != null && !userId.equals(myid)) {
                        rL_star_connect.setVisibility(View.VISIBLE);
                    } else {
                        rL_star_connect.setVisibility(View.GONE);
                    }

                } else {
                    iV_star.setVisibility(View.GONE);
                    cV_starVideo.setVisibility(View.GONE);
                }

                if (userId != null && !userId.equals(myid)) {
                    btnFollow.setVisibility(View.VISIBLE);
                    llwallet.setVisibility(View.GONE);
                } else {
                    btnFollow.setVisibility(View.GONE);
                    llwallet.setVisibility(View.VISIBLE);
                }
            }

            isBusiness = sessionManager.isBusinessProfileAvailable();
            if (isBusiness) switchToBusiness(isBusiness);
        } catch (
                Exception ignored) {

            ignored.printStackTrace();
        }

    }

    @Override
    public void isFollowing(boolean flag) {
        if (btnFollow != null) {
            if (userId != null && !userId.equals(myid)) {
                presenter.loadMemberData(userId);
            } else {
                presenter.loadProfileData();
            }

            btnFollow.setEnabled(true);
            btnFollow.setChecked(flag);
        }
    }

    @Override
    public void launchCustomCamera() {
    }

    @Override
    public void checkReadImage() {
        if (ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED) {
            presenter.launchImagePicker();
        } else {
            requestReadImagePermission();
        }
    }

    private void requestReadImagePermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(mActivity,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                || ActivityCompat.shouldShowRequestPermissionRationale(mActivity,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

            Snackbar snackbar = Snackbar.make(root, R.string.string_222, Snackbar.LENGTH_INDEFINITE)
                    .setAction(getString(R.string.string_580), new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ActivityCompat.requestPermissions(mActivity, new String[]{
                                    Manifest.permission.READ_EXTERNAL_STORAGE,
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                            }, READ_STORAGE_REQ_CODE);
                        }
                    });
            snackbar.show();
            View view = snackbar.getView();
            ((TextView) view.findViewById(R.id.snackbar_text)).setGravity(Gravity.CENTER_HORIZONTAL);
        } else {
            ActivityCompat.requestPermissions(mActivity, new String[]{
                    Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE
            }, READ_STORAGE_REQ_CODE);
        }
    }

    @Override
    public void launchImagePicker(Intent intent) {
        startActivityForResult(intent, RESULT_LOAD_IMAGE);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        Toast.makeText(mActivity, isConnected ? "Internet connected" : "No internet",
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRefresh() {

    }

    @Override
    public void addToReportList(ArrayList<String> data) {
        arrayAdapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_list_item_1, data);
        //reportDialog.setAdapter(arrayAdapter, mActivity);
    }

    @Override
    public void addToBlockList(ArrayList<String> data) {
        blockReasons = new ArrayAdapter<String>(mActivity, android.R.layout.simple_list_item_1, data);
        blockDialog.setAdapter(blockReasons, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                presenter.block(userId, "block", blockReasons.getItem(i));
            }
        });
    }

    @Override
    public void block(boolean block) {
        try {
            JSONObject obj = new JSONObject();
            obj.put("eventName", "reloadFriends");
            bus.post(obj);
        } catch (Exception ignored) {

        }
        isBlocked = block;
        String message = getResources().getString(block ? R.string.ubBlock : R.string.Block);
        //this.block.setTitle(message);
        Toast.makeText(mActivity, getResources().getString(!block ? R.string.ubBlock : R.string.Block),
                Toast.LENGTH_SHORT).show();

        if (AppController.getInstance().getSignedIn() && AppController.getInstance().profileSaved()) {
            Intent i2 = new Intent(mActivity, LandingActivity.class);
            i2.putExtra("userId", AppController.getInstance().getUserId());
            i2.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(i2);
            //finish();
            //overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
        }
    }

    @Override
    public void unfriend() {
        try {
            JSONObject obj = new JSONObject();
            obj.put("eventName", "reloadFriends");
            bus.post(obj);
        } catch (Exception ignored) {

        }
        loadData();
    }

    @Override
    public void showBalance(WalletResponse.Data.Wallet data) {
        if (data != null && data.getBalance() != null) {

            String balance = sessionManager.getCurrencySymbol() + " " + Utilities.formatMoney(Double.valueOf(sessionManager.getWalletBalance()));
            if (tV_balance != null)
                tV_balance.setText(balance);
            rl_balance.setVisibility(View.VISIBLE);
            sessionManager.setWalletBalance(String.valueOf(data.getBalance()));
//            sessionManager.setCurrencySymbol(data.getCurrencySymbol());
//            sessionManager.setCurrency(data.getCurrency());
        }
    }

    @Override
    public void noProfile(String message) {
        Dialog dialog = new Dialog(mActivity);
        dialog.setCancelable(false);
        Objects.requireNonNull(dialog.getWindow()).requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_profile);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow()
                .setLayout(RelativeLayout.LayoutParams.MATCH_PARENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT);

        TextView tvMessage = dialog.findViewById(R.id.tV_msg);
        tvMessage.setText(message);
        Button ok = dialog.findViewById(R.id.btnOk);
        ok.setOnClickListener(v -> {
            dialog.dismiss();
            //onBackPressed();
        });
        dialog.show();
    }

    @Override
    public void showSnackMsg(int msgId) {
        String msg = getResources().getString(msgId);
        Snackbar snackbar = Snackbar.make(root, "" + msg, Snackbar.LENGTH_SHORT);
        snackbar.show();
        View view = snackbar.getView();
        ((TextView) view.findViewById(R.id.snackbar_text)).setGravity(Gravity.CENTER_HORIZONTAL);
    }

    @Override
    public void launchCropImage(Uri data) {
        CropImage.activity(data).start(mActivity);
    }


    @Override
    public void reload() {
        presenter.init();
    }

    public void moveNext(Integer verificationStatus) {

    }
}
