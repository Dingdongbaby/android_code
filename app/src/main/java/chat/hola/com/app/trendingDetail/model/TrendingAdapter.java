package chat.hola.com.app.trendingDetail.model;

import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.lighthusky.dingdong.R;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import chat.hola.com.app.Utilities.Utilities;
import chat.hola.com.app.home.model.Data;
import chat.hola.com.app.trendingDetail.TrendingDetail;

/**
 * <h>TrendingDtlAdapter</h>
 * <p> This Adapter is used by {@link TrendingDetail} activity recyclerView.</p>
 *
 * @author 3Embed
 * @version 1.0
 * @since 14/2/18.
 */

public class TrendingAdapter extends RecyclerView.Adapter<ViewHolder> {

    private Context context;
    private List<Data> data = new ArrayList<>();
    private ClickListner clickListner;

    @Inject
    public TrendingAdapter(Context context, List<Data> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.trending_detail_item, parent, false), clickListner);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        initHolder(viewHolder);
    }

    private void initHolder(ViewHolder holder) {
        int position = holder.getAdapterPosition();
        Data data = this.data.get(position);
        if (data != null) {
            if (data.getTrending_score() == null)
                data.setTrending_score("Score is not coming");
            holder.tvScore.setText(data.getTrending_score());
            boolean isVideo = data.getMediaType1() != null && data.getMediaType1() == 1;
            holder.ibPlay.setVisibility(isVideo ? View.VISIBLE : View.GONE);
            Glide.with(context).load(Utilities.getModifiedImageLink(Utilities.getModifiedImageLink(data.getImageUrl1()))).asBitmap().centerCrop().into(new BitmapImageViewTarget(holder.ivImage));
        }
    }

    @Override
    public int getItemCount() {
        return data != null ? data.size() : 0;
    }

    public void setListener(ClickListner clickListner) {
        this.clickListner = clickListner;
    }
}