package chat.hola.com.app.profileScreen.editProfile;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import chat.hola.com.app.AppController;
import chat.hola.com.app.Dialog.BlockDialog;
import chat.hola.com.app.Dialog.DatePickerFragment;
import chat.hola.com.app.Dialog.ImageSourcePicker;
import chat.hola.com.app.ImageCropper.CropImage;
import chat.hola.com.app.NumberVerification.ChooseCountry;
import chat.hola.com.app.Utilities.Constants;
import chat.hola.com.app.Utilities.TypefaceManager;
import chat.hola.com.app.Utilities.UploadFileAmazonS3;
import chat.hola.com.app.Utilities.Utilities;
import chat.hola.com.app.authentication.verifyEmail.VerifyEmailActivity;
import chat.hola.com.app.manager.session.SessionManager;
import chat.hola.com.app.profileScreen.editProfile.model.EditProfileBody;
import chat.hola.com.app.profileScreen.model.Data;
import chat.hola.com.app.user_verification.RequestEmailOTPActivity;
import chat.hola.com.app.user_verification.RequestNumberOTPActivity;
import chat.hola.com.app.user_verification.phone.VerifyNumberOTPActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.signature.StringSignature;
import com.lighthusky.dingdong.R;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.squareup.otto.Bus;

import dagger.android.support.DaggerAppCompatActivity;

import javax.inject.Inject;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * <h>EditProfileActivity</h>
 * <p>This fragment allow user to edit their profile detail like username, status etc.</p>
 *
 * @author 3Embed
 * @since 19/2/18.
 */
public class EditProfileActivity extends DaggerAppCompatActivity
        implements EditProfileContract.View, View.OnFocusChangeListener {

    private static final String TAG = EditProfileActivity.class.getSimpleName();
    //constants
    private static final int EDIT_EMAIL_REQ_CODE = 333;
    private static final int STATUS_UPDATE_REQ_CODE = 555;
    private static final int CAMERA_REQ_CODE = 24;
    private static final int READ_STORAGE_REQ_CODE = 26;
    private static final int WRITE_STORAGE_REQ_CODE = 27;

    private static final int RESULT_CAPTURE_PROFILE_IMAGE = 0;
    private static final int RESULT_LOAD_PROFILE_IMAGE = 1;
    private static final int RESULT_CAPTURE_COVER_IMAGE = 2;
    private static final int RESULT_LOAD_COVER_IMAGE = 3;
    private static final int FETCH_ADDRESS = 135;
    private static final int FETCH_CATEGORY = 145;
    private static final int FETCH_COUNTRY = 155;
    private static final int VERIFY_BUSINESS_PHONE = 1122;
    private static final int VERIFY_BUSINESS_EMAIL = 1123;
    private static final int FETCH_BUSINESS_COUNTRY = 156;

    @Inject
    EditProfilePresenter presenter;
    @Inject
    TypefaceManager typefaceManager;
    @Inject
    DatePickerFragment datePickerFragment;
    ImageSourcePicker imageSourcePicker;
    @Inject
    SessionManager sessionManager;
    @Inject
    BlockDialog dialog1;

    @BindView(R.id.btnCancel)
    Button btnCancel;
    @BindView(R.id.btnSave)
    Button btnSave;
    @BindView(R.id.tvEditProfile)
    TextView tvEditProfile;
    @BindView(R.id.btnChangePhoto)
    Button btnChangePhoto;
    @BindView(R.id.flAddProfile)
    FrameLayout flAddProfile;
    @BindView(R.id.ivProfile)
    ImageView ivProfile;
    @BindView(R.id.ivProfileBg)
    ImageView ivCover;
    @BindView(R.id.etName)
    EditText etName;
    @BindView(R.id.etUsername)
    EditText etUsername;
    @BindView(R.id.titleContactNumber)
    TextInputLayout tvContactNumberTitle;
    @BindView(R.id.titleStarContactNumber)
    TextInputLayout titleStarContactNumber;
    @BindView(R.id.etContactNumber)
    TextInputEditText etContactNumber;
    @BindView(R.id.etStaContactNumber)
    TextInputEditText etStaContactNumber;
    @BindView(R.id.tvContactDetail)
    TextView tvContactDeatil;
    @BindView(R.id.titleStatus)
    TextInputLayout titleStatus;
    @BindView(R.id.etStatus)
    TextInputEditText etStatus;
    @BindView(R.id.titleEmail)
    TextInputLayout titleEmail;
    @BindView(R.id.titleBusinessBio)
    TextInputLayout titleBusinessBio;
    @BindView(R.id.titleStarEmail)
    TextInputLayout titleStarEmail;
    @BindView(R.id.etEmail)
    TextInputEditText etEmail;
    @BindView(R.id.etStarEmail)
    TextInputEditText etStarEmail;
    @BindView(R.id.etKnownAs)
    TextInputEditText etKnownAs;
    @BindView(R.id.root)
    RelativeLayout root;
    @BindView(R.id.tilName)
    TextInputLayout tilName;
    @BindView(R.id.tilUserName)
    TextInputLayout tilUserName;
    @BindView(R.id.tilKnownAs)
    TextInputLayout tilKnownAs;
    @BindView(R.id.isPrivate)
    Switch isPrivate;
    @BindView(R.id.llStar)
    LinearLayout llStar;
    @BindView(R.id.pbProfilePic)
    ProgressBar pbProfilePic;

    @BindView(R.id.llBusiness)
    LinearLayout llBusiness;
    @BindView(R.id.llPersonalDetail)
    LinearLayout llPersonalDetail;
    @BindView(R.id.etBusinessBio)
    TextInputEditText etBusinessBio;
    @BindView(R.id.etBusinessCategory)
    TextInputEditText etBusinessCategory;
    @BindView(R.id.etBusinessContactNumber)
    TextInputEditText etBusinessContactNumber;
    @BindView(R.id.etBusinessEmail)
    TextInputEditText etBusinessEmail;
    @BindView(R.id.etBusinessName)
    TextInputEditText etBusinessName;
    @BindView(R.id.etBusinessWebsite)
    TextInputEditText etBusinessWebsite;
    @BindView(R.id.tvBusinessCountryCode)
    TextView tvBusinessCountryCode;

    @BindView(R.id.rL_businessCat)
    RelativeLayout rl_businessCat;
    @BindView(R.id.tV_businessCat)
    TextView tV_businessCat;
    @BindView(R.id.rL_contactOpt)
    RelativeLayout rL_contactOpt;

    private Unbinder unbinder;
    private Data profileData;
    private ProgressDialog dialog;
    private String call = "";
    private Bus bus = AppController.getBus();
    private boolean isProfile;
    private String coverPicture;
    private String profilePicture;
    private boolean isPicChange = false;
    public static Data.BusinessProfile businessProfile;
    private boolean isBusinessEmailVerified;
    private boolean isBusinessPhoneVerified;
    private String verifiedBusinessEmail = "";
    private String verifiedBusinessPhone = "";
    private String businessAddress = "";
    private String businessCountryCode = "";
    private String countryCode = "";
    private int max_digits = 20;
    private int business_max_digits = 20;
    private String businessCategoryId;

    @Override
    public void userBlocked() {
        dialog1.show();
    }

    @SuppressLint({"ClickableViewAccessibility", "SetTextI18n"})
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editprofile);
        unbinder = ButterKnife.bind(this);
        bus.register(this);

        Bundle bundle = getIntent().getBundleExtra("bundle");
        profileData = (Data) bundle.getSerializable("profile_data");
        call = getIntent().getStringExtra("call");

        presenter.init();
        progressBarSetup();

        isPrivate.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isPrivate.setText(isChecked ? "Private" : "Public");
            }
        });

        etStarEmail.setOnTouchListener((v, event) -> {
            final int DRAWABLE_LEFT = 0;
            final int DRAWABLE_TOP = 1;
            final int DRAWABLE_RIGHT = 2;
            final int DRAWABLE_BOTTOM = 3;

            if (event.getAction() == MotionEvent.ACTION_UP) {
                if (event.getRawX() >= (etStarEmail.getRight()
                        - etStarEmail.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {

                    return true;
                }
            }
            return false;
        });

        etStaContactNumber.setOnTouchListener((v, event) -> {
            final int DRAWABLE_LEFT = 0;
            final int DRAWABLE_TOP = 1;
            final int DRAWABLE_RIGHT = 2;
            final int DRAWABLE_BOTTOM = 3;

            if (event.getAction() == MotionEvent.ACTION_UP) {
                if (event.getRawX() >= (etStaContactNumber.getRight()
                        - etStaContactNumber.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {

                    return true;
                }
            }
            return false;
        });

        if (sessionManager.isBusinessProfileAvailable()) {
            businessProfile = profileData.getBusinessProfiles().get(0);
            if (businessProfile != null) {
                titleBusinessBio.setVisibility(View.VISIBLE);
                etBusinessName.setText(businessProfile.getBusinessName());
                etBusinessBio.setText(businessProfile.getBusinessBio());
                etBusinessCategory.setText(businessProfile.getBusinessCategory());
                businessCountryCode = businessProfile.getPhone().getCountryCode();
                tvBusinessCountryCode.setText(businessCountryCode);
                etBusinessContactNumber.setText(businessProfile.getPhone().getNumber());
                etBusinessEmail.setText(businessProfile.getEmail().getId());
                etBusinessName.setText(businessProfile.getBusinessName());
                etBusinessWebsite.setText(businessProfile.getWebsite());

                isBusinessPhoneVerified = businessProfile.getPhone().getVerified() == 1;
                isBusinessEmailVerified = businessProfile.getEmail().getVerified() == 1;
                etBusinessContactNumber.setSelected(isBusinessPhoneVerified);
                etBusinessEmail.setSelected(isBusinessEmailVerified);

                if (isBusinessPhoneVerified) {
                    verifiedBusinessPhone = etBusinessContactNumber.getText().toString().trim();
                }
                if (isBusinessEmailVerified) {
                    verifiedBusinessEmail = etBusinessEmail.getText().toString().trim();
                }

                tV_businessCat.setText(businessProfile.getBusinessCategory());
                businessCategoryId = businessProfile.getBussinessId();
                businessAddress = businessProfile.getAddress();
            }
        }

        setProfileData();

        etBusinessContactNumber.setOnTouchListener((v, event) -> {
            final int DRAWABLE_LEFT = 0;
            final int DRAWABLE_TOP = 1;
            final int DRAWABLE_RIGHT = 2;
            final int DRAWABLE_BOTTOM = 3;

            if (event.getAction() == MotionEvent.ACTION_UP) {
                if (event.getRawX() >= (etBusinessContactNumber.getRight()
                        - etBusinessContactNumber.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                    verifyMobileNumber();
                    return true;
                }
            }
            return false;
        });

        etBusinessEmail.setOnTouchListener((v, event) -> {
            final int DRAWABLE_LEFT = 0;
            final int DRAWABLE_TOP = 1;
            final int DRAWABLE_RIGHT = 2;
            final int DRAWABLE_BOTTOM = 3;

            if (event.getAction() == MotionEvent.ACTION_UP) {
                if (event.getRawX() >= (etBusinessEmail.getRight()
                        - etBusinessEmail.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                    verifyEmail();
                    return true;
                }
            }
            return false;
        });

        etBusinessContactNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String number =
                        businessCountryCode + "" + etBusinessContactNumber.getText().toString().trim();
                etBusinessContactNumber.setSelected(number.equals(verifiedBusinessPhone));
            }
        });

        etBusinessEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String email = etBusinessEmail.getText().toString().trim();
                etBusinessEmail.setSelected(email.equals(verifiedBusinessEmail));
            }
        });
    }

    /**
     * It will open business contact option page.
     */
    @OnClick(R.id.rL_contactOpt)
    public void clickBusinessContactOptions() {
    }

    @OnClick({R.id.rL_businessCat})
    public void businessCategory() {
    }

    @OnClick(R.id.tvBusinessCountryCode)
    public void getBusinessCountryCode() {
        Intent intent = new Intent(this, ChooseCountry.class);
        startActivityForResult(intent, FETCH_BUSINESS_COUNTRY);
    }

    /**
     * Verifies the business email address
     */
    private void verifyEmail() {
        String email = etBusinessEmail.getText().toString().trim();
        if (Utilities.isValidEmail(email)) {
            presenter.businessEmailVerificationCode(email);
        } else {
            showMessage(null, R.string.enter_valid_email);
        }
    }

    /**
     * Verifies the business mobile number
     */
    private void verifyMobileNumber() {
        String phone = etBusinessContactNumber.getText().toString().trim();
        if (!phone.isEmpty() && businessCountryCode != null) {
            presenter.businessPhoneVerificationCode(businessCountryCode, phone);
        } else {
            showMessage(null, R.string.enter_valid_phone_countrycode);
        }
    }

    private void progressBarSetup() {
        dialog = new ProgressDialog(this);
        dialog.setMessage(getResources().getString(R.string.editProfileProgressMsg));
        dialog.setCancelable(false);
    }

    private void setProfileData() {
        if (profileData != null) {

            llPersonalDetail.setVisibility(
                    !profileData.isActiveBussinessProfile() ? View.VISIBLE : View.GONE);
            llBusiness.setVisibility(profileData.isActiveBussinessProfile() ? View.VISIBLE : View.GONE);
            llStar.setVisibility(
                    profileData.isStar() && !profileData.isActiveBussinessProfile() ? View.VISIBLE
                            : View.GONE);

            etUsername.setText(profileData.getUserName());
            String firstName = profileData.getFirstName();
            String lastName = profileData.getLastName();
            String name =
                    ((firstName == null) ? "" : firstName) + ((lastName == null) ? "" : " " + lastName);
            etName.setText(name);
            etStatus.setText(profileData.getStatus());
            etEmail.setText(profileData.getEmail());
            etEmail.setKeyListener(null);
            etContactNumber.setText(profileData.getNumber());

            if (profileData.isActiveBussinessProfile()) {
                isPrivate.setChecked(false);
                isPrivate.setEnabled(false);
            }

            isPrivate.setChecked(profileData.getPrivate().equals("1"));
            isPrivate.setText(isPrivate.isChecked() ? "Private" : "Public");

            if (businessProfile != null && profileData.isActiveBussinessProfile()) {
                profilePicture = businessProfile.getBusinessProfilePic();
                coverPicture = businessProfile.getBusinessProfileCoverImage();
                Glide.with(getBaseContext())
                        .load(businessProfile.getBusinessProfilePic())
                        .asBitmap()
                        .signature(new StringSignature(
                                AppController.getInstance().getSessionManager().getUserProfilePicUpdateTime()))
                        .centerCrop()
                        //.signature(new StringSignature(String.valueOf(System.currentTimeMillis())))
                        .into(ivProfile);
                Glide.with(getBaseContext())
                        .load(businessProfile.getBusinessProfileCoverImage())
                        .asBitmap()
                        .signature(new StringSignature(
                                AppController.getInstance().getSessionManager().getUserProfilePicUpdateTime()))
                        .centerCrop()
                        //.signature(new StringSignature(String.valueOf(System.currentTimeMillis())))
                        .into(ivCover);
            } else {
                profilePicture = profileData.getProfilePic();
                coverPicture = profileData.getProfileCoverImage();
                Glide.with(getBaseContext())
                        .load(profileData.getProfilePic())
                        .asBitmap()
                        .centerCrop()
                        //.signature(new StringSignature(String.valueOf(System.currentTimeMillis())))
                        .signature(new StringSignature(
                                AppController.getInstance().getSessionManager().getUserProfilePicUpdateTime()))
                        .into(ivProfile);
                Glide.with(getBaseContext())
                        .load(profileData.getProfileCoverImage())
                        .asBitmap()
                        .signature(new StringSignature(
                                AppController.getInstance().getSessionManager().getUserProfilePicUpdateTime()))
                        .centerCrop()
                        //.signature(new StringSignature(String.valueOf(System.currentTimeMillis())))
                        .into(ivCover);
            }
            etEmail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(EditProfileActivity.this, RequestEmailOTPActivity.class);
                    intent.putExtra("profileData", profileData);
                    intent.putExtra("type", "3");
                    startActivityForResult(intent, Constants.Verification.EMAIL_VERIFICATION_REQ);
                }
            });

            etContactNumber.setOnKeyListener(null);
            etContactNumber.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(EditProfileActivity.this, RequestNumberOTPActivity.class);
                    intent.putExtra("profileData", profileData);
                    startActivityForResult(intent, Constants.Verification.NUMBER_VERIFICATION_REQ);
                }
            });
        } else {

        }

        if (call != null) etName.setSelection(etName.getText().toString().trim().length());
    }

    @Override
    public void applyFont() {
        btnCancel.setTypeface(typefaceManager.getSemiboldFont());
        btnSave.setTypeface(typefaceManager.getSemiboldFont());
        tvEditProfile.setTypeface(typefaceManager.getSemiboldFont());
        btnChangePhoto.setTypeface(typefaceManager.getMediumFont());
        tvContactDeatil.setTypeface(typefaceManager.getMediumFont());
        tvContactNumberTitle.setTypeface(typefaceManager.getRegularFont());
        titleStarContactNumber.setTypeface(typefaceManager.getRegularFont());
        tilName.setTypeface(typefaceManager.getMediumFont());
        tilUserName.setTypeface(typefaceManager.getMediumFont());
        tilKnownAs.setTypeface(typefaceManager.getMediumFont());
        etName.setTypeface(typefaceManager.getMediumFont());
        etName.setOnFocusChangeListener(this);
        etUsername.setTypeface(typefaceManager.getMediumFont());
        etUsername.setOnFocusChangeListener(this);
        titleStatus.setTypeface(typefaceManager.getMediumFont());
        etStatus.setTypeface(typefaceManager.getMediumFont());
        etStatus.setOnFocusChangeListener(this);
        titleEmail.setTypeface(typefaceManager.getMediumFont());
        titleStarEmail.setTypeface(typefaceManager.getMediumFont());
        etEmail.setTypeface(typefaceManager.getMediumFont());
        etStarEmail.setTypeface(typefaceManager.getMediumFont());
        etKnownAs.setTypeface(typefaceManager.getMediumFont());
        etEmail.setOnFocusChangeListener(this);
        etContactNumber.setTypeface(typefaceManager.getMediumFont());
        etStaContactNumber.setTypeface(typefaceManager.getMediumFont());
        isPrivate.setTypeface(typefaceManager.getMediumFont());

        etBusinessBio.setTypeface(typefaceManager.getMediumFont());
        etBusinessCategory.setTypeface(typefaceManager.getMediumFont());
        etBusinessContactNumber.setTypeface(typefaceManager.getMediumFont());
        etBusinessEmail.setTypeface(typefaceManager.getMediumFont());
        etBusinessName.setTypeface(typefaceManager.getMediumFont());
        etBusinessWebsite.setTypeface(typefaceManager.getMediumFont());
        tvBusinessCountryCode.setTypeface(typefaceManager.getMediumFont());
    }

    @Override
    public void finishActivity(boolean success, boolean imageUpdated) {

        Intent intent = new Intent();

        if (success) {
            JSONObject obj = new JSONObject();
            try {
                obj.put("eventName", "profileUpdated");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            bus.post(obj);
            if (imageUpdated) {
                AppController.getInstance().getSessionManager().setUserProfilePicUpdateTime();
            }

            intent.putExtra("imageUpdated", imageUpdated);
            setResult(RESULT_OK, intent);
        } else {
            setResult(RESULT_CANCELED, intent);
        }

        finish();
    }

    @Override
    public void showProgress(boolean show) {
        try {
            if (show && dialog != null && !dialog.isShowing()) {
                dialog.show();
            } else if (!show && dialog != null && dialog.isShowing()) dialog.dismiss();
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Override
    public void setEmail(String email) {
        etEmail.setText(email);
        titleEmail.setVisibility(email != null && TextUtils.isEmpty(email) ? View.VISIBLE : View.GONE);
    }

    @Override
    public void launchCamera(Intent intent, boolean isProfile) {
        startActivityForResult(intent,
                isProfile ? RESULT_CAPTURE_PROFILE_IMAGE : RESULT_CAPTURE_COVER_IMAGE);
    }

    @Override
    public void launchImagePicker(Intent intent, boolean isProfile) {
        startActivityForResult(intent, isProfile ? RESULT_LOAD_PROFILE_IMAGE : RESULT_LOAD_COVER_IMAGE);
    }

    @OnClick({R.id.ivProfile, R.id.ivAdd, R.id.btnChangePhoto})
    void addProfile() {
        isProfile = true;
        imageSourcePicker = new ImageSourcePicker(this, true, true);
        imageSourcePicker.setOnSelectImage(callbackProfile);
        imageSourcePicker.show();
    }

    @OnClick(R.id.ivChangeCover)
    void changeCover() {
        isProfile = false;
        imageSourcePicker = new ImageSourcePicker(this, true, false);
        imageSourcePicker.setOnSelectImageSource(callbackCover);
        imageSourcePicker.show();
    }

    ImageSourcePicker.OnSelectImageSource callbackCover =
            new ImageSourcePicker.OnSelectImageSource() {
                @Override
                public void onCamera() {
                    checkCameraPermissionImage(true);
                }

                @Override
                public void onGallary() {
                    checkReadImage(true);
                }

                @Override
                public void onCancel() {
                    //nothing to do.
                }
            };

    ImageSourcePicker.OnSelectImage callbackProfile = new ImageSourcePicker.OnSelectImage() {
        //        @Override
        //        public void onRemove() {
        //            profilePicture = Constants.DEFAULT_PROFILE_PIC_LINK;
        //            Glide.with(getBaseContext()).load(profilePicture).asBitmap().centerCrop().into(ivProfile);
        //        }

        @Override
        public void onCamera() {
            checkCameraPermissionImage(false);
        }

        @Override
        public void onGallary() {
            checkReadImage(false);
        }

        @Override
        public void onCancel() {
            //nothing to do.
        }
    };

    @OnClick(R.id.btnCancel)
    public void cancel() {
        onBackPressed();
    }

    @OnClick(R.id.btnSave)
    public void save() {
        //need to call some api method
        EditProfileBody editProfileBody = new EditProfileBody();
        String[] name = etName.getText().toString().split("\\s+");
        editProfileBody.setFirstName((name[0] == null) ? "" : name[0]);
        editProfileBody.setLastName(name.length > 1 ? (name[1] == null) ? "" : name[1] : "");
        editProfileBody.setUserName(etUsername.getText().toString());
        editProfileBody.setKnownAs(etKnownAs.getText().toString());
        String status = etStatus.getText().toString();
        editProfileBody.setStatus(
                (status.isEmpty()) ? getResources().getString(R.string.default_status) : status);
        editProfileBody.setEmail(etEmail.getText().toString());
        editProfileBody.set_private(isPrivate.isChecked() ? 1 : 0);
        editProfileBody.setImgUrl(profilePicture);
        editProfileBody.setProfileCoverImage(coverPicture);

        if (businessProfile != null) {
            editProfileBody.setBusinessCategoryId(businessProfile.getBussinessId());
            editProfileBody.setBusinessAddress(businessProfile.getAddress());
            editProfileBody.setBusinessEmail(businessProfile.getEmail().getId());
            editProfileBody.setBusinessCountryCode(businessProfile.getPhone().getCountryCode());
            editProfileBody.setBusinessPhone(businessProfile.getPhone().getNumber());
            editProfileBody.setBusinessName(businessProfile.getBusinessName());
            editProfileBody.setBusinessBio(businessProfile.getBusinessBio());
            editProfileBody.setBusinessWebsite(businessProfile.getWebsite());
            editProfileBody.setBusinessStreet(businessProfile.getBusinessStreet());
            editProfileBody.setBusinessCity(businessProfile.getBusinessCity());
            editProfileBody.setBusinessZipCode(businessProfile.getBusinessZipCode());
            editProfileBody.setBusinessLat(businessProfile.getBusinessLat());
            editProfileBody.setBusinessLng(businessProfile.getBusinessLng());
            //        editProfileBody.setBusinessWebsite(etBusinessWebsite.getText().toString());
            //        editProfileBody.setBusinessAddress(etBusinessAddress.getText().toString());
            editProfileBody.setBusinessName(etBusinessName.getText().toString());
            editProfileBody.setBusinessBio(businessProfile.getBusinessBio());
            //        if (businessCategoryId != null)
            //            editProfileBody.setBusinessCategoryId(businessCategoryId);
            //        editProfileBody.setBusinessCountryCode(businessCountryCode);
            //        editProfileBody.setBusinessPhone(etBusinessContactNumber.getText().toString());
            //        editProfileBody.setBusinessEmail(etBusinessEmail.getText().toString());
        }
        UploadFileAmazonS3 amazonS3 = new UploadFileAmazonS3(this);
        presenter.initUpdateProfile(editProfileBody, profileData, amazonS3, isPicChange,
                profileData.isActiveBussinessProfile());
    }

    @OnClick(R.id.etBusinessBio)
    public void businessBio() {
        startActivityForResult(new Intent(this, BusinessBioActivity.class).putExtra("bio",
                businessProfile.getBusinessBio()), 888);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void showMessage(String msg, int msgId) {
        if (msg != null && !msg.isEmpty()) {
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        } else if (msgId != 0) {
            Toast.makeText(this, getResources().getString(msgId), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void showSnackMsg(int msgId) {
        String msg = getResources().getString(msgId);
        Snackbar snackbar = Snackbar.make(root, "" + msg, Snackbar.LENGTH_SHORT);
        snackbar.show();
        View view = snackbar.getView();
        ((TextView) view.findViewById(R.id.snackbar_text)).setGravity(Gravity.CENTER_HORIZONTAL);
    }

    @Override
    public void launchCropImage(Uri data) {
        //        .setCropShape(isProfile ? CropImageView.CropShape.OVAL : CropImageView.CropShape.RECTANGLE)
        CropImage.activity(data)
                .setFixAspectRatio(true)
                .setAspectRatio(isProfile ? 100 : 300, isProfile ? 100 : 200)
                .start(this);
    }

    @Override
    public void setProfileImage(Bitmap bitmap) {
        ivProfile.setImageBitmap(bitmap);
    }

    @Override
    public void setCover(Bitmap bitmap) {
        ivCover.setImageBitmap(bitmap);
    }

    @Override
    public void saveEnable(boolean enable) {
        pbProfilePic.setVisibility(!enable ? View.VISIBLE : View.GONE);
        btnSave.setEnabled(enable);
        btnSave.setVisibility(enable ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public void setProfilePic(String profilePath) {
        profilePicture = profilePath;
        sessionManager.setUserProfilePic(profilePath, false);
        try {
            Glide.with(getBaseContext())
                    .load(profilePicture)
                    .asBitmap()
                    .signature(new StringSignature(
                            AppController.getInstance().getSessionManager().getUserProfilePicUpdateTime()))
                    .centerCrop()
                    .into(ivProfile);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void verifyMobile() {
        Intent intent = new Intent(this, VerifyNumberOTPActivity.class);
        intent.putExtra("call", Constants.BUSINESS);
        intent.putExtra("countryCode", businessCountryCode);
        intent.putExtra("phoneNumber", etBusinessContactNumber.getText().toString().trim());
        startActivityForResult(intent, VERIFY_BUSINESS_PHONE);
    }

    @Override
    public void verifyEmailAddress() {
        Intent intent = new Intent(this, VerifyEmailActivity.class);
        intent.putExtra("call", Constants.BUSINESS);
        intent.putExtra("email", etBusinessEmail.getText().toString());
        startActivityForResult(intent, VERIFY_BUSINESS_EMAIL);
    }

    @Override
    public void sessionExpired() {
        sessionManager.sessionExpired(getApplicationContext());
    }

    @Override
    public void isInternetAvailable(boolean flag) {
        if (!flag) Toast.makeText(this, "No Internet", Toast.LENGTH_SHORT).show();
    }

    private void checkCameraPermissionImage(boolean isProfile) {
        if (ActivityCompat.checkSelfPermission(EditProfileActivity
                .this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.checkSelfPermission(EditProfileActivity.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

                presenter.launchCamera(getPackageManager(), isProfile);
            } else {
                /*
                 *permission required to save the image captured
                 */
                requestReadImagePermission(0);
            }
        } else {
            requestCameraPermissionImage();
        }
    }

    private void requestCameraPermissionImage() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(EditProfileActivity.this,
                Manifest.permission.CAMERA)) {

            Snackbar snackbar = Snackbar.make(root, R.string.string_221, Snackbar.LENGTH_INDEFINITE)
                    .setAction(getString(R.string.string_580), new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ActivityCompat.requestPermissions(EditProfileActivity.this,
                                    new String[]{Manifest.permission.CAMERA}, CAMERA_REQ_CODE);
                        }
                    });
            snackbar.show();
            View view = snackbar.getView();
            ((TextView) view.findViewById(R.id.snackbar_text)).setGravity(Gravity.CENTER_HORIZONTAL);
        } else {
            ActivityCompat.requestPermissions(EditProfileActivity.this,
                    new String[]{Manifest.permission.CAMERA}, CAMERA_REQ_CODE);
        }
    }

    private void checkReadImage(boolean isProfile) {
        if (ActivityCompat.checkSelfPermission(EditProfileActivity.this,
                Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(EditProfileActivity.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            presenter.launchImagePicker(isProfile);
        } else {
            requestReadImagePermission(1);
        }
    }

    private void requestReadImagePermission(int k) {
        if (k == 1) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(EditProfileActivity.this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)
                    || ActivityCompat.shouldShowRequestPermissionRationale(EditProfileActivity.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                Snackbar snackbar = Snackbar.make(root, R.string.string_222, Snackbar.LENGTH_INDEFINITE)
                        .setAction(getString(R.string.string_580), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                ActivityCompat.requestPermissions(EditProfileActivity.this, new String[]{
                                        Manifest.permission.READ_EXTERNAL_STORAGE,
                                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                                }, READ_STORAGE_REQ_CODE);
                            }
                        });
                snackbar.show();
                View view = snackbar.getView();
                ((TextView) view.findViewById(R.id.snackbar_text)).setGravity(Gravity.CENTER_HORIZONTAL);
            } else {
                ActivityCompat.requestPermissions(EditProfileActivity.this, new String[]{
                        Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE
                }, READ_STORAGE_REQ_CODE);
            }
        } else if (k == 0) {
            /*
             * For capturing the image permission
             */
            if (ActivityCompat.shouldShowRequestPermissionRationale(EditProfileActivity.this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)
                    || ActivityCompat.shouldShowRequestPermissionRationale(EditProfileActivity.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                Snackbar snackbar = Snackbar.make(root, R.string.string_1218, Snackbar.LENGTH_INDEFINITE)
                        .setAction(getString(R.string.string_580), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                ActivityCompat.requestPermissions(EditProfileActivity.this, new String[]{
                                        Manifest.permission.READ_EXTERNAL_STORAGE,
                                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                                }, WRITE_STORAGE_REQ_CODE);
                            }
                        });

                snackbar.show();
                View view = snackbar.getView();
                ((TextView) view.findViewById(R.id.snackbar_text)).setGravity(Gravity.CENTER_HORIZONTAL);
            } else {
                ActivityCompat.requestPermissions(EditProfileActivity.this, new String[]{
                        Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE
                }, WRITE_STORAGE_REQ_CODE);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == 1001) {
            /*Contact option result*/
            businessProfile = (Data.BusinessProfile) data.getSerializableExtra("businessProfile");
        }

        switch (requestCode){
            case 551:
                if (data!=null && data.getBooleanExtra("emailVerified", false)) {
                    etStarEmail.setText(data.getStringExtra("email"));
                }
                break;
            case 888:
                if(data!=null) {
                    String bio = data.getStringExtra("bio");
                    businessProfile.setBusinessBio(bio);
                    etBusinessBio.setText(bio);
                }
                break;
            case EDIT_EMAIL_REQ_CODE:
                if(data!=null) {
                    String newEmail = data.getStringExtra("email");
                    if (newEmail != null && !newEmail.isEmpty()) {
                        etEmail.setText(newEmail);
                    } else {
                        etEmail.setText(getResources().getString(R.string.enterEmail));
                    }
                }

                break;

            case Constants.Verification.EMAIL_VERIFICATION_REQ:
                if (data!=null && data.getBooleanExtra("emailVerified", true)) {
                    String email = data.getStringExtra("email");
                    etEmail.setText(email);
                }
                break;

            case Constants.Verification.NUMBER_VERIFICATION_REQ:
                if (data!=null && data.getBooleanExtra("numberVerified", true)) {
                    String phoneNumber = data.getStringExtra("phoneNumber");
                    etContactNumber.setText(phoneNumber);
                }
                break;
            case STATUS_UPDATE_REQ_CODE:
                if(data!=null) {
                    String newStatus = data.getStringExtra("updatedValue");
                    if (newStatus != null && !newStatus.isEmpty()) {
                        etStatus.setText(newStatus);
                    } else {
                        etStatus.setText(getResources().getString(R.string.default_status));
                    }
                }
                break;



            case FETCH_ADDRESS:
                //                    if (data != null) {
                //                        city = data.getStringExtra("city");
                //                        street = data.getStringExtra("street");
                //                        zipcode = data.getStringExtra("zipcode");
                //                        String address = street + "," + city + "-" + zipcode;
                //                        etAddress.setText(address);
                //                    }
                break;
            case FETCH_CATEGORY:
                if (data != null) {
                    String categoryId = data.getStringExtra("category_id");
                    String category = data.getStringExtra("category");
                    tV_businessCat.setText(category);
                    businessProfile.setBusinessCategory(category);
                    businessProfile.setBussinessId(categoryId);
                    System.out.println("updated business profile" + businessProfile.toString());
                }
                break;
            case FETCH_BUSINESS_COUNTRY:
                if (data != null) {
                    String code = data.getStringExtra("CODE");
                    int flag = data.getIntExtra("FLAG", R.drawable.flag_in);
                    //                        ivFlag.setImageResource(flag);

                    //                        business_max_digits = data.getIntExtra("MAX", 6);
                    businessCountryCode = "+" + code.substring(1);
                    tvBusinessCountryCode.setText(businessCountryCode);
                    etBusinessContactNumber.setText(businessProfile.getPhone().getNumber());
                    etBusinessContactNumber.setFilters(
                            new InputFilter[]{new InputFilter.LengthFilter(business_max_digits)});

                    String number = countryCode + etBusinessContactNumber.getText().toString().trim();
                    etBusinessContactNumber.setSelected(number.equals(verifiedBusinessPhone));
                }
                break;
            case FETCH_COUNTRY:
                if (data != null) {
                    String code = data.getStringExtra("CODE");
                    int flag = data.getIntExtra("FLAG", R.drawable.flag_in);
                    //                        ivFlag.setImageResource(flag);

                    //                        max_digits = data.getIntExtra("MAX", 6);
                    countryCode = "+" + code.substring(1);
                    etContactNumber.setText(sessionManager.getMobileNumber().replace(countryCode, ""));
                    etContactNumber.setFilters(
                            new InputFilter[]{new InputFilter.LengthFilter(max_digits)});
                }
                break;
            case VERIFY_BUSINESS_EMAIL:
                verifiedBusinessEmail = etBusinessEmail.getText().toString().trim();
                isBusinessEmailVerified = true;
                etBusinessEmail.setSelected(isBusinessEmailVerified);
                break;
            case VERIFY_BUSINESS_PHONE:
                verifiedBusinessPhone =
                        businessCountryCode + etBusinessContactNumber.getText().toString().trim();
                isBusinessPhoneVerified = true;
                etBusinessContactNumber.setSelected(isBusinessPhoneVerified);
                break;
        }

        if (resultCode == RESULT_OK) {
            switch (requestCode) {

                case RESULT_LOAD_PROFILE_IMAGE:
                case RESULT_LOAD_COVER_IMAGE:
                    presenter.parseSelectedImage(requestCode, resultCode, data);
                    break;

                case RESULT_CAPTURE_PROFILE_IMAGE:
                case RESULT_CAPTURE_COVER_IMAGE:
                    presenter.parseCapturedImage(requestCode, resultCode, data);
                    break;

                case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
                    isPicChange = true;
                    presenter.parseCropedImage(requestCode, resultCode, data, isProfile);
                    break;
            }
        }
    }

    /**
     * Result of the permission request
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == CAMERA_REQ_CODE) {

            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.checkSelfPermission(EditProfileActivity.this, Manifest.permission.CAMERA)
                        == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(EditProfileActivity.this,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        presenter.launchCamera(getPackageManager(), isProfile);
                    } else {
                        requestReadImagePermission(0);
                    }
                } else {
                    //camera permission denied msg
                    showSnackMsg(R.string.string_62);
                }
            } else {
                //camera permission denied msg
                showSnackMsg(R.string.string_62);
            }
        } else if (requestCode == READ_STORAGE_REQ_CODE) {

            if (grantResults.length == 2
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.checkSelfPermission(EditProfileActivity.this,
                        Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    presenter.launchImagePicker(isProfile);
                } else {
                    //Access storage permission denied
                    showSnackMsg(R.string.string_1006);
                }
            } else {
                //Access storage permission denied
                showSnackMsg(R.string.string_1006);
            }
        } else if (requestCode == WRITE_STORAGE_REQ_CODE) {
            if (grantResults.length == 2
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.checkSelfPermission(EditProfileActivity.this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

                    presenter.launchCamera(getPackageManager(), isProfile);
                } else {
                    //Access storage permission denied
                    showSnackMsg(R.string.string_1006);
                }
            }
        }
    }

    @Override
    protected void onDestroy() {
        bus.unregister(this);
        super.onDestroy();
        if (unbinder != null) unbinder.unbind();
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        switch (view.getId()) {
            case R.id.etName:
                etName.setSelection(etName.getText().toString().trim().length());
                break;
            case R.id.etUsername:
                etUsername.setSelection(etUsername.getText().toString().trim().length());
                break;
            case R.id.etStatus:
                etStatus.setSelection(etStatus.getText().toString().trim().length());
                break;
            case R.id.etEmail:
                etStatus.setSelection(etEmail.getText().toString().trim().length());
                break;
            case R.id.etContactNumber:
                etContactNumber.setSelection(etContactNumber.getText().toString().trim().length());
                break;
        }
    }

    @Override
    public void reload() {

    }
}
