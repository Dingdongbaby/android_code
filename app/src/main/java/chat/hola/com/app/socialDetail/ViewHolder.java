package chat.hola.com.app.socialDetail;

import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageButton;
import com.lighthusky.dingdong.R;
import com.volokh.danylo.video_player_manager.ui.VideoPlayerView;

import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import chat.hola.com.app.Utilities.ExpandableTextView;

/**
 * <h1></h1>
 *
 * @author DELL
 * @version 1.0
 * @since 11/27/2018.
 */public class ViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.player)
    public VideoPlayerView mPlayer;
    @BindView(R.id.cover)
    public ImageView mCover;
    @BindView(R.id.tvUserName)
    public TextView tvUserName;
    @BindView(R.id.tvTitle)
    public TextView tvTitle;
    @BindView(R.id.tvLocation)
    public TextView tvLocation;
    @BindView(R.id.tvCategory)
    public TextView tvCategory;
    @BindView(R.id.tvChannel)
    public TextView tvChannel;
    @BindView(R.id.ivProfilePic)
    public ImageView ivProfilePic;
    @BindView(R.id.ibLike)
    public AppCompatImageButton ibLike;
    @BindView(R.id.ivComment)
    public ImageView ivComment;
    @BindView(R.id.ivShare)
    public ImageView ivShare;
    @BindView(R.id.ivView)
    public ImageView ivView;
    @BindView(R.id.tvLikeCount)
    public TextView tvLikeCount;
    @BindView(R.id.tvCommentCount)
    public TextView tvCommentCount;
    @BindView(R.id.tvViewCount)
    public TextView tvViewCount;
    @BindView(R.id.flMediaContainer)
    public RelativeLayout flMediaContainer;
    @BindView(R.id.vBgLike)
    public View vBgLike;
    @BindView(R.id.ivLikeIt)
    public ImageView ivLikeIt;
    @BindView(R.id.ivStarBadge)
    public ImageView ivStarBadge;
    @BindView(R.id.tvMusic)
    public TextView tvMusic;
    @BindView(R.id.llMusic)
    public RelativeLayout llMusic;
    @BindView(R.id.tvDivide)
    public TextView tvDivide;
    @BindView(R.id.ibFollow)
    public AppCompatImageButton ibFollow;
    @BindView(R.id.ibMenu)
    public ImageButton ibMenu;
    @BindView(R.id.ibShare)
    public ImageButton ibShare;
    @BindView(R.id.llFabContainer)
    public LinearLayout llFabContainer;

    @BindView(R.id.rlAction)
    public RelativeLayout rlAction;
    @BindView(R.id.tvActionText)
    public TextView tvActionButton;
    @BindView(R.id.tvPrice)
    public TextView tvPrice;

    @BindView(R.id.rL_save)
    public RelativeLayout rL_save;
    @BindView(R.id.tV_saveTo)
    public TextView tV_saveTo;
    @BindView(R.id.tV_savedView)
    public TextView tV_savedView;
    @BindView(R.id.ibSaved)
    public ImageView ibSaved;


    public ViewHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }


}

