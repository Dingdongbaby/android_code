package chat.hola.com.app.models;

import android.annotation.SuppressLint;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.observables.ConnectableObservable;

/**
 * <h1></h1>
 *
 * @author DELL
 * @version 1.0
 * @since 5/24/2018.
 */

public class PostObserver {
    private ConnectableObservable<Boolean> connectableObservable;
    private ObservableEmitter<Boolean> emitor;
    private ConnectableObservable<PostUpdateData> likeUnlikeConnectableObservable;
    private ObservableEmitter<PostUpdateData> likeUnlikeObservableEmitter;
    private ConnectableObservable<PostUpdateData> savedConnectableObservable;
    private ObservableEmitter<PostUpdateData> savedObservableEmitter;
    private ConnectableObservable<PostUpdateData> deleteConnectableObservable;
    private ObservableEmitter<PostUpdateData> deleteObservableEmitter;
    private ConnectableObservable<PostUpdateData> updateConnectableObservable;
    private ObservableEmitter<PostUpdateData> updateObservableEmitter;


    @SuppressLint("CheckResult")
    public PostObserver() {

        Observable<Boolean> observable = Observable.create(e -> emitor = e);

        Observable<PostUpdateData> observable1 = Observable.create(e -> likeUnlikeObservableEmitter = e);

        Observable<PostUpdateData> observable2 = Observable.create(e -> savedObservableEmitter = e);

        Observable<PostUpdateData> observable3 = Observable.create(e -> deleteObservableEmitter = e);

        Observable<PostUpdateData> observable4 = Observable.create(e -> updateObservableEmitter = e);

        connectableObservable = observable.publish();
        connectableObservable.share();
        connectableObservable.replay();
        connectableObservable.connect();

        likeUnlikeConnectableObservable = observable1.publish();
        likeUnlikeConnectableObservable.share();
        likeUnlikeConnectableObservable.replay();
        likeUnlikeConnectableObservable.connect();

        savedConnectableObservable = observable2.publish();
        savedConnectableObservable.share();
        savedConnectableObservable.replay();
        savedConnectableObservable.connect();

        deleteConnectableObservable = observable3.publish();
        deleteConnectableObservable.share();
        deleteConnectableObservable.replay();
        deleteConnectableObservable.connect();

        updateConnectableObservable = observable4.publish();
        updateConnectableObservable.share();
        updateConnectableObservable.replay();
        updateConnectableObservable.connect();
    }

    public ConnectableObservable<Boolean> getObservable() {
        return connectableObservable;
    }

    public ConnectableObservable<PostUpdateData> getLikeUnlikeObservable() {
        return likeUnlikeConnectableObservable;
    }

    public ConnectableObservable<PostUpdateData> getSavedObservable() {
        return savedConnectableObservable;
    }

    public ConnectableObservable<PostUpdateData> getDeleteObservable() {
        return deleteConnectableObservable;
    }

    public ConnectableObservable<PostUpdateData> getUpdateObservable() {
        return updateConnectableObservable;
    }

    public void postData(Boolean flag) {
        if (emitor != null) {
            emitor.onNext(flag);
        }
    }

    public void postLikeUnlikeObject(PostUpdateData data){
        if(likeUnlikeObservableEmitter != null){
            likeUnlikeObservableEmitter.onNext(data);
        }
    }

    public void postSavedUpdate(PostUpdateData data){
        if(savedObservableEmitter != null){
            savedObservableEmitter.onNext(data);
        }
    }

    public void postDeleteUpdate(PostUpdateData data){
        if(deleteObservableEmitter != null){
            deleteObservableEmitter.onNext(data);
        }
    }

    public void postEditUpdate(PostUpdateData data){
        if(updateObservableEmitter != null){
            updateObservableEmitter.onNext(data);
        }
    }

}
