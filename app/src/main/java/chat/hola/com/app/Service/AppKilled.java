package chat.hola.com.app.Service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import chat.hola.com.app.AppController;


/**
 * Created by moda on 21/06/17.
 */

public class AppKilled extends Service {


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }


    public void onTaskRemoved(Intent rootIntent) {


        if (AppController.getInstance().isActiveOnACall()) {
/*
 *If active on a call and the app is slided off,then have to make myself available
 */




        }


        AppController.getInstance().setApplicationKilled(true);



        /*
         * For sticky service not restarting on KITKAT devices
         */



        stopSelf();
    }
}