package chat.hola.com.app.home.activity.followingTab;

import android.os.Handler;

import javax.inject.Inject;

import androidx.annotation.Nullable;
import chat.hola.com.app.AppController;
import chat.hola.com.app.Networking.HowdooService;
import chat.hola.com.app.Utilities.Constants;
import chat.hola.com.app.Utilities.SessionApiCall;
import chat.hola.com.app.home.activity.followingTab.model.ClickListner;
import chat.hola.com.app.home.activity.followingTab.model.FollowingModel;
import chat.hola.com.app.home.activity.followingTab.model.FollowingResponse;
import chat.hola.com.app.models.NetworkConnector;
import chat.hola.com.app.models.SessionObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

/**
 * @author 3Embed.
 * @since 22/2/18.
 */

public class FollowingPresenter implements FollowingContract.Presenter, ClickListner {

    @Nullable
    FollowingContract.View view;
    @Inject
    HowdooService service;
    @Inject
    FollowingModel model;
    @Inject
    NetworkConnector networkConnector;
    private SessionApiCall sessionApiCall = new SessionApiCall();

    @Inject
    public FollowingPresenter() {

    }

    @Override
    public void attachView(FollowingContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }

    @Override
    public void init() {
        if (view != null)
            view.initPostRecycler();
    }

    @Override
    public void loadFollowing() {
        if (view != null)
            view.loading(true);
        service.followingActivities(AppController.getInstance().getApiToken(), Constants.LANGUAGE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<FollowingResponse>>() {
                    @Override
                    public void onNext(Response<FollowingResponse> response) {
                        if (view != null) {
                            switch (response.code()) {
                                case 200:
                                    view.showFollowings(response.body().getData());
                                    break;
                                case 401:
                                    view.sessionExpired();
                                    break;
                                case 406:
                                    SessionObserver sessionObserver = new SessionObserver();
                                    sessionObserver.getObservable().subscribeOn(Schedulers.io())
                                            .observeOn(AndroidSchedulers.mainThread())
                                            .subscribe(new DisposableObserver<Boolean>() {
                                                @Override
                                                public void onNext(Boolean flag) {
                                                    Handler handler = new Handler();
                                                    handler.postDelayed(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            loadFollowing();
                                                        }
                                                    }, 1000);
                                                }

                                                @Override
                                                public void onError(Throwable e) {

                                                }

                                                @Override
                                                public void onComplete() {
                                                }
                                            });
                                    sessionApiCall.getNewSession(service, sessionObserver);
                                    break;

                            }
                            view.isInternetAvailable(true);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.isInternetAvailable(networkConnector.isConnected());
                            view.loading(false);
                        }
                    }

                    @Override
                    public void onComplete() {
                        if (view != null)
                            view.loading(false);
                    }
                });
    }

    @Override
    public void onUserClicked(String userId) {
        if (view != null) {
            view.onUserClicked(userId);
        }
    }

    @Override
    public void onMediaClick(int position, android.view.View v) {
        if (view != null) {
            view.onMediaClick(position, v);
        }
    }
}
