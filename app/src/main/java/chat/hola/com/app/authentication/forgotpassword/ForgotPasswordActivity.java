package chat.hola.com.app.authentication.forgotpassword;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.lighthusky.dingdong.R;

import javax.inject.Inject;

import androidx.annotation.Nullable;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import chat.hola.com.app.Utilities.TypefaceManager;
import chat.hola.com.app.Utilities.Utilities;
import chat.hola.com.app.user_verification.VerifyEmailOTPActivity;
import dagger.android.support.DaggerAppCompatActivity;

public class ForgotPasswordActivity extends DaggerAppCompatActivity implements ForgotPasswordContract.View {

    @Inject
    TypefaceManager typefaceManager;
    @Inject
    ForgotPasswordContract.Presenter presenter;

    @BindView(R.id.eT_email)
    EditText etEmail;
    @BindView(R.id.tV_title)
    TextView tV_title;

    private InputMethodManager imm;
    private String phone, email, userName;
    private Dialog dialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(this);
        imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);

        phone = getIntent().getStringExtra("phone");
        userName = getIntent().getStringExtra("userName");
        etEmail.setTypeface(typefaceManager.getRegularFont());
        showKeyboard();

//        tV_title.setTypeface(se);
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
    }


    @OnClick(R.id.iV_back)
    public void back() {
        hideKeyBoard();
        super.onBackPressed();
    }


    @OnClick(R.id.rL_next)
    public void send() {
        hideKeyBoard();
        email = etEmail.getText().toString().trim();
        if (Utilities.isValidEmail(email)) {
            presenter.requestOtp(email);
//            presenter.forgotPassword(userName, phone, email);
        } else {
            message("Please enter valid email address");
        }
    }

    @Override
    public void message(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void success() {
        startActivity(new Intent(this, VerifyEmailOTPActivity.class).putExtra("isLogin", true).putExtra("email", email).putExtra("userName", userName));
    }


    @Override
    public void progress(boolean b) {
        if (b)
            dialog.show();
        else
            dialog.dismiss();
    }

    private void showKeyboard() {
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                etEmail.requestFocus();
                if (imm != null) {
                    imm.showSoftInput(etEmail, InputMethodManager.SHOW_FORCED);
                }
            }
        }, 200);
    }

    private void hideKeyBoard() {
        if (imm != null)
            imm.hideSoftInputFromWindow(etEmail.getWindowToken(), 0);
    }

}
