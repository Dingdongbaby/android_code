package chat.hola.com.app.user_verification.phone;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AlertDialog;
import com.lighthusky.dingdong.BuildConfig;
import com.lighthusky.dingdong.R;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import javax.inject.Inject;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import chat.hola.com.app.AppController;
import chat.hola.com.app.Utilities.ApiOnServer;
import chat.hola.com.app.Utilities.Constants;
import chat.hola.com.app.Utilities.Loader;
import chat.hola.com.app.Utilities.UploadFileAmazonS3;
import chat.hola.com.app.Utilities.Utilities;
import chat.hola.com.app.authentication.newpassword.NewPasswordActivity;
import chat.hola.com.app.models.Login;
import chat.hola.com.app.models.Register;
import chat.hola.com.app.profileScreen.discover.DiscoverActivity;
import chat.hola.com.app.webScreen.WebActivity;
import dagger.android.support.DaggerAppCompatActivity;

public class VerifyNumberOTPActivity extends DaggerAppCompatActivity
        implements VerifyNumberContract.View {

    @BindView(R.id.tV_msg)
    TextView tV_msg;
    @BindView(R.id.tV_timer)
    TextView tV_timer;
    @BindView(R.id.et_otp)
    EditText et_otp;
    @BindView(R.id.root)
    RelativeLayout root;
    @BindView(R.id.tV_resend)
    TextView tV_resend;
    @BindView(R.id.rL_next)
    RelativeLayout rL_next;

    Unbinder unbinder;
    @Inject
    VerifyNumberContract.Presenter presenter;

    String phoneNumber, email, countryCode, country;
    private CountDownTimer cTimer;
    private boolean isNumberVisible = false;
    private boolean isEmailVisible = false;
    private boolean isLogin = false;
    private boolean isFromSignUp = false;
    private boolean isForgotPassword = false;

    private Register register;
    private UploadFileAmazonS3 uploadFileAmazonS3;
    private String profilePic;
    private Loader dialog;
    private String call;
    private static Bus bus = AppController.getBus();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);
        Utilities.initSmsRetriever(this);
        bus.register(this);
        unbinder = ButterKnife.bind(this);
        uploadFileAmazonS3 = new UploadFileAmazonS3(this);
        dialog = new Loader(this);

        tV_resend.setVisibility(View.GONE);
        if (getIntent() != null) {
            call = getIntent().getStringExtra("call");
            phoneNumber = getIntent().getStringExtra("phoneNumber");
            countryCode = getIntent().getStringExtra("countryCode");
            country = getIntent().getStringExtra("country");
            isNumberVisible = getIntent().getBooleanExtra("isNumberVisible", false);
            String msg = getString(R.string.verification_code_sent_phone)
                    + " \""
                    + countryCode
                    + ""
                    + phoneNumber
                    + "\" "
                    + getString(R.string.please_enter_code_below);
            isFromSignUp = getIntent().getBooleanExtra("isFromSignUp", false);
            tV_msg.setText(msg);
            isLogin = getIntent().getBooleanExtra("isLogin", false);
            isForgotPassword = getIntent().getBooleanExtra("isForgotPassword", false);

            profilePic = getIntent().getStringExtra("profilePic");
            register = (Register) getIntent().getSerializableExtra("register");
            uploadFileAmazonS3 = new UploadFileAmazonS3(this);
        }

        if (BuildConfig.DEBUG || ApiOnServer.ALLOW_DEFAULT_OTP) {
        et_otp.setText(getString(R.string.text_1111));
                }

        if (isFromSignUp) {
            Map<String, Object> params = new HashMap<>();
            params.put("deviceId", AppController.getInstance().getDeviceId());
            params.put("phoneNumber", phoneNumber);
            params.put("countryCode", countryCode);
            params.put("type", 1);
            params.put("development", BuildConfig.DEBUG);
            params.put("hashKey", Utilities.getHashCode(this));
            presenter.makeReqToNumber(params);
        }

        cTimer = new CountDownTimer(60000, 1000) {

            public void onTick(long millisUntilFinished) {
                tV_resend.setVisibility(View.GONE);
                long sec = millisUntilFinished / 1000;
                if (sec > 9) {
                    tV_timer.setText(getString(R.string.string_296) + getString(R.string.double_inverted_comma) + sec);
                } else {
                    tV_timer.setText(getString(R.string.string_297) + getString(R.string.double_inverted_comma) + sec);
                }
            }

            public void onFinish() {
                tV_msg.setText(getString(R.string.resend_otp_msg));
                tV_resend.setVisibility(View.VISIBLE);
                tV_timer.setVisibility(View.GONE);
            }
        };

        startTimer();
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgress(boolean show) {
        if(!isFinishing()) {
            if (show) {
                dialog.show();
            } else {
                try {
                    if (dialog != null && dialog.isShowing()) dialog.dismiss();
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void numberNotRegistered(String message) {
        androidx.appcompat.app.AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setMessage(message + "\n\nDo you want to SignUp with this number ?");
        builder.setPositiveButton("SignUp", (dialog, which) -> {
            Intent intent = new Intent(VerifyNumberOTPActivity.this, WebActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("url", getResources().getString(R.string.privacyPolicyUrl));
            bundle.putString("title", getResources().getString(R.string.privacyPolicy));
            bundle.putString("action", "accept");
            startActivity(intent.putExtra("url_data", bundle));
            finish();
        });
        builder.setNegativeButton("Change", (dialog, which) -> {
            super.onBackPressed();
            finish();
        });
        builder.show();
    }

    @Override
    public void openSuccessDialog() {
        Dialog dialog = new Dialog(this);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.verified_star_email_phone_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow()
                .setLayout(RelativeLayout.LayoutParams.MATCH_PARENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false);

        ImageView imageView = dialog.findViewById(R.id.imageView);
        TextView textView = dialog.findViewById(R.id.tV_msg);
        RelativeLayout rL_ok = dialog.findViewById(R.id.rL_ok);

        imageView.setImageDrawable(getDrawable(R.drawable.ic_phone_number_verified));
        textView.setText(getString(R.string.verified_phone_num_msg));

        rL_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent data = new Intent();
                data.putExtra("numberVerified", true);
                setResult(Constants.Verification.NUMBER_VERIFICATION_REQ, data);
                onBackPressed();
            }
        });

        dialog.show();
    }

    @Override
    public void newPassword() {
        startActivity(new Intent(this, NewPasswordActivity.class).putExtra("countryCode", countryCode)
                .putExtra("phoneNumber", phoneNumber));
        finish();
    }

    @Override
    public void registered(Login.LoginResponse response) {
        Intent i = new Intent(this, DiscoverActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.putExtra("caller", "SaveProfile");
        startActivity(i);
        finish();
    }

    @Override
    public void startTimer() {
        cTimer.start();
    }

    @Override
    public void businessPhoneVerified() {
        setResult(RESULT_OK, new Intent());
        finish();
    }

    @Override
    public void showNext() {
        if(rL_next!=null) rL_next.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.iV_back)
    public void back() {
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        unbinder.unbind();
        cTimer.cancel();
        if(bus!=null)
            bus.unregister(this);
        super.onDestroy();
    }

    @OnClick(R.id.rL_next)
    public void next() {

        if (!et_otp.getText().toString().isEmpty() || et_otp.getText().toString().length() < 4) {
            if (call != null && call.equals(Constants.BUSINESS)) {
                presenter.verifyBusinessPhoneNumber(et_otp.getText().toString(), countryCode, phoneNumber);
            } else {
                //                if (!BuildConfig.DEBUG)
                //                    cTimer.start();

                Login login = new Login();
                login.setOtp(et_otp.getText().toString());
                login.setDeviceId(AppController.getInstance().getDeviceId());
                login.setPhoneNumber(phoneNumber);
                login.setCountryCode(countryCode);
                login.setCountry(country);
                login.setDeviceName(Build.DEVICE);
                login.setDeviceOs(Build.VERSION.RELEASE);
                login.setModelNumber(Build.MODEL);
                login.setDeviceType("2"); // here 2= android
                try {
                    PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
                    String version = pInfo.versionName;
                    login.setAppVersion(version);
                } catch (PackageManager.NameNotFoundException e) {
                    login.setAppVersion("123");
                }
                login.setType(
                        isLogin || isFromSignUp ? 1 : 2); // here type 1 for verify user, 2 for star user verify
                int move = 0;
                if (isFromSignUp) {
                    login.setIgnUp(true);
                    move = 1;
                } else if (isLogin) {
                    move = 2;
                } else if (isForgotPassword) {
                    move = 3;
                } else {
                    openSuccessDialog();
                }

                String appVersion;
                try {
                    PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                    appVersion = pInfo.versionName;
                } catch (PackageManager.NameNotFoundException e) {
                    appVersion = "123";
                }

                if (register != null) {
                    register.setDeviceName(Build.DEVICE);
                    register.setDeviceOs(Build.VERSION.RELEASE);
                    register.setModelNumber(Build.MODEL);
                    register.setDeviceType("2");
                    register.setDeviceId(AppController.getInstance().getDeviceId());
                    register.setAppVersion(appVersion);
                    register.setCountry(country);
                }
                presenter.verifyPhoneNumber(login, move, register, uploadFileAmazonS3,
                        profilePic != null ? new File(profilePic) : null);
            }
        } else {
            showMessage("Enter verification code that sent to your register phone number");
        }
    }

    @OnClick(R.id.tV_resend)
    public void resend() {
        rL_next.setVisibility(View.GONE);
        tV_timer.setVisibility(View.VISIBLE);
        if (call != null && call.equals(Constants.BUSINESS)) {
            //TODO verify business mobile number
        } else {
            Map<String, Object> params = new HashMap<>();
            params.put("deviceId", AppController.getInstance().getDeviceId());
            params.put("phoneNumber", phoneNumber);
            params.put("countryCode", countryCode);
            params.put("type", 1);
            params.put("development", BuildConfig.DEBUG);
            params.put("hashKey", Utilities.getHashCode(this));
            presenter.makeReqToNumber(params);
        }
    }

    @Subscribe
    public void getMessage(JSONObject obj){
        try {
            if(obj.getString("eventName").equals("OTP_RECEIVED")){
                et_otp.setText(obj.getString("otp"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}