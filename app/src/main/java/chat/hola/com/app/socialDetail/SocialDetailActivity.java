package chat.hola.com.app.socialDetail;

import android.app.Activity;
import android.app.KeyguardManager;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import com.bumptech.glide.DrawableRequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.signature.StringSignature;
import com.lighthusky.dingdong.R;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.ShortDynamicLink;
import com.volokh.danylo.video_player_manager.manager.PlayerItemChangeListener;
import com.volokh.danylo.video_player_manager.manager.SingleVideoPlayerManager;
import com.volokh.danylo.video_player_manager.manager.VideoPlayerManager;
import com.volokh.danylo.video_player_manager.meta.MetaData;
import com.volokh.danylo.visibility_utils.calculator.DefaultSingleItemCalculatorCallback;
import com.volokh.danylo.visibility_utils.calculator.ListItemsVisibilityCalculator;
import com.volokh.danylo.visibility_utils.calculator.SingleListViewItemActiveCalculator;
import com.volokh.danylo.visibility_utils.scroll_utils.ItemsPositionGetter;
import com.volokh.danylo.visibility_utils.scroll_utils.RecyclerViewItemPositionGetter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import chat.hola.com.app.AppController;
import chat.hola.com.app.Dialog.BlockDialog;
import chat.hola.com.app.Utilities.Constants;
import chat.hola.com.app.Utilities.RoundedImageView;
import chat.hola.com.app.Utilities.TypefaceManager;
import chat.hola.com.app.comment.CommentActivity;
import chat.hola.com.app.home.contact.Friend;
import chat.hola.com.app.home.model.Data;
import chat.hola.com.app.manager.session.SessionManager;
import chat.hola.com.app.music.MusicActivity;
import chat.hola.com.app.post.PostActivity;
import chat.hola.com.app.profileScreen.ProfileActivity;
import chat.hola.com.app.profileScreen.followers.FollowersActivity;
import chat.hola.com.app.socialDetail.video_manager.BaseVideoItem;
import chat.hola.com.app.socialDetail.video_manager.ClickListner;
import chat.hola.com.app.socialDetail.video_manager.ItemFactory;
import chat.hola.com.app.trendingDetail.TrendingDetail;
import chat.hola.com.app.webScreen.WebActivity;
import dagger.android.support.DaggerAppCompatActivity;

/**
 * <h1>SocialDetailActivity</h1>
 * <p>It shows details of post. user can like, share, report, edit and delete post</p>
 *
 * @author 3Embed
 * @version 1.0
 * @since 5/3/2018
 */

public class SocialDetailActivity extends DaggerAppCompatActivity
        implements ClickListner, ActionListner,
        ItemAdapter.ItemListener, SocialDetailContract.View, DialogInterface.OnClickListener,
        PopupMenu.OnMenuItemClickListener {
    private static final int COMMENT_COUNT = 1010;
    static final int PAGE_SIZE = Constants.PAGE_SIZE;

    @Inject
    BlockDialog dialog;
    @Inject
    SocialDetailPresenter mPresenter;
    @Inject
    SessionManager sessionManager;
    @Inject
    TypefaceManager typefaceManager;
    @Inject
    AlertDialog.Builder reportDialog;
    @Inject
    ArrayAdapter<String> arrayAdapter;

    @BindView(R.id.rvList)
    RecyclerView rvList;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.bottom_sheet)
    View bottomSheet;
    @BindView(R.id.ivProfilePic)
    RoundedImageView ivProfilePic;
    @BindView(R.id.etMessage)
    EditText etMessage;
    @BindView(R.id.searchView)
    androidx.appcompat.widget.SearchView searchView;
    @BindView(R.id.shareList)
    RecyclerView shareList;
    @BindView(R.id.overlay)
    View overlay;
    @BindView(R.id.cover)
    AppCompatImageView cover;

    // Collection bottom sheet data.
    @BindView((R.id.sheetCollection))
    View sheetCollection;
    @BindView(R.id.cTitle)
    TextView cTitle;
    @BindView(R.id.iV_newCollection)
    ImageView iV_newCollection;
    @BindView(R.id.cBack)
    ImageView cBack;
    @BindView(R.id.rV_collections)
    RecyclerView rV_collections;
    @BindView(R.id.ll_newCollection)
    LinearLayout ll_newCollection;
    @BindView(R.id.iV_cImage)
    ImageView iV_cImage;
    @BindView(R.id.et_cName)
    EditText et_cName;
    @BindView(R.id.tV_cAction)
    TextView tV_cAction;
    @BindView(R.id.blurView)
    View blurView;
    private BottomSheetBehavior collectionBehavior;
    // used while new collection creation.
    private String collectionImage = "", addToCollectionPostId = "";
    ///////////////////////////////

    private String postId;
    private Data data = null;
    private String userId;
    private String categoryId;
    private Menu menu;

    private String call;
    private ItemAdapter itemAdapter;
    @Inject
    List<Data> dataList;
    private BottomSheetBehavior behavior;

    private PopupMenu popupMenu;

    private final ArrayList<BaseVideoItem> mList = new ArrayList<>();
    private final ListItemsVisibilityCalculator mVideoVisibilityCalculator =
            new SingleListViewItemActiveCalculator(new DefaultSingleItemCalculatorCallback(), mList);
    private int mScrollState = AbsListView.OnScrollListener.SCROLL_STATE_IDLE;
    private ItemsPositionGetter mItemsPositionGetter;
    private SocialDetailAdapter socialDetailAdapter;
    private LinearLayoutManager mLayoutManager;
    private static int currentPosition = 0;
    private boolean notFirstResume = false;
    private ProgressDialog pDialog;
    private int lastVisibleItemPositionOnActivityStop;
    private DisplayMetrics displayMetrics = Resources.getSystem().getDisplayMetrics();
    private double screenRatio = ((double) displayMetrics.heightPixels / displayMetrics.widthPixels);

    //    private boolean needToScrollVideo = false;
    private KeyguardManager keyboardManager;

    // private boolean notFirstResume = true;
    private final VideoPlayerManager<MetaData> mVideoPlayerManager =
            new SingleVideoPlayerManager(new PlayerItemChangeListener() {
                @Override
                public void onPlayerItemChanged(MetaData metaData) {

                }
            });

    private List<Friend> friends = new ArrayList<>();
    //private int position;
    private int pos = 0;

    @SuppressWarnings("unchecked")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_social_detail);
        ButterKnife.bind(this);
        toolbarSetup();

        postId = getIntent().getStringExtra("postId");
        data = (Data) getIntent().getSerializableExtra(Constants.SocialFragment.DATA);
        call = getIntent().getStringExtra("call");
        dataList = (List<Data>) getIntent().getSerializableExtra("dataList");
        if (dataList != null && !dataList.isEmpty()) setDataList(dataList, true, false);

        pDialog = new ProgressDialog(this, 0);
        pDialog.setCancelable(false);

        if (dataList == null) dataList = new ArrayList<>();
        pos = getIntent().getIntExtra("position", 0);
        if (!dataList.isEmpty()) data = dataList.get(pos);

        //SocialDetailPresenter.page = getIntent().getIntExtra("page", 0);//+ 1;

        mPresenter.setPageNumber(dataList.size() / PAGE_SIZE);
        mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setInitialPrefetchItemCount(10);
        mLayoutManager.setItemPrefetchEnabled(true);

        rvList.setLayoutManager(mLayoutManager);
        socialDetailAdapter =
                new SocialDetailAdapter(mVideoPlayerManager, SocialDetailActivity.this, mList, dataList,
                        getScreenWidth(), getScreenHeight());
        socialDetailAdapter.setClickListner(this);
        rvList.setAdapter(socialDetailAdapter);

        SnapHelper snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(rvList);
        behavior = BottomSheetBehavior.from(bottomSheet);

        rvList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int scrollState) {
                mScrollState = scrollState;
                if (scrollState == RecyclerView.SCROLL_STATE_IDLE && !mList.isEmpty()) {
                    mVideoVisibilityCalculator.onScrollStateIdle(mItemsPositionGetter,
                            mLayoutManager.findFirstVisibleItemPosition(),
                            mLayoutManager.findLastVisibleItemPosition());
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                try {
                    if (!mList.isEmpty()) {
                        try {
                            mVideoVisibilityCalculator.onScroll(mItemsPositionGetter,
                                    mLayoutManager.findFirstVisibleItemPosition(),
                                    mLayoutManager.findLastVisibleItemPosition()
                                            - mLayoutManager.findFirstVisibleItemPosition() + 1, mScrollState);

                            behavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    int visibleItemCount = mLayoutManager.getChildCount();

                    mPresenter.callApiOnScroll(mLayoutManager.findFirstVisibleItemPosition(),
                            visibleItemCount, mLayoutManager.getItemCount());
                    data = dataList.get(visibleItemCount);
                } catch (Exception ignored) {
                }
            }
        });

        mItemsPositionGetter = new RecyclerViewItemPositionGetter(mLayoutManager, rvList);
        rvList.scrollToPosition(pos);
        //   rvList.smoothScrollToPosition(pos);

        mPresenter.getFollowUsers();
        Glide.with(this)
                .load(sessionManager.getUserProfilePic().replace("upload/", Constants.PROFILE_PIC_SHAPE))

                .asBitmap()
                .signature(new StringSignature(
                        AppController.getInstance().getSessionManager().getUserProfilePicUpdateTime()))
                .centerCrop()
                .into(ivProfilePic);

        if (data != null && dataList == null) {
            userId = data.getUserId();
            postId = data.getPostId();
            getData(postId);
        } else if (postId != null) {
            getData(postId);
        }
        if (dataList != null && !dataList.isEmpty()) postId = dataList.get(pos).getPostId();
        reportDialog.setTitle(R.string.report);
        mPresenter.getReportReasons();
        reportDialog.setAdapter(arrayAdapter, this);
        invalidateOptionsMenu();

        behavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {

                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        refreshFriendList();
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                        break;
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                // React to dragging events
            }
        });

        rV_collections.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        collectionBehavior = BottomSheetBehavior.from(sheetCollection);
        collectionBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        collectionBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {

                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        blurView.setVisibility(View.GONE);
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                        blurView.setVisibility(View.VISIBLE);
                        break;
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        blurView.setVisibility(View.VISIBLE);
                        break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        blurView.setVisibility(View.VISIBLE);
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                // React to dragging events
            }
        });

        shareList.setHasFixedSize(true);
        shareList.setLayoutManager(new LinearLayoutManager(this));

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);

        // listening to search query text change
        searchView.setOnQueryTextListener(
                new androidx.appcompat.widget.SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        // itemAdapter.getFilter().filter(query);
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String newText) {
                        //    itemAdapter.getFilter().filter(newText);
                        return false;
                    }
                });

        rvList.post(new Runnable() {
            @Override
            public void run() {
                try {
                    BaseVideoItem videoItem = mList.get(pos);

                    ViewHolder viewHolder = (ViewHolder) rvList.findViewHolderForAdapterPosition(pos);
                    if (viewHolder != null) {

                        videoItem.setActive(viewHolder.itemView, pos);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        keyboardManager = (KeyguardManager) getSystemService(Context.KEYGUARD_SERVICE);
    }

    @OnClick(R.id.blurView)
    public void blurViewClick() {
        collectionBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        sheetCollection.setVisibility(View.GONE);
    }

    @OnClick({R.id.iV_newCollection})
    public void createNewCollection() {

        iV_newCollection.setVisibility(View.GONE);
        rV_collections.setVisibility(View.GONE);
        ll_newCollection.setVisibility(View.VISIBLE);
        cBack.setVisibility(View.VISIBLE);
        cTitle.setText(getString(R.string.new_collection));
        tV_cAction.setText(getString(R.string.done));

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                et_cName.requestFocus();
                openKeyboard(getApplicationContext());
            }
        }, 500);
    }

    @OnClick({R.id.cBack})
    public void backFromCreateCollection() {

        hideKeyboard(getApplicationContext());


        iV_newCollection.setVisibility(View.VISIBLE);
        rV_collections.setVisibility(View.VISIBLE);
        ll_newCollection.setVisibility(View.GONE);
        cBack.setVisibility(View.GONE);
        cTitle.setText(getString(R.string.save_to));
        tV_cAction.setText(getString(R.string.cancel));
    }

    @OnClick(R.id.tV_cAction)
    public void collectionActionClick() {
        if (iV_newCollection.getVisibility() == View.VISIBLE) {
            // visible then cancel show as an action.
            sheetCollection.setVisibility(View.GONE);
            collectionBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        } else {
            // Gone then done show as an action.
            if (!et_cName.getText().toString().trim().isEmpty()) {
                mPresenter.createCollection(et_cName.getText().toString().trim(), collectionImage,
                        addToCollectionPostId);
            }
        }
    }

    private int calculateFinalPosition(int currentPosition) {

        int size = mList.size();
        if (size == 1) {
            return currentPosition;
        } else if (currentPosition == size - 1) {
            return currentPosition - 1;
        } else {
            return currentPosition + 1;
        }
    }

    private void getData(String postId) {
        mPresenter.getPostById(postId, false);
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int which) {
        AlertDialog.Builder confirm = new AlertDialog.Builder(this);
        confirm.setMessage(R.string.report_message);
        confirm.setPositiveButton(R.string.confirm,
                (dialog, w) -> mPresenter.reportPost(postId, arrayAdapter.getItem(which),
                        arrayAdapter.getItem(which)));
        confirm.setNegativeButton(R.string.cancel, (dialog, w) -> dialog.dismiss());
        confirm.create().show();
    }

    private void invalidateMenu(Data data) {
        try {
            MenuItem edit = menu.findItem(R.id.action_edit);
            MenuItem report = menu.findItem(R.id.action_report);
            MenuItem delete = menu.findItem(R.id.action_delete);

            boolean flag = data.getUserId() != null && data.getUserId()
                    .equals(AppController.getInstance().getUserId());
            edit.setVisible(flag);
            report.setVisible(!flag);
            delete.setVisible(flag);
            popupMenu.show();
        } catch (Exception ignored) {
        }
    }

    private void toolbarSetup() {
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mVideoPlayerManager.stopAnyPlayback(true);
        lastVisibleItemPositionOnActivityStop = mLayoutManager.findLastVisibleItemPosition();
        showOverlay(lastVisibleItemPositionOnActivityStop);
    }

    @Override
    protected void onDestroy() {
        try {
            mVideoPlayerManager.resetMediaPlayer();
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void userBlocked() {
        dialog.show();
    }

    @Override
    public void liked(boolean likeRequest, boolean hasError, String postId) {
        if (hasError) {

            for (int i = 0; i < dataList.size(); i++) {

                if (dataList.get(i).getPostId().equals(postId)) {
                    if (likeRequest) {

                        Data data = dataList.get(i);
                        data.setLiked(false);
                        int count = Integer.parseInt(data.getLikesCount());
                        if (count > 0) {
                            count--;
                        } else {
                            count = 0;
                        }
                        data.setLikesCount(String.valueOf(count));

                        dataList.set(i, data);
                    } else {

                        Data data = dataList.get(i);
                        data.setLiked(true);
                        int count = Integer.parseInt(data.getLikesCount());

                        count++;

                        data.setLikesCount(String.valueOf(count));

                        dataList.set(i, data);
                    }
                    socialDetailAdapter.notifyItemChanged(i);
                    break;
                }
            }
        }
    }

    @Override
    public void dismissDialog() {
    }

    @Override
    public void addToReportList(ArrayList<String> data) {
        arrayAdapter.clear();
        arrayAdapter.addAll(data);
    }

    @Override
    public void setData(Data data) {
        if (data != null) {
            this.data = data;
            userId = data.getUserId();
            postId = data.getPostId();

            int postion = -1;
            for (int i = 0; i < dataList.size(); i++) {

                if (dataList.get(i).getPostId().equals(postId)) {
                    postion = i;
                    break;
                }
            }

            if (postion != -1) {
                dataList.set(postion, data);
                try {
                    mList.set(postion, ItemFactory.createItem(this, mVideoPlayerManager, data));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                dataList.add(data);
                List<Data> list = new ArrayList<>();
                list.add(data);
                setData(list, true);
            }

            socialDetailAdapter.setDataList(mList, dataList);
        } else {
            showMessage("", R.string.no_post_available);
            finish();
        }
    }

    @Override
    public void like(boolean like, String postId) {
        try {
            if (like) {
                mPresenter.like(postId);
            } else {
                mPresenter.unlike(postId);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleted() {
        onBackPressed();
    }

    @Override
    public void follow(boolean isChecked, String userId) {

        if (isChecked) {
            mPresenter.follow(userId);
        } else {
            mPresenter.unfollow(userId);
        }
    }

    @Override
    public void showMessage(String msg, int msgId) {
        Toast.makeText(this, msgId != 0 ? getString(msgId) : msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void sessionExpired() {
        sessionManager.sessionExpired(getApplicationContext());
    }

    @Override
    public void isInternetAvailable(boolean flag) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == COMMENT_COUNT && resultCode == RESULT_OK) {
            currentPosition = data.getIntExtra("position", 0);
            try {

                socialDetailAdapter.updateCommentCount(data.getStringExtra("commentCount"),
                        data.getStringExtra("postId"), data.getIntExtra("position", 0), rvList);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.getCollections();
        if (notFirstResume) {
            if (keyboardManager != null && !keyboardManager.isKeyguardLocked()) {

                //it is not locked
                if (dataList != null && !dataList.isEmpty()) {
                    try {

                        if (dataList.get(lastVisibleItemPositionOnActivityStop).getMediaType1() == 1) {

                            playVideoOnResume(lastVisibleItemPositionOnActivityStop,
                                    calculateFinalPosition(lastVisibleItemPositionOnActivityStop));
                        } else {

                            hideOverlay();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else {
                //For some cases when screen is showing as locked although it is not(happens rarely),in which case video playback doesnt resume until scrolled
                if (overlay.getVisibility() == View.VISIBLE) {
                    hideOverlay();
                }
            }
        } else {
            notFirstResume = true;
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void reload() {
    }

    @Override
    public void likePost(Data data) {
    }

    @Override
    public void send(int position) {
        //this.position = position;
        if (itemAdapter != null) itemAdapter.setPostItemClickPostion(position);
        if (friends != null && !friends.isEmpty()) {
            bottomSheet.setVisibility(View.VISIBLE);
            bottomSheet.post(() -> {
                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            });
        } else {
            Toast.makeText(this, getResources().getString(R.string.please_add_friends),
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void comment(String postId, int position, String commentsCount) {
        currentPosition = position;
        startActivityForResult(new Intent(this, CommentActivity.class).putExtra("position", position)
                .putExtra("postId", postId)
                .putExtra("commentsCount", commentsCount), COMMENT_COUNT);
    }

    @Override
    public void profile(String id, boolean isChannel, boolean isBusiness) {

        if (isBusiness) {
            Intent intent = new Intent(this, ProfileActivity.class);
            intent.putExtra("isBusiness", true);
            intent.putExtra(Constants.SocialFragment.USERID, id);
            startActivity(intent);
        } else if (isChannel) {
            Intent intent = new Intent(this, TrendingDetail.class);
            intent.putExtra("channelId", id);
            intent.putExtra("call", "channel");
            startActivity(intent);
        } else {
            Intent intent = new Intent(this, ProfileActivity.class);
            intent.putExtra(Constants.SocialFragment.USERID, id);
            startActivity(intent);
        }
    }

    @Override
    public void category(String categoryId, String categoryName) {
        startActivity(new Intent(this, TrendingDetail.class).putExtra("categoryId", categoryId)
                .putExtra("call", "category")
                .putExtra("category", categoryName));
    }

    @Override
    public void channel(String channelId, String channelName) {
        startActivity(new Intent(this, TrendingDetail.class).putExtra("call", "channel")
                .putExtra("channelId", channelId));
    }

    @Override
    public void music(String id, String name, String path) {
        startActivity(new Intent(this, MusicActivity.class).putExtra("musicPath", path)
                .putExtra("call", "music")
                .putExtra("musicId", id)
                .putExtra("name", name));
    }

    @Override
    public void followers(List<Friend> data) {
        friends = data;
        itemAdapter = new ItemAdapter(this, friends, this, typefaceManager);
        shareList.setAdapter(itemAdapter);
    }

    private void refreshFriendList() {
        for (Friend f : friends) {
            if (f.isSent()) {
                f.setSent(false);
            }
        }
        itemAdapter.notifyDataSetChanged();
    }

    @Override
    public void setDataList(List<Data> data, boolean entirelyNewList, boolean refreshRequest) {

        List<Data> nonDuplicateData = new ArrayList<>();

        if (entirelyNewList) {
            nonDuplicateData = data;
        } else {

            Map<String, Integer> postIds = new HashMap<>();

            for (int i = 0; i < dataList.size(); i++) {

                postIds.put(dataList.get(i).getPostId(), i);
            }

            for (int i = 0; i < data.size(); i++) {

                if (postIds.containsKey(data.get(i).getPostId())) {

                    try {
                        dataList.set(postIds.get(data.get(i).getPostId()), data.get(i));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {

                    nonDuplicateData.add(data.get(i));
                }
            }
            //HashSet<String> postIds = new HashSet<>();
            //
            //for (int i = 0; i < dataList.size(); i++) {
            //  postIds.add(dataList.get(i).getPostId());
            //}
            //
            //for (int i = 0; i < data.size(); i++) {
            //
            //  if (!postIds.contains(data.get(i).getPostId())) {
            //
            //    nonDuplicateData.add(data.get(i));
            //  }
            //}
        }
        if (refreshRequest) {
            dataList.addAll(0, nonDuplicateData);
        } else {
            if (!entirelyNewList) {
                dataList.addAll(nonDuplicateData);
            }
        }
        setData(nonDuplicateData, refreshRequest);
        if (socialDetailAdapter != null) {
            socialDetailAdapter.setDataList(mList, dataList);

            if ((entirelyNewList || refreshRequest) && mList.size() > 0) {
                rvList.post(() -> {
                    try {
                        BaseVideoItem videoItem = mList.get(0);

                        ViewHolder viewHolder = (ViewHolder) rvList.findViewHolderForAdapterPosition(0);
                        if (viewHolder != null) {

                            videoItem.setActive(viewHolder.itemView, 0);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
            }
        }
    }

    @Override
    public void showProgress(boolean b) {
        //TODO
    }

    @Override
    public void postAddedToCollection() {
        sheetCollection.setVisibility(View.GONE);
        collectionBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        backFromCreateCollection();

        Toast.makeText(this, R.string.added_to_collection, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void collectionCreated() {
        hideKeyboard(this);
        sheetCollection.setVisibility(View.GONE);
        collectionBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        backFromCreateCollection();
        et_cName.setText("");
        mPresenter.getCollections();

        Toast.makeText(this, R.string.collection_created, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onItemClick(Friend userdata, int position) {

    }

    public void setData(List<Data> dataList, boolean refreshRequest) {
        try {

            for (Data data : dataList) {
                if (refreshRequest) {

                    mList.add(0, ItemFactory.createItem(this, mVideoPlayerManager, data));
                } else {

                    mList.add(ItemFactory.createItem(this, mVideoPlayerManager, data));
                }
                try {
                    Glide.with(this)
                            .load(data.getThumbnailUrl1().replace("upload", "upload/t_media_lib_thumb"))
                            .downloadOnly(Integer.parseInt(data.getImageUrl1Width()),
                                    Integer.parseInt(data.getImageUrl1Height()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (IllegalArgumentException | NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void share(Data data) {
        showProgressDialog(getResources().getString(R.string.please_wait));
        new Thread(() -> {
            String url = "https://www.appscrip.com/post/" + data.getPostId();
            Task<ShortDynamicLink> shortLinkTask = FirebaseDynamicLinks.getInstance()
                    .createDynamicLink()
                    .setLongLink(Uri.parse("https://dublyapp.page.link?link=" + url + "&apn=com.lighthusky.dingdong"))
                    .buildShortDynamicLink()
                    .addOnCompleteListener(this, task -> {
                        if (task.isSuccessful()) {
                            Uri shortLink = task.getResult().getShortLink();
                            Uri flowchartLink = task.getResult().getPreviewLink();
                            Intent intent = new Intent(Intent.ACTION_SEND);
                            intent.putExtra(Intent.EXTRA_TEXT, shortLink.toString());
                            intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
                            intent.setType("text/plain");
                            Intent chooser = Intent.createChooser(intent, getString(R.string.selectApp));
                            startActivity(chooser);
                        }
                        hideProgressDialog();
                    })
                    .addOnFailureListener(Throwable::printStackTrace);
        }).start();
    }

    /**
     * To show progress dialog
     */
    private void showProgressDialog(String message) {
        pDialog.setMessage(message);
        if (pDialog != null && !pDialog.isShowing()) {
            pDialog.show();
            ProgressBar bar = (ProgressBar) pDialog.findViewById(android.R.id.progress);

            bar.getIndeterminateDrawable()
                    .setColorFilter(ContextCompat.getColor(this, R.color.color_black),
                            android.graphics.PorterDuff.Mode.SRC_IN);
        }
    }

    /**
     * To hide progress dialog
     */
    @SuppressWarnings("TryWithIdenticalCatches")
    private void hideProgressDialog() {
        if (pDialog.isShowing()) {
            Context context = ((ContextWrapper) (pDialog).getContext()).getBaseContext();
            if (context instanceof Activity) {
                if (!((Activity) context).isFinishing() && !((Activity) context).isDestroyed()) {
                    pDialog.dismiss();
                }
            } else {
                try {
                    pDialog.dismiss();
                } catch (final IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (final Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void openMenu(Data data, ImageButton ibMenu) {
        this.data = data;
        popupMenu = new PopupMenu(this, ibMenu);
        popupMenu.inflate(R.menu.social_detail_menu);
        popupMenu.setOnMenuItemClickListener(this);
        menu = popupMenu.getMenu();
        invalidateMenu(data);
    }

    @Override
    public void view(Data data) {
        mPresenter.getPostById(data.getPostId(), true);
    }

    @Override
    public void location(Data data) {
        startActivity(new Intent(this, TrendingDetail.class).putExtra("placeId", data.getPlaceId())
                .putExtra("call", "location")
                .putExtra("location", data.getPlace())
                .putExtra("latlong", data.getLocation()));
    }

    @Override
    public void openLikers(Data data) {
        Intent intent = new Intent(this, FollowersActivity.class);
        intent.putExtra("title", getResources().getString(R.string.likers));
        intent.putExtra("userId", data.getPostId());
        startActivity(intent);
    }

    @Override
    public void openViewers(Data data) {
        Intent intent = new Intent(this, FollowersActivity.class);
        intent.putExtra("title", "Viewers");
        intent.putExtra("userId", data.getPostId());
        startActivity(intent);
    }

    @Override
    public void onActionButtonClick(String title, String url) {
        if (!url.contains("http")) url = "http://" + url;
        Intent intent = new Intent(this, WebActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("url", url);
        bundle.putString("title", title);
        intent.putExtra("url_data", bundle);
        startActivity(intent);
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        if (item != null) {
            switch (item.getItemId()) {
                case R.id.action_report:
                    reportDialog.show();
                    return true;
                case R.id.action_edit:
                    Intent intentEdit = new Intent(this, PostActivity.class);
                    intentEdit.putExtra("data", data);
                    intentEdit.putExtra("call", "edit");
                    intentEdit.putExtra(Constants.Post.TYPE,
                            data.getMediaType1() == 0 ? Constants.Post.IMAGE : Constants.Post.VIDEO);
                    startActivity(intentEdit);
                    finish();
                    return true;
                case R.id.action_delete:
                    reportDialog.setMessage(R.string.postDeleteMsg);
                    reportDialog.setPositiveButton(R.string.yes,
                            (dialog, which) -> mPresenter.deletePost(postId));
                    reportDialog.setNegativeButton(R.string.no, (dialog, which) -> dialog.dismiss());
                    reportDialog.create().show();
                    return true;
                default:
                    return super.onOptionsItemSelected(item);
            }
        }
        return false;
    }

    public void playVideoOnResume(int initialPosition, int finalPosition) {

        rvList.smoothScrollToPosition(finalPosition);

        try {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    rvList.smoothScrollToPosition(initialPosition);
                    try {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {

                                hideOverlay();
                            }
                        }, 500);
                    } catch (Exception e) {

                        hideOverlay();
                    }
                }
            }, 500);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int getScreenWidth() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    public static int getScreenHeight() {
        return Resources.getSystem().getDisplayMetrics().heightPixels;
    }

    private void hideOverlay() {
        overlay.setVisibility(View.GONE);
    }

    private void showOverlay(int position) {
        if (dataList != null && !dataList.isEmpty()) {
            overlay.setVisibility(View.VISIBLE);


            try {

                double ratio = ((double) Integer.parseInt(dataList.get(position).getImageUrl1Height()))
                        / Integer.parseInt(dataList.get(position).getImageUrl1Width());

                if (ratio > screenRatio) {
                    try {
                        DrawableRequestBuilder<String> thumbnailRequest = Glide.with(this)
                                .load(dataList.get(position)
                                        .getThumbnailUrl1()
                                        .replace("upload", "upload/t_media_lib_thumb")
                                        .replace(".jpg", ".webp")
                                        .replace(".jpeg", ".webp")
                                        .replace(".png", ".webp"))
                                .centerCrop()
                                .diskCacheStrategy(DiskCacheStrategy.SOURCE);

                        Glide.with(this)
                                .load(dataList.get(position)
                                        .getThumbnailUrl1()
                                        .replace("upload", "upload/so_0.01")
                                        .replace(".jpg", ".webp"))
                                .thumbnail(thumbnailRequest)
                                .dontAnimate()
                                .centerCrop()
                                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                                .into(cover);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        DrawableRequestBuilder<String> thumbnailRequest = Glide.with(this)
                                .load(dataList.get(position)
                                        .getThumbnailUrl1()
                                        .replace("upload", "upload/t_media_lib_thumb")
                                        .replace(".jpg", ".webp")
                                        .replace(".jpeg", ".webp")
                                        .replace(".png", ".webp"))
                                .fitCenter()
                                .diskCacheStrategy(DiskCacheStrategy.SOURCE);

                        Glide.with(this)
                                .load(dataList.get(position)
                                        .getThumbnailUrl1()
                                        .replace("upload", "upload/so_0.01")
                                        .replace(".jpg", ".webp"))
                                .thumbnail(thumbnailRequest)
                                .dontAnimate()
                                .fitCenter()
                                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                                .into(cover);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {

                try {
                    DrawableRequestBuilder<String> thumbnailRequest = Glide.with(this)
                            .load(dataList.get(position)
                                    .getThumbnailUrl1()
                                    .replace("upload", "upload/t_media_lib_thumb")
                                    .replace(".jpg", ".webp")
                                    .replace(".jpeg", ".webp")
                                    .replace(".png", ".webp"))
                            .fitCenter()
                            .diskCacheStrategy(DiskCacheStrategy.SOURCE);

                    Glide.with(this)
                            .load(dataList.get(position)
                                    .getThumbnailUrl1()
                                    .replace("upload", "upload/so_0.01")
                                    .replace(".jpg", ".webp"))
                            .thumbnail(thumbnailRequest)
                            .dontAnimate()
                            .fitCenter()
                            .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            .into(cover);
                } catch (Exception ef) {
                    e.printStackTrace();
                }
            }
        }
    }


    @Override
    public void savedClick(int position, Boolean bookMarked) {
        if (!bookMarked) {
            mPresenter.saveToBookmark(position, dataList.get(position).getPostId());
        } else {
            mPresenter.deleteToBookmark(position, dataList.get(position).getPostId());
        }
    }

    @Override
    public void savedLongCick(int position, Boolean isSaved) {
        dataList.get(pos).setBookMarked(isSaved);
    }

    @Override
    public void savedViewClick(int position, Data data) {

    }

    @Override
    public void saveToCollectionClick(int position, Data data) {
        // This two get data get from post.
        addToCollectionPostId = data.getId();
        collectionImage = data.getThumbnailUrl1();

        Glide.with(this)
                .load(collectionImage)
                .placeholder(R.color.colorBonJour)
                .centerCrop()
                .into(iV_cImage);

        sheetCollection.setVisibility(View.VISIBLE);
        sheetCollection.post(() -> collectionBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED));

    }

    @Override
    public void onSaveToCollectionClick(int position, Data data) {

        // This two get data get from post.
        addToCollectionPostId = data.getId();
        collectionImage = data.getThumbnailUrl1();

        Glide.with(this)
                .load(collectionImage)
                .placeholder(R.color.colorBonJour)
                .centerCrop()
                .into(iV_cImage);

        sheetCollection.setVisibility(View.VISIBLE);
        sheetCollection.post(() -> collectionBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED));
    }

    @Override
    public void bookMarkPostResponse(int pos, boolean isSaved) {
        dataList.get(pos).setBookMarked(isSaved);
        //    mAdapter.notifyItemChanged(pos);
    }

    /**
     * Used to hide open keyboard
     */
    public void hideKeyboard(Context ctx) {
        InputMethodManager inputManager =
                (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        View v = getCurrentFocus();
        if (v == null) return;
        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    /**
     * Used to open keyboard
     */
    private void openKeyboard(Context ctx) {
        InputMethodManager inputManager =
                (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        View v = getCurrentFocus();
        if (v == null) return;
        inputManager.toggleSoftInputFromWindow(v.getWindowToken(), InputMethodManager.SHOW_FORCED, 0);
    }
}