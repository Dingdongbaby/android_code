package chat.hola.com.app;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.media.MediaScannerConnection;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.util.Pair;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.app.NotificationCompat;
import androidx.lifecycle.MutableLiveData;
import androidx.multidex.MultiDex;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.cloudinary.android.MediaManager;
import com.couchbase.lite.android.AndroidContext;
import com.crashlytics.android.Crashlytics;
import com.lighthusky.dingdong.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.inject.Inject;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import chat.hola.com.app.ContentObserver.ContactAddedOrUpdatedObserver;
import chat.hola.com.app.ContentObserver.ContactDeletedObserver;
import chat.hola.com.app.Database.CouchDbController;
import chat.hola.com.app.DownloadFile.FileUploadService;
import chat.hola.com.app.DownloadFile.FileUtils;
import chat.hola.com.app.DownloadFile.ServiceGenerator;
import chat.hola.com.app.Networking.HowdooService;
import chat.hola.com.app.Networking.connection.NetworkCheckerService;
import chat.hola.com.app.Notifications.NotificationGenerator;
import chat.hola.com.app.Notifications.RegistrationIntentService;
import chat.hola.com.app.Service.PublishPost;
import chat.hola.com.app.Utilities.ApiOnServer;
import chat.hola.com.app.Utilities.ClearGlideCacheAsyncTask;
import chat.hola.com.app.Utilities.ConfirmDialog;
import chat.hola.com.app.Utilities.ConnectivityReceiver;
import chat.hola.com.app.Utilities.Constants;
import chat.hola.com.app.Utilities.DeviceUuidFactory;
import chat.hola.com.app.Utilities.FileHelper;
import chat.hola.com.app.Utilities.SessionApiCall;
import chat.hola.com.app.Utilities.Utilities;
import chat.hola.com.app.authentication.login.LoginActivity;
import chat.hola.com.app.dagger.AppComponent;
import chat.hola.com.app.dagger.AppUtilModule;
import chat.hola.com.app.dagger.DaggerAppComponent;
import chat.hola.com.app.dagger.NetworkModule;
import chat.hola.com.app.manager.session.SessionManager;
import chat.hola.com.app.models.ConnectionObserver;
import chat.hola.com.app.models.NetworkConnector;
import chat.hola.com.app.models.SessionObserver;
import chat.hola.com.app.models.SocialObserver;
import chat.hola.com.app.post.model.PostData;
import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;
import io.fabric.sdk.android.Fabric;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by moda on 29/06/17.
 */
public class AppController extends DaggerApplication
        implements Application.ActivityLifecycleCallbacks {
    public static final String TAG = AppController.class.getSimpleName();
    private SessionApiCall sessionApiCall;
    private static final int NOTIFICATION_SIZE = 5;
    public static Bus bus = new Bus(ThreadEnforcer.ANY);
    private static AppController mInstance;
    /**
     * Arrays for the secret chats dTag message received
     */
    final String[] dTimeForDB = {
            "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "15", "30", "60", "3600", "86400",
            "604800"
    };//getResources().getStringArray(R.array.dTimeForDB);
    final String[] dTimeOptions = {
            "off", "1 second", "2 seconds", "3 seconds", "4 seconds", "5 seconds", "6 seconds",
            "7 seconds", "8 seconds", "9 seconds", "10 seconds", "15 seconds", "30 seconds", "1 minute",
            "1 hour", "1 day", "1 week"
    };//getResources().getStringArray(R.array.dTimeOptions);
    public boolean applicationKilled;
    @Inject
    ConnectionObserver connectionObserver;
    @Inject
    NetworkConnector networkConnector;
    ContactAddedOrUpdatedObserver contactUpdateObserver =
            new ContactAddedOrUpdatedObserver(new Handler());
    ContactDeletedObserver contactDeletedObserver = new ContactDeletedObserver(new Handler());
    private AppComponent appComponent;
    private boolean previewImagesForFiltersToBeGenerated = true;
    private boolean fullScreenCameraTobeOpened = true;
    private SharedPreferences sharedPref;
    private boolean foreground;
    private String groupChatsDocId;
    private String chatDocId;
    private String unsentMessageDocId;
    private String mqttTokenDocId;
    private String notificationDocId;
    private String statusDocId;

    private String mutedDocId;
    private String blockedDocId;

    private String callsDocId;
    private String contactsDocId;
    private String allContactsDocId; /*activeTimersDocId,*/
    private String userName;
    public static Activity activity;

    public void setApiToken(String apiToken) {
        this.apiToken = apiToken;
    }

    private String apiToken;
    private String userId;
    private String userImageUrl;
    private String userIdentifier;
    private String friendsDocId;
    /**
     * logintype-0-email
     * 1-phone  number
     */

    private int signInType;
    private String indexDocId, pushToken;
    private CouchDbController db;
    private boolean filtersUpdated = false;
    private boolean signStatusChanged = false;
    private ArrayList<Integer> excludedIds;
    private boolean signedIn = false;
    /*
     * Have put this value intentionally
     */
    private String activeReceiverId = "";
    private RequestQueue mRequestQueue;
    private ArrayList<String> colors;
    private String userDocId;
    private int activeActivitiesCount;
    /*
     * For the unread chats count
     */
    //    private int unreadChatCount = 0;
    private boolean flag = true;
    private ArrayList<HashMap<String, Object>> tokenMapping = new ArrayList<>();
    private Typeface tfRegularFont, tfMediumFont, tfSemiboldFont, tfRegularRoboto, tfBold, avenyT;
    private long timeDelta = 0;
    private String defaultProfilePicUrl =
            "http://truesample.com/wp-content/themes/truesample/img/icon-Respondent020716v2.png";
    private boolean newSignupOrLogin = false;
    private boolean activeOnACall = false;
    private String activeCallId, activeCallerId;
    private String activeSecretId = "";
    private boolean profileSaved = false;
    /**
     * Initialize for callback in the chatlist or chatmessage screen activity
     */

    private boolean callMinimized = false;
    private boolean firstTimeAfterCallMinimized = false;
    private String deviceId;
    /*
     * Utilities for the resending of the unsent messages
     */
    private Intent notificationService;
    private boolean contactSynced = false;
    private boolean registeredObserver = false;
    public ArrayList<Map<String, Object>> dirtyContacts = new ArrayList<>();
    public ArrayList<Map<String, Object>> newContacts = new ArrayList<>();
    public ArrayList<Map<String, Object>> inactiveContacts = new ArrayList<>();
    public int pendingContactUpdateRequests = 0;
    private String userStatus;
    private boolean chatSynced;
    private ArrayList<Map<String, Object>> notifications = new ArrayList<>();

    /*
     *
     * For clubbing of the notifications
     */
    private boolean activeOnVideoCall = false;
    // private boolean autoDownloadAllowed;
    private WifiManager wifiMgr;
    private WifiInfo wifiInfo;

    /*
     *For auto download of the media messages
     *
     */
    //changed here
    private boolean serviceAlreadyScheduled = false;
    /*
     *For message auto download
     */
    private String removedMessageString = "The message has been removed";
    private PublishPost publishPost;

    private boolean friendsFetched;

    public boolean isFriendsFetched() {
        return friendsFetched;
    }

    public void setFriendsFetched(boolean friendsFetched) {
        this.friendsFetched = friendsFetched;
        sharedPref.edit().putBoolean("friendsFetched", friendsFetched).apply();
    }


    public static Bus getBus() {
        return bus;
    }

    public static synchronized AppController getInstance() {
        return mInstance;
    }

    public FileHelper getFileHelper() {
        if (fileHelper == null) fileHelper = new FileHelper();
        return fileHelper;
    }

    private FileHelper fileHelper;

    public String[] getdTimeForDB() {
        return dTimeForDB;
    }

    public String[] getdTimeOptions() {
        return dTimeOptions;
    }

    public int getsignInType() {
        return signInType;
    }

    public NotificationGenerator getNotificationGenerator() {
        if (notificationGenerator == null)
            notificationGenerator = new NotificationGenerator();
        return notificationGenerator;
    }

    private NotificationGenerator notificationGenerator;

    public String getNotificationDocId() {
        return notificationDocId;
    }


    //------------- Chats message handler END-----------//

    /**
     * Convert image or video or audio to byte[] so that it can be sent on socket(Unsetn messages)
     */
    @SuppressWarnings("TryWithIdenticalCatches")

    private static byte[] convertFileToByteArray(File f) {
        byte[] byteArray = null;
        byte[] b;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        try {

            InputStream inputStream = new FileInputStream(f);
            b = new byte[2663];

            int bytesRead;

            while ((bytesRead = inputStream.read(b)) != -1) {
                bos.write(b, 0, bytesRead);
            }

            byteArray = bos.toByteArray();
        } catch (IOException e) {

        } catch (OutOfMemoryError e) {

        } finally {
            b = null;

            try {
                bos.close();
            } catch (IOException e) {

            }
        }

        return byteArray;
    }

    public boolean isPreviewImagesForFiltersToBeGenerated() {
        return previewImagesForFiltersToBeGenerated;
    }

    public boolean isFullScreenCameraTobeOpened() {
        return fullScreenCameraTobeOpened;
    }

    /*
     *Starting the network service for the internet checking. */
    private void initNetworkService() {
        startService(new Intent(this, NetworkCheckerService.class));
    }


    private SessionManager sessionManager;

    @Override
    public void onCreate() {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        super.onCreate();

        try {
            MediaManager.init(this);
        } catch (IllegalStateException e) {
            e.fillInStackTrace();
        }

        Fabric.with(this, new Crashlytics());

        mInstance = this;
        try {
            new ClearGlideCacheAsyncTask().execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //initNetworkService();
        sessionApiCall = new SessionApiCall();
        setBackgroundColorArray();

        sharedPref = this.getSharedPreferences("defaultPreferences", Context.MODE_PRIVATE);

        applicationKilled = sharedPref.getBoolean("applicationKilled", false);

        friendsFetched = sharedPref.getBoolean("friendsFetched", false);

        indexDocId = sharedPref.getString("indexDoc", null);
        db = new CouchDbController(new AndroidContext(this));

        if (indexDocId == null) {

            indexDocId = db.createIndexDocument();

            sharedPref.edit().putString("indexDoc", indexDocId).apply();
        }

        //autoDownloadAllowed = sharedPref.getBoolean("autoDownloadAllowed", true);
        pushToken = sharedPref.getString("pushToken", null);
        if (pushToken == null) {

            if (checkPlayServices() && isForeground()) {
                notificationService = new Intent(this, RegistrationIntentService.class);
                //                try {
                startService(notificationService);
                //                }catch (Exception e){
                //                    e.fillInStackTrace();
                //                }

            }
        }
        sessionManager = new SessionManager(mInstance);
        isStar = sessionManager.isStar();
        contactSynced = sharedPref.getBoolean("contactSynced", false);
        chatSynced = sharedPref.getBoolean("chatSynced", false);
        Map<String, Object> signInDetails = db.isSignedIn(indexDocId);

        signedIn = (boolean) signInDetails.get("isSignedIn");

        if (signedIn) {
            signInType = (int) signInDetails.get("signInType");

            userId = (String) signInDetails.get("signedUserId");

            profileSaved = (boolean) signInDetails.get("profileSaved");

            getUserDocIdsFromDb(userId);
            /*
             * First need to create the android client
             */

        }

        registerActivityLifecycleCallbacks(mInstance);

        tfRegularFont =
                Typeface.createFromAsset(mInstance.getAssets(), "fonts/proxima-nova-soft-regular.ttf");
        tfMediumFont = Typeface.createFromAsset(getAssets(), "fonts/ProximaNovaSoft-Medium.otf");
        tfSemiboldFont = Typeface.createFromAsset(getAssets(), "fonts/ProximaNovaSoft-Semibold.otf");
        tfRegularRoboto = Typeface.createFromAsset(getAssets(), "fonts/RobotoRegular.ttf");
        avenyT = Typeface.createFromAsset(getAssets(), "fonts/AvenyTRegular.otf");
        if (sharedPref.getBoolean("deltaRequired", true)) {
            getCurrentTime();
        } else {

            timeDelta = sharedPref.getLong("timeDelta", 0);
        }

        deviceId = new DeviceUuidFactory(this).getDeviceUuid();

        activeActivitiesCount = 0;
        fileHelper = new FileHelper();
        notificationGenerator = new NotificationGenerator();
    }

    private void copyInputStreamToOutputStream(InputStream in, PrintStream out) {
        try {
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            out.close();
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        appComponent = DaggerAppComponent.builder()
                .application(this)
                .netModule(new NetworkModule())
                .appUtilModule(new AppUtilModule())
                .build();
        appComponent.inject(this);
        return appComponent;
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public String getGroupChatsDocId() {
        return groupChatsDocId;
    }

    public void getUserDocIdsFromDb(String userId) {

        userDocId = db.getUserInformationDocumentId(indexDocId, userId);

        ArrayList<String> docIds = db.getUserDocIds(userDocId);

        chatDocId = docIds.get(0);

        unsentMessageDocId = docIds.get(1);
        mqttTokenDocId = docIds.get(2);
        //        unackMessageDocId = docIds.get(3);

        callsDocId = docIds.get(3);
        contactsDocId = docIds.get(4);
        //activeTimersDocId = docIds.get(5);
        allContactsDocId = docIds.get(5);
        statusDocId = docIds.get(6);

        notificationDocId = docIds.get(7);

        groupChatsDocId = docIds.get(8);
        mutedDocId = docIds.get(9);

        blockedDocId = docIds.get(10);

        friendsDocId = docIds.get(11);

        getUserInfoFromDb(userDocId);

        tokenMapping = db.fetchMqttTokenMapping(mqttTokenDocId);
    }

    public void getUserInfoFromDb(String docId) {

        Map<String, Object> userInfo = db.getUserInfo(docId);

        excludedIds = (ArrayList<Integer>) userInfo.get("excludedFilterIds");
        userName = (String) userInfo.get("userName");
        userIdentifier = (String) userInfo.get("userIdentifier");

        apiToken = (String) userInfo.get("apiToken");

        userStatus = (String) userInfo.get("socialStatus");

        userId = (String) userInfo.get("userId");

        userImageUrl = (String) userInfo.get("userImageUrl");

        //  Log.d("log71", apiToken + " " + userName + " " + userIdentifier + " " + userId + " " + userImageUrl);

        //        if (userImageUrl == null || userImageUrl.isEmpty()) {
        //
        //
        //            /*
        //             * Have put it as of now just for the sake of convenience, although needs to be removed later
        //             */
        //
        //
        //            userImageUrl = defaultProfilePicUrl;
        //
        //        }

        //        unreadChatCount = db.getUnreadChatsReceiverUidsCount(chatDocId);

        notifications = db.fetchAllNotifications(notificationDocId);
    }

    public ArrayList<Map<String, Object>> fetchNotificationsList() {

        return notifications;
    }

    public void setNotificationsList(ArrayList<Map<String, Object>> notificationsList) {

        notifications = notificationsList;
    }

    /**
     * To check play services before starting service for generating push token
     */

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        return resultCode == ConnectionResult.SUCCESS;
    }

    public String getPushToken() {

        return pushToken;
    }

    public void setPushToken(String token) {

        this.pushToken = token;

        if (notificationService != null) {
            stopService(notificationService);
        }
    }

    /**
     * Prepare image or audio or video file for upload
     */
    @SuppressWarnings("all")

    public File convertByteArrayToFileToUpload(byte[] data, String name, String extension) {

        File file = null;

        try {

            File folder = new File(
                    getInstance().getExternalFilesDir(null) + ApiOnServer.CHAT_UPLOAD_THUMBNAILS_FOLDER);

            if (!folder.exists() && !folder.isDirectory()) {
                folder.mkdirs();
            }

            file = new File(
                    getInstance().getExternalFilesDir(null) + ApiOnServer.CHAT_UPLOAD_THUMBNAILS_FOLDER,
                    name + extension);
            if (!file.exists()) {
                file.createNewFile();
            }
            FileOutputStream fos = new FileOutputStream(file);

            fos.write(data);
            fos.flush();
            fos.close();
        } catch (IOException e) {

        }

        return file;
    }

    /**
     * To prepare image file for upload
     */
    private int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {

        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            while ((halfHeight / inSampleSize) > reqHeight && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    private Bitmap decodeSampledBitmapFromResource(String pathName, int reqWidth, int reqHeight) {

        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(pathName, options);

        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(pathName, options);
    }

    /**
     * To upload image or video or audio to server using multipart upload to avoid OOM exception
     */
    @SuppressWarnings("TryWithIdenticalCatches,all")

    private void uploadFile(final Uri fileUri, final String name, final int messageType,
                            final JSONObject obj, final String receiverUid, final String id,
                            final HashMap<String, Object> mapTemp, final String secretId, final String extension,
                            final boolean toDelete, final boolean isGroupMessage, final Object groupMembersDocId) {

        FileUploadService service = ServiceGenerator.createService(FileUploadService.class);

        final File file = FileUtils.getFile(this, fileUri);

        String url = null;
        if (messageType == 1) {

            url = name + ".jpg";
        } else if (messageType == 2) {

            url = name + ".mp4";
        } else if (messageType == 5) {

            url = name + ".mp3";
        } else if (messageType == 7) {

            url = name + ".jpg";
        } else if (messageType == 9) {

            url = name + "." + extension;
        }

        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);

        MultipartBody.Part body = MultipartBody.Part.createFormData("photo", url, requestFile);

        String descriptionString = "Hola File Uploading";
        RequestBody description =
                RequestBody.create(MediaType.parse("multipart/form-data"), descriptionString);

        // finally, execute the request
        Call<ResponseBody> call = service.upload(description, body);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                /**
                 *
                 *
                 * has to get url from the server in response
                 *
                 *
                 * */

                try {

                    if (response.code() == 200) {

                        String url = null;
                        if (messageType == 1) {

                            url = name + ".jpg";
                        } else if (messageType == 2) {

                            url = name + ".mp4";
                        } else if (messageType == 5) {

                            url = name + ".mp3";
                        } else if (messageType == 7) {

                            url = name + ".jpg";
                        } else if (messageType == 9) {

                            url = name + "." + extension;
                        }

                        obj.put("payload",
                                Base64.encodeToString(("" + url).getBytes("UTF-8"),
                                        Base64.DEFAULT));
                        obj.put("dataSize", file.length());
                        obj.put("timestamp", new Utilities().gmtToEpoch(Utilities.tsInGmt()));

                        if (toDelete) {
                            File fdelete = new File(fileUri.getPath());
                            if (fdelete.exists()) fdelete.delete();
                        }
                    }
                } catch (JSONException e) {

                } catch (IOException e) {

                }

                /**
                 *
                 *
                 * emitting to the server the values after the file has been uploaded
                 *
                 **/


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    public void setSignedIn(final boolean signedIn, final String userId, final String userName,
                            final String userIdentifier, int signInType) {

        this.signedIn = signedIn;

        if (signedIn) {
            /*
             *
             * A temporary fix for now,have to commented out later,once things are sorted from server side
             *
             *
             * */

            AppController.getInstance().setGuest(false);

            mInstance.userId = userId;
            mInstance.userIdentifier = userIdentifier;
            mInstance.signInType = signInType;

            mInstance.userName = userName;
            newSignupOrLogin = true;

            getUserDocIdsFromDb(userId);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {




                    /*
                     *
                     * for checking internet availability
                     * */

                    if (sharedPref.getString("chatNotificationArray", null) == null) {
                        SharedPreferences.Editor prefsEditor = sharedPref.edit();

                        prefsEditor.putString("chatNotificationArray",
                                new Gson().toJson(new ArrayList<Map<String, String>>()));
                        prefsEditor.apply();
                    }
                }
            }, 1000);
        } else {
            //  deleteNewPostListener();
            flag = true;
            mInstance.userId = null;

            /*
             *
             * although userid is set to null but deviceid is still made to persist
             *
             *
             * */

            //
            //            new Handler().postDelayed(new Runnable() {
            //                @Override
            //                public void run() {
            //
            //
            //                    if (intent != null)
            //                        stopService(intent);
            //
            //
            //                }
            //            }, 1000);
        }
    }

    public long getTimeDelta() {
        return timeDelta;
    }

    public boolean getSignedIn() {
        return this.signedIn;
    }

    /**
     * Update change in signin status
     * If signed in then have to connect socket,start listening on various socket events and
     * disconnect socket in case of signout, stop listening on various socket events
     */
    public void setSignedIn(boolean signedIn) {
        this.signedIn = signedIn;
    }

    public String getChatDocId() {

        return chatDocId;
    }

    public String getUserIdentifier() {
        return userIdentifier;
    }

    public String getApiToken() {
        return apiToken;
    }

    public boolean isForeground() {
        return foreground;
    }

    public SharedPreferences getSharedPreferences() {

        return sharedPref;
    }

    public boolean isActiveOnVideoCall() {
        return activeOnVideoCall;
    }

    public boolean isSignStatusChanged() {
        return signStatusChanged;
    }

    public void setSignStatusChanged(boolean signStatusChanged) {
        this.signStatusChanged = signStatusChanged;
    }

    public String getActiveReceiverId() {

        return activeReceiverId;
    }

    public void setActiveReceiverId(String receiverId) {

        this.activeReceiverId = receiverId;
    }

    public String getActiveSecretId() {

        return activeSecretId;
    }

    public void setActiveSecretId(String secretId) {
        this.activeSecretId = secretId;
    }

    public boolean isCallMinimized() {
        return callMinimized;
    }

    public void setCallMinimized(boolean callMinimized) {
        this.callMinimized = callMinimized;
    }

    public boolean getContactSynced() {

        return contactSynced;
    }

    public void setContactSynced(boolean synced) {

        contactSynced = synced;

        sharedPref.edit().putBoolean("contactSynced", synced).apply();
        String time = String.valueOf(Utilities.getGmtEpoch());
        sharedPref.edit().putString("lastUpdated", time).apply();

        sharedPref.edit().putString("lastDeleted", time).apply();
    }

    public boolean getChatSynced() {

        return chatSynced;
    }

    public void setChatSynced(boolean synced) {
        chatSynced = synced;
        sharedPref.edit().putBoolean("chatSynced", synced).apply();
    }

    public boolean isActiveOnACall() {
        return activeOnACall;
    }

    public void setActiveOnACall(boolean activeOnACall, boolean notCallCut) {
        this.activeOnACall = activeOnACall;

        if (!activeOnACall && notCallCut) {

            this.callMinimized = false;
        }
    }

    public boolean isFirstTimeAfterCallMinimized() {
        return firstTimeAfterCallMinimized;
    }

    public void setFirstTimeAfterCallMinimized(boolean firstTimeAfterCallMinimized) {
        this.firstTimeAfterCallMinimized = firstTimeAfterCallMinimized;
    }

    public CouchDbController getDbController() {

        return db;
    }

    public void setApplicationKilled(boolean applicationKilled) {

        this.applicationKilled = applicationKilled;
        //        sharedPref.edit().putBoolean("applicationKilled", applicationKilled).commit();

        sharedPref.edit().putBoolean("applicationKilled", applicationKilled).apply();

        if (applicationKilled) {

            unregisterContactsObserver();
            sharedPref.edit().putString("lastSeenTime", Utilities.tsInGmt()).apply();
        }
    }

    public boolean profileSaved() {
        return profileSaved;
    }

    public void setProfileSaved(boolean profileSaved) {
        this.profileSaved = profileSaved;
    }

    public int getSignInType() {
        return signInType;
    }

    //typefaces changed..
    public Typeface getRegularFont() {
        return tfRegularFont;
    }

    public Typeface getBoldFont() {
        return tfBold;
    }

    public Typeface getSemiboldFont() {
        return tfSemiboldFont;
    }

    public Typeface getAvenyT() {
        return avenyT;
    }

    public Typeface getMediumFont() {
        return tfMediumFont;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;

        db.updateUserName(db.getUserDocId(userId, indexDocId), userName);
    }

    public String getUserId() {
        return userId;
    }

    public String getUserStatus() {
        return userStatus;
    }

    /**
     * To update the user status
     */

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;

        db.updateUserStatus(db.getUserDocId(userId, indexDocId), userStatus);
    }

    public String getUserImageUrl() {
        return userImageUrl;
    }

    public void setUserImageUrl(String userImageUrl) {
        this.userImageUrl = userImageUrl;

        db.updateUserImageUrl(db.getUserDocId(userId, indexDocId), userImageUrl);
    }

    public String getDefaultUserImageUrl() {
        return defaultProfilePicUrl;
    }

    public int getActiveActivitiesCount() {
        return activeActivitiesCount;
    }

    public String getDeviceId() {

        return deviceId;
    }

    public void setActiveCallId(String activeCallId) {
        this.activeCallId = activeCallId;
    }

    public void setActiveCallerId(String activeCallerId) {

        this.activeCallerId = activeCallerId;
    }
    /*
     * volley methods to hit api on server
     */

    public String getunsentMessageDocId() {
        return unsentMessageDocId;
    }

    public String getContactsDocId() {
        return contactsDocId;
    }

    public String getFriendsDocId() {
        return friendsDocId;
    }

    //    public void decrementUnreadChatCount() {
    //        unreadChatCount--;
    //    }
    //
    //    public void incrementChatCount() {
    //
    //
    //        unreadChatCount++;
    //    }

    public String getAllContactsDocId() {
        return allContactsDocId;
    }

    public String getCallsDocId() {
        return callsDocId;
    }

    public String getMutedDocId() {
        return mutedDocId;
    }

    public String getBlockedDocId() {
        return blockedDocId;
    }

    public String getIndexDocId() {

        return indexDocId;
    }

    public String getStatusDocId() {

        return statusDocId;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            //            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
            mRequestQueue = Volley.newRequestQueue(getApplicationContext(), hurlStack);
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    private void setBackgroundColorArray() {
        colors = new ArrayList<>();
        colors.add("#FFCDD2");
        colors.add("#D1C4E9");
        colors.add("#B3E5FC");
        colors.add("#C8E6C9");
        colors.add("#FFF9C4");
        colors.add("#FFCCBC");
        colors.add("#CFD8DC");
        colors.add("#F8BBD0");
        colors.add("#C5CAE9");
        colors.add("#B2EBF2");
        colors.add("#DCEDC8");
        colors.add("#FFECB3");
        colors.add("#D7CCC8");
        colors.add("#F5F5F5");
        colors.add("#FFE0B2");
        colors.add("#F0F4C3");
        colors.add("#B2DFDB");
        colors.add("#BBDEFB");
        colors.add("#E1BEE7");
    }

    /*
     * To save the message received to the local couchdb for the normal and the group chat
     */

    public String getColorCode(int position) {
        return colors.get(position);
    }


    public void updatePresence(int status, boolean applicationKilled) {

        if (signedIn) {

            if (status == 0) {

                /*
                 * Background
                 */

                try {
                    JSONObject obj = new JSONObject();
                    obj.put("status", 0);

                    if (applicationKilled) {
                        if (sharedPref.getString("lastSeenTime", null) != null) {
                            obj.put("timestamp", sharedPref.getString("lastSeenTime", null));
                        } else {

                            obj.put("timestamp", Utilities.tsInGmt());
                        }
                    } else {

                        obj.put("timestamp", Utilities.tsInGmt());
                    }
                    obj.put("userId", userId);

                    obj.put("lastSeenEnabled", sharedPref.getBoolean("enableLastSeen", true));

                } catch (JSONException w) {

                }
            } else {

                /*
                 *Foreground
                 */

                //                if (!applicationKilled) {
                try {
                    JSONObject obj = new JSONObject();
                    obj.put("status", 1);
                    obj.put("userId", userId);
                    obj.put("lastSeenEnabled", sharedPref.getBoolean("enableLastSeen", true));

                } catch (JSONException w) {

                }
                //    }
            }
        }
    }

    public void updateTokenMapping() {

        if (signedIn) {
            db.addMqttTokenMapping(mqttTokenDocId, tokenMapping);
        }
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle bundle) {

        activeActivitiesCount++;


        /*
         *
         * As app can also be started when clicked on the chat notification
         *
         *
         * */

        //        Log.d("log1", activity.getClass().getSimpleName());
        if (AppController.getInstance().getSignedIn() && activity.getClass()
                .getSimpleName()
                .equals("MainActivity")) {
            //            new PublishPost().retryPublishingPosts();
            //            sendAcknowledgements();
            foreground = true;

            setApplicationKilled(false);
            updatePresence(1, false);


            /*
             *
             * When the app started,update the status(JUST have put it for checking)
             *
             */

            //  if (signedIn) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("status", 1);
                AppController.getInstance().setActiveOnACall(false, true);
            } catch (JSONException e) {

            }
            //      }
        }
    }

    @Override
    public void onActivityDestroyed(Activity activity) {

        if (activity.getClass().getSimpleName().equals("ContactSyncLandingPage")) {

            checkActiveCalls();
        }

        //        Log.d("log2", activity.getClass().getSimpleName());
        activeActivitiesCount--;
    }

    @Override
    public void onActivityPaused(Activity activity) {

        //        Log.d("log3", activity.getClass().getSimpleName());
    }

    @Override
    public void onActivityResumed(Activity activity) {

        Log.d("log4", activity.getClass().getSimpleName());
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

    }

    @Override
    public void onActivityStarted(Activity activity) {

    }

    @Override
    public void onActivityStopped(Activity activity) {

        //        Log.d("log5", activity.getClass().getSimpleName());
    }

    //    /**
    //     * To remove the message to be deleted from list of the active timers
    //     */
    //
    //
    //    public void removeMessageToBeDeleted(String docId, String messageId) {
    //
    //
    //    }

    @SuppressWarnings("unchecked")
    public void getCurrentTime() {

        new FetchTime().execute();
    }

    public String randomString() {
        char[] chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < 20; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }

        sb.append("PnPLabs3Embed");
        return sb.toString();
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    public void registerContactsObserver() {

        if (contactSynced && !registeredObserver) {
            registeredObserver = true;

            getContentResolver().registerContentObserver(ContactsContract.Contacts.CONTENT_URI, false,
                    contactUpdateObserver);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                getContentResolver().registerContentObserver(ContactsContract.DeletedContacts.CONTENT_URI,
                        false, contactDeletedObserver);
            }
        }
    }

    public void unregisterContactsObserver() {

        if (contactSynced) {

            getContentResolver().unregisterContentObserver(contactUpdateObserver);

            getContentResolver().unregisterContentObserver(contactDeletedObserver);
            registeredObserver = false;
        }
    }

    /**
     * @param arrayList containing the list of the contact ids which have been deleted
     */

    public void updateDeletedContact(ArrayList<String> arrayList) {

        ArrayList<Map<String, Object>> allContactDetails = db.fetchAllContacts(allContactsDocId);
        Map<String, Object> localContact;
        JSONObject obj;
        JSONArray arr = new JSONArray();

        for (int i = 0; i < allContactDetails.size(); i++) {
            localContact = allContactDetails.get(i);

            if (arrayList.contains(localContact.get("contactId"))) {

                obj = new JSONObject();
                try {

                    obj.put("number", localContact.get("phoneNumber"));

                    obj.put("type", localContact.get("type"));
                } catch (JSONException e) {

                }

                arr.put(obj);
            }
        }
        if (arr.length() > 0) {

            try {
                obj = new JSONObject();

                obj.put("contacts", arr);

            } catch (JSONException e) {

            }
        }
    }

    /**
     * If a new contact has been added
     */

    public void putUpdatedContact(ArrayList<Map<String, Object>> arrayList) {

        ArrayList<Map<String, Object>> allContactDetails = db.fetchAllContacts(allContactsDocId);

        Map<String, Object> localContact;
        JSONObject obj;
        JSONArray arr = new JSONArray();

        boolean contactAlreadyExists;

        Map<String, Object> contactChanged;

        for (int j = 0; j < arrayList.size(); j++) {

            contactAlreadyExists = false;

            contactChanged = arrayList.get(j);

            for (int i = 0; i < allContactDetails.size(); i++) {
                localContact = allContactDetails.get(i);

                /*
                 * Considering only change of the phone number and the name in the contacts list
                 */

                if ((contactChanged.get("contactId")).equals(localContact.get("contactId"))) {
                    contactAlreadyExists = true;

                    if (!((contactChanged.get("phoneNumber")).equals(localContact.get("phoneNumber")))) {

                        /*
                         * This means phone number has been changed,so will need to send to the server
                         */

                        obj = new JSONObject();
                        try {

                            obj.put("number", contactChanged.get("phoneNumber"));

                            obj.put("type", contactChanged.get("type"));
                        } catch (JSONException e) {

                        }
                        arr.put(obj);

                        Map<String, Object> wasActiveContact =
                                db.wasActiveContact(contactsDocId, (String) contactChanged.get("contactId"));

                        if ((boolean) wasActiveContact.get("wasActive")) {
                            /*
                             * Follow whose number has been changed was on active contacts list
                             */

                            Map<String, Object> dirtyContact = new HashMap<>();
                            dirtyContact.put("contactUid", wasActiveContact.get("contactUid"));
                            dirtyContact.put("contactNumber", contactChanged.get("phoneNumber"));
                            dirtyContact.put("contactIdentifier", wasActiveContact.get("contactIdentifier"));

                            dirtyContact.put("contactId", contactChanged.get("contactId"));

                            dirtyContact.put("contactName", contactChanged.get("userName"));

                            dirtyContacts.add(dirtyContact);
                        } else {
                            /*
                             * Follow was in list of the inactive contacts
                             */

                            Map<String, Object> inactiveContact = new HashMap<>();
                            inactiveContact.put("contactUid", wasActiveContact.get("contactUid"));
                            inactiveContact.put("phoneNumber", contactChanged.get("phoneNumber"));
                            inactiveContact.put("contactIdentifier", wasActiveContact.get("contactIdentifier"));

                            inactiveContact.put("contactId", contactChanged.get("contactId"));

                            inactiveContact.put("userName", contactChanged.get("userName"));

                            inactiveContacts.add(inactiveContact);
                        }
                        db.updateAllContactNunber(allContactsDocId, (String) contactChanged.get("phoneNumber"),
                                (String) contactChanged.get("contactId"));
                    }

                    if (!((contactChanged.get("userName")).equals(localContact.get("userName")))) {



                        /*
                         * If just the name changed,then update the value in the local contacts,calls and chat list but no need to send to the server
                         */

                        try {

                            Map<String, Object> result =
                                    db.updateActiveContactName(contactsDocId, (String) contactChanged.get("userName"),
                                            (String) contactChanged.get("contactId"));

                            if ((boolean) result.get("updated")) {

                                /*
                                 * If the contact exists in list of active users
                                 */

                                JSONObject jsonObject = new JSONObject();

                                jsonObject.put("eventName", "ContactNameUpdated");
                                jsonObject.put("contactName", contactChanged.get("userName"));
                                jsonObject.put("contactUid", result.get("contactUid"));
                                bus.post(jsonObject);
                            }


                            /*
                             * To update the list of all contacts for the new name
                             */
                            db.updateAllContactName(allContactsDocId, (String) contactChanged.get("userName"),
                                    (String) contactChanged.get("contactId"));
                        } catch (JSONException e) {

                        }
                    }
                }
            }

            if (!contactAlreadyExists) {

                obj = new JSONObject();
                try {

                    obj.put("number", contactChanged.get("phoneNumber"));

                    obj.put("type", contactChanged.get("type"));

                    newContacts.add(contactChanged);
                } catch (JSONException e) {

                }
                arr.put(obj);

                db.addToAllContacts(allContactsDocId, contactChanged);
            }
        }

        if (arr.length() > 0) {

            try {
                obj = new JSONObject();

                obj.put("contacts", arr);

            } catch (JSONException e) {

            }
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    public void checkContactAddedOrUpdated() {

        ArrayList<Map<String, Object>> localContactsList = new ArrayList<>();

        ArrayList<String> alreadyAddedContacts = new ArrayList<>();

        Uri uri = ContactsContract.Contacts.CONTENT_URI;

        /*
         * For added or updated contact
         */
        String time = String.valueOf(Utilities.getGmtEpoch());
        String lastChecked =
                AppController.getInstance().getSharedPreferences().getString("lastUpdated", time);
        try {

            Cursor cur = AppController.getInstance()
                    .getContentResolver()
                    .query(uri, null, ContactsContract.Contacts.CONTACT_LAST_UPDATED_TIMESTAMP + ">=?",
                            new String[]{lastChecked}, null);

            Map<String, Object> contactsInfo;
            if (cur != null && cur.getCount() > 0) {

                int type;

                while (cur.moveToNext()) {

                    String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));

                    String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

                    if (Integer.parseInt(
                            cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                        Cursor pCur =
                                getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id},
                                        null);

                        if (pCur != null) {

                            while (pCur.moveToNext()) {

                                String phoneNo = pCur.getString(
                                        pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                                /*
                                 * To replace brackets or the dashes or the spaces
                                 *
                                 * */

                                phoneNo = phoneNo.replaceAll("[\\D\\s\\-()]", "");

                                /*
                                 * To get all the phone numbers for the given contacts
                                 */
                                if (phoneNo != null && !phoneNo.trim().equals("null") && !phoneNo.trim()
                                        .isEmpty()) {


                                    /*
                                     * By default assuming the number is of unknown type
                                     */

                                    type = -1;
                                    switch (pCur.getInt(
                                            pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE))) {
                                        case ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE:
                                            type = 1;
                                            break;
                                        case ContactsContract.CommonDataKinds.Phone.TYPE_HOME:
                                            type = 0;
                                            break;
                                        case ContactsContract.CommonDataKinds.Phone.TYPE_PAGER:
                                            type = 8;
                                    }

                                    if (!alreadyAddedContacts.contains(phoneNo)) {

                                        alreadyAddedContacts.add(phoneNo);
                                        contactsInfo = new HashMap<>();

                                        contactsInfo.put("phoneNumber", "+" + phoneNo);
                                        contactsInfo.put("userName", name);
                                        contactsInfo.put("type", type);
                                        contactsInfo.put("contactId", id);

                                        localContactsList.add(contactsInfo);
                                    }
                                }
                            }

                            pCur.close();
                        }
                    }
                }

                if (localContactsList.size() > 0) putUpdatedContact(localContactsList);
            }

            if (cur != null) {
                cur.close();
            }
        } catch (SQLiteException e) {
            e.printStackTrace();

            generatePushForContactUpdate();
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    public void checkContactDeleted() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            Uri uri = ContactsContract.DeletedContacts.CONTENT_URI;

            /*
             * For deleted contact
             */
            String time = String.valueOf(Utilities.getGmtEpoch());
            String lastChecked =
                    AppController.getInstance().getSharedPreferences().getString("lastDeleted", time);

            Cursor cur = getContentResolver().query(uri, null,
                    ContactsContract.DeletedContacts.CONTACT_DELETED_TIMESTAMP + ">=?",
                    new String[]{lastChecked}, null);

            if (cur != null && cur.getCount() > 0) {

                ArrayList<String> deletedContactIds = new ArrayList<>();

                while (cur.moveToNext()) {

                    String id =
                            cur.getString(cur.getColumnIndex(ContactsContract.DeletedContacts.CONTACT_ID));

                    deletedContactIds.add(id);
                }

                /*
                 * Have to tell the server of the contact being deleted and also to remove from the local list
                 */

                updateDeletedContact(deletedContactIds);
            }

            if (cur != null) {
                cur.close();
            }
        }
    }

    /*
     * To remove a particular notification
     */

    public void removeNotification(String notificationId) {

        boolean found = false;
        for (int i = 0; i < notifications.size(); i++) {

            if (notifications.get(i).get("notificationId").equals(notificationId)) {
                notifications.remove(i);
                found = true;
                break;
            }
        }

        if (found) {

            //  db.getParticularNotificationId(notificationDocId, notificationId);

            int systemNotificationId = db.removeNotification(notificationDocId, notificationId);
            if (systemNotificationId != -1) {

                NotificationManager nMgr =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                nMgr.cancel(notificationId, systemNotificationId);
            }
        }
    }


    /*
     *
     *To allow the option of auto downloading of the media,when on wifi
     *
     */

    public boolean checkMobileDataOn() {

        try {

            ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo info = cm.getActiveNetworkInfo();

            return (info != null
                    && info.isConnected()
                    && info.getType() == ConnectivityManager.TYPE_MOBILE);
        } catch (Exception e) {

            return false;
        }
    }

    @SuppressWarnings("all")
    public boolean writeResponseBodyToDisk(ResponseBody body, String filePath, String messageType,
                                           final String messageId, int replyType) {

        try {
            // todo change the file location/name according to your needs

            File folder = new File(getInstance().getExternalFilesDir(null) + "/hola");

            if (!folder.exists() && !folder.isDirectory()) {
                folder.mkdirs();
            }

            File file = new File(filePath);

            if (!file.exists()) {
                file.createNewFile();
            }

            InputStream inputStream = null;
            OutputStream outputStream = null;

            try {

                byte[] fileReader = new byte[4096];

                final long fileSize = body.contentLength();

                inputStream = body.byteStream();
                outputStream = new FileOutputStream(file);

                while (true) {

                    int read = inputStream.read(fileReader);

                    if (read == -1) {
                        break;
                    }

                    outputStream.write(fileReader, 0, read);
                }

                outputStream.flush();

                return true;
            } catch (ArrayIndexOutOfBoundsException e) {
                return false;
            } catch (IOException e) {
                return false;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }

                if (outputStream != null) {
                    outputStream.close();
                }
            }
        } catch (IOException e) {
            return false;
        }
    }


    /*
     * Will try to download the media message asynchronously in the background
     *
     */

    /**
     * For generating the push notification incase contact updated on below api level 18 devices.
     */
    private void generatePushForContactUpdate() {

        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this).setSmallIcon(R.drawable.notification)
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                        .setContentTitle(getString(R.string.app_name))

                        .setContentText(getString(R.string.NotificationContent))
                        .setTicker(getString(R.string.NotificationTitle))
                        .setAutoCancel(true)
                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                        .setContentIntent(
                                PendingIntent.getActivity(getApplicationContext(), 0, new Intent(), 0))

                        .setDefaults(Notification.DEFAULT_VIBRATE)
                        .setPriority(Notification.PRIORITY_HIGH);

        if (Build.VERSION.SDK_INT >= 21) notificationBuilder.setVibrate(new long[0]);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        /*
         *
         * Notification id is used to notify the same notification
         *
         * */

        notificationManager.notify(-1, notificationBuilder.build());
    }

    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }

    public void createNewPostListener(HowdooService howdooService,
                                      SocialObserver socialShareObserver) {

        if (publishPost == null) {
            publishPost = new PublishPost();
        }
        publishPost.retryPublishingPosts(howdooService, socialShareObserver);
    }

    public void addNewPost(PostData postData, SocialObserver socialObserver) {

        if (publishPost != null) {

            publishPost.addNewPost(postData, socialObserver);
        }
    }

    public void addNewPost(PostData postData) {

        if (publishPost != null) {

            publishPost.addNewPost(postData);
        }
    }

    public ArrayList<Integer> getExcludedFilterIds() {
        return excludedIds;
    }

    public void setExcludedFilterIds(ArrayList<Integer> excludedIds) {
        this.excludedIds = excludedIds;
        db.updateExcludedFilters(userDocId, excludedIds);
    }

    public boolean isFiltersUpdated() {
        return filtersUpdated;
    }

    public void setFiltersUpdated(boolean filtersUpdated) {
        this.filtersUpdated = filtersUpdated;
    }

    public void refreshMediaGallery(String path) {

        for (int i = 0; i < 17; i++) {

            try {

                MediaScannerConnection.scanFile(this, new String[]{path + (i) + ".jpg"}, null,
                        new MediaScannerConnection.OnScanCompletedListener() {
                            /**
                             *(non-Javadoc)
                             *@see android.media.MediaScannerConnection.OnScanCompletedListener#onScanCompleted(java.lang.String, android.net.Uri)
                             */
                            public void onScanCompleted(String path, Uri uri) {

                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void notifyOnImageDeleteRequired(String tempPath) {
        try {

            MediaScannerConnection.scanFile(this, new String[]{tempPath}, null,
                    new MediaScannerConnection.OnScanCompletedListener() {
                        /*
                         *   (non-Javadoc)
                         * @see android.media.MediaScannerConnection.OnScanCompletedListener#onScanCompleted(java.lang.String, android.net.Uri)
                         */
                        public void onScanCompleted(String path, Uri uri) {

                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("TryWithIdenticalCatches")
    private class FetchTime extends AsyncTask {

        @Override
        protected Object doInBackground(Object[] params) {

            sharedPref.edit().putBoolean("deltaRequired", true).apply();

            String url_ping = "https://google.com/";
            URL url = null;
            try {
                url = new URL(url_ping);
            } catch (MalformedURLException e) {

            }

            try {
                /*
                 * Maybe inaccurate due to network inaccuracy
                 */

                HttpURLConnection urlc = (HttpURLConnection) url.openConnection();

                if (urlc.getResponseCode() == 200 || urlc.getResponseCode() == 503) {
                    long dateStr = urlc.getDate();
                    //Here I do something with the Date String

                    timeDelta = System.currentTimeMillis() - dateStr;

                    sharedPref.edit().putBoolean("deltaRequired", false).apply();
                    sharedPref.edit().putLong("timeDelta", timeDelta).apply();
                    urlc.disconnect();
                }
            } catch (IOException e) {


                /*
                 * Should disable user from using the app
                 */

            } catch (NullPointerException e) {

            }
            return null;
        }
    }

    private boolean isStar;

    public void setStar(boolean star) {
        isStar = star;
    }

    public void logOutAndDisconnectMqtt() {
        setSignedIn(false, "", "", "", -1);
        setSignStatusChanged(false);
    }

    private void initializeFirebase() {
        FirebaseInstanceId.getInstance()
                .getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token

                        pushToken = task.getResult().getToken();
                        sharedPref.edit().putString("pushToken", pushToken).apply();
                    }
                });
    }

    private void checkActiveCalls() {

        String callId = getCallId();
        String activeCallerId = getCallerId();

        if (callId != null && activeCallerId != null) {

            JSONObject obj = new JSONObject();
            try {
                obj.put("callId", callId);
                obj.put("userId", getUserId());
                obj.put("type", 2);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        removeCurrentCallDetails();
    }

    public void saveCurrentCallDetails(String callId, String callerId) {

        sharedPref.edit().putString("callId", callId).apply();
        sharedPref.edit().putString("callerId", callerId).apply();
    }

    public void removeCurrentCallDetails() {

        sharedPref.edit().putString("callId", null).apply();
        sharedPref.edit().putString("callerId", null).apply();
    }

    public String getCallId() {
        return sharedPref.getString("callId", null);
    }

    public String getCallerId() {
        return sharedPref.getString("callerId", null);
    }

    HurlStack hurlStack = new HurlStack() {
        @Override
        protected HttpURLConnection createConnection(java.net.URL url) throws IOException {
            HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super.createConnection(url);
            try {
                httpsURLConnection.setSSLSocketFactory(getSSLSocketFactory(getApplicationContext()));
                // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return httpsURLConnection;
        }
    };

    private SSLSocketFactory getSSLSocketFactory(Context context)
            throws CertificateException, KeyStoreException, IOException, NoSuchAlgorithmException,
            KeyManagementException {

        CertificateFactory cf = CertificateFactory.getInstance("X.509");
        InputStream caInput = context.getResources().openRawResource(R.raw.dingdong);// TODO add your crt file

        Certificate ca = cf.generateCertificate(caInput);
        caInput.close();

        KeyStore keyStore = KeyStore.getInstance("BKS");

        keyStore.load(null, null);
        keyStore.setCertificateEntry("ca", ca);

        String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
        TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
        tmf.init(keyStore);

        TrustManager[] wrappedTrustManagers = getWrappedTrustManagers(tmf.getTrustManagers());

        SSLContext sslContext = SSLContext.getInstance("TLS");
        sslContext.init(null, wrappedTrustManagers, null);

        return sslContext.getSocketFactory();
    }

    private TrustManager[] getWrappedTrustManagers(TrustManager[] trustManagers) {
        final X509TrustManager originalTrustManager = (X509TrustManager) trustManagers[0];
        return new TrustManager[]{
                new X509TrustManager() {
                    public X509Certificate[] getAcceptedIssuers() {
                        return originalTrustManager.getAcceptedIssuers();
                    }

                    public void checkClientTrusted(X509Certificate[] certs, String authType) {
                        try {
                            if (certs != null && certs.length > 0) {
                                certs[0].checkValidity();
                            } else {
                                originalTrustManager.checkClientTrusted(certs, authType);
                            }
                        } catch (CertificateException e) {
                            Log.w("checkClientTrusted", e.toString());
                        }
                    }

                    public void checkServerTrusted(X509Certificate[] certs, String authType) {
                        try {
                            if (certs != null && certs.length > 0) {
                                certs[0].checkValidity();
                            } else {
                                originalTrustManager.checkServerTrusted(certs, authType);
                            }
                        } catch (CertificateException e) {
                            Log.w("checkServerTrusted", e.toString());
                        }
                    }
                }
        };
    }

    public SSLSocketFactory getSocketFactory(int certificateId, String certificatePassword) {
        SSLSocketFactory result = mSocketFactoryMap.get(certificateId);
        if ((null == result) && (null != getApplicationContext())) {

            try {
                KeyStore keystoreTrust = KeyStore.getInstance("BKS");
                keystoreTrust.load(getApplicationContext().getResources().
                        openRawResource(certificateId), certificatePassword.toCharArray());
                TrustManagerFactory trustManagerFactory =
                        TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
                trustManagerFactory.init(keystoreTrust);
                SSLContext sslContext = SSLContext.getInstance("TLS");
                sslContext.init(null, trustManagerFactory.getTrustManagers(), new SecureRandom());
                result = sslContext.getSocketFactory();
                mSocketFactoryMap.put(certificateId, result);
            } catch (Exception ex) {
            }
        }
        return result;
    }

    private HashMap<Integer, SSLSocketFactory> mSocketFactoryMap =
            new HashMap<Integer, SSLSocketFactory>();


    @Override
    public void onActivityPostResumed(@NonNull Activity atv) {
        activity = atv;
    }

    /**
     * used to get the live data object for dicoonect call
     */

    MutableLiveData<Pair<Boolean, String>> disconnectCallLiveData =
            new MutableLiveData<Pair<Boolean, String>>();

    public MutableLiveData<Pair<Boolean, String>> getDisconnectCallLiveData() {
        return disconnectCallLiveData;
    }

    public SessionManager getSessionManager() {
        return sessionManager;
    }

    public boolean isGuest() {
        return sharedPref.getBoolean(Constants.GUEST_USER, false);
    }

    public void setGuest(boolean guest) {
        sharedPref.edit().putBoolean(Constants.GUEST_USER, guest).apply();
    }

    public void openSignInDialog(Context mContext) {
        ConfirmDialog dialog = new ConfirmDialog(mContext, R.drawable.welcome_screen_logo, mContext.getString(R.string.sign_in_message), mContext.getString(R.string.login), mContext.getString(R.string.cancel));
        dialog.show();

        Button btnYes = dialog.findViewById(R.id.btnYes);
        btnYes.setOnClickListener(v1 -> {
            Intent intent2 = new Intent(this, LoginActivity.class);
            intent2.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            mContext.startActivity(intent2);
            dialog.dismiss();
        });

        dialog.show();
    }
}