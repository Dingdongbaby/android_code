package chat.hola.com.app.comment.model;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;


import com.lighthusky.dingdong.R;

import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import chat.hola.com.app.Utilities.TypefaceManager;

/**
 * <h1>ViewHolder</h1>
 *
 * @author 3embed
 * @version 1.0
 * @since 4/9/2018
 */

public class ViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.tvUserName)
    TextView tvUserName;
    @BindView(R.id.tvMessage)
    TextView tvMessage;
    @BindView(R.id.ivCommentProfilePic)
    ImageView ivCommentProfilePic;
    @BindView(R.id.tvTime)
    TextView tvTime;
    @BindView(R.id.item)
    FrameLayout item;
    private ClickListner clickListner;

    public ViewHolder(View itemView, TypefaceManager typefaceManager, ClickListner clickListner) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        tvUserName.setTypeface(typefaceManager.getSemiboldFont());
        tvMessage.setTypeface(typefaceManager.getMediumFont());
        tvTime.setTypeface(typefaceManager.getRegularFont());
        this.clickListner = clickListner;
    }

    @OnClick(R.id.item)
    public void itemSelect() {
        clickListner.itemSelect(getAdapterPosition(), !item.isSelected());
    }

    @OnClick({R.id.ivCommentProfilePic, R.id.tvUserName})
    public void profile() {
        clickListner.onUserClick(getAdapterPosition());
    }


}