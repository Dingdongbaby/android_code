package chat.hola.com.app.home;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.PopupMenu;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.signature.StringSignature;
import com.lighthusky.dingdong.R;
import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.squareup.otto.Bus;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import chat.hola.com.app.AppController;
import chat.hola.com.app.Dialog.BlockDialog;
import chat.hola.com.app.Dialog.ImageSourcePicker;
import chat.hola.com.app.DublyCamera.CameraActivity;
import chat.hola.com.app.DublyCamera.CameraInFragments.CameraInFragmentsActivity;
import chat.hola.com.app.ImageCropper.CropImage;
import chat.hola.com.app.Utilities.BottomNavigationViewHelper;
import chat.hola.com.app.Utilities.Constants;
import chat.hola.com.app.Utilities.Loader;
import chat.hola.com.app.Utilities.TypefaceManager;
import chat.hola.com.app.Utilities.Utilities;
import chat.hola.com.app.activities_user.UserActivitiesActivity;
import chat.hola.com.app.home.activity.ActivitiesFragment;
import chat.hola.com.app.home.contact.ContactFragment;
import chat.hola.com.app.home.home.HomeFragment;
import chat.hola.com.app.home.popular.PopularFragment;
import chat.hola.com.app.home.profile.ProfileFragment;
import chat.hola.com.app.home.trending.TrendingFragment;
import chat.hola.com.app.manager.session.SessionManager;
import chat.hola.com.app.models.ConnectionObserver;
import chat.hola.com.app.models.IPServicePojo;
import chat.hola.com.app.models.NetworkConnector;
import chat.hola.com.app.models.WalletResponse;
import chat.hola.com.app.post.model.UploadObserver;
import chat.hola.com.app.profileScreen.ProfileActivity;
import chat.hola.com.app.profileScreen.discover.DiscoverActivity;
import chat.hola.com.app.search.SearchActivity;
import dagger.android.support.DaggerAppCompatActivity;

/**
 * <h1>LandingActivity</h1>
 *
 * <p> It is main activity which attaches and redirect fragments
 * {@link CameraActivity} {@link TrendingFragment}
 * {@link ContactFragment} {@link UserActivitiesActivity} {@link CameraActivity}
 * Fetching current location and IP address
 * Loading profile data</p>
 *
 * @author Shaktisinh Jadeja
 * @version 1.0
 * @since 21/2/2018
 */

public class LandingActivity extends DaggerAppCompatActivity
        implements LandingContract.View,
        BottomNavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = LandingActivity.class.getSimpleName();
    private final int CAMERA_REQUEST = 222;
    private final int RESULT_LOAD_IMAGE = 1;
    private static final int REQUEST_CODE = 111;
    //    private final int READ_STORAGE_REQ_CODE = 26;

    public static boolean isConnect = false;
    private Unbinder unbinder;
    private FragmentTransaction ft;
    private boolean doubleBackToExitPressedOnce = false;
    private boolean isStar = false;
    private static Bus bus = AppController.getBus();

    //    @Inject
    //    SocialObserver socialShareObserver;
    @Inject
    ConnectionObserver connectionObserver;
    @Inject
    BlockDialog dialog;
    @Inject
    LandingPresenter presenter;
    @Inject
    TypefaceManager typefaceManager;
    @Inject
    UploadObserver uploadObserver;
    @Inject
    HomeFragment homeFragment;
    @Inject
    PopularFragment popularFragment;
    @Inject
    TrendingFragment trendingFragment;
    @Inject
    ContactFragment contactFragment;
    @Inject
    ProfileFragment profileFragment;
    @Inject
    SessionManager sessionManager;
    @Inject
    ActivitiesFragment activitiesFragment;

    @BindView(R.id.bottomNavigationView)
    public BottomNavigationView bottomNavigationView;
    @BindView(R.id.actionBarRl)
    RelativeLayout actionBarRl;
    @BindView(R.id.profilePicIv)
    public ImageView ivProfilePic;
    @BindView(R.id.searchIv)
    ImageView searchIv;
    @BindView(R.id.iV_plus)
    public ImageView iV_plus;
    @BindView(R.id.root)
    RelativeLayout root;
    @BindView(R.id.frameLayout)
    FrameLayout frameLayout;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.linearPostTabs)
    public LinearLayout linearPostTabs;
    private TextView chatBadge;
    private boolean isMoveDone = false;
    Loader loader;
    private boolean isPopular = true;
    @BindView(R.id.tVFollowing)
    TextView tVFollowing;
    @BindView(R.id.tVForYou)
    TextView tVForYou;
    @BindView(R.id.iVcamera)
    public ImageView iVcamera;

    @BindView(R.id.drawerLayout)
    public DrawerLayout drawerLayout;
    @BindView(R.id.right_drawer)
    public LinearLayout right_drawer;
    @BindView(R.id.ivDp)
    ImageView ivDp;
    @BindView(R.id.tvName)
    TextView tVName;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        supportRequestWindowFeature(AppCompatDelegate.FEATURE_SUPPORT_ACTION_BAR_OVERLAY);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.landing_page);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        unbinder = ButterKnife.bind(this);
        bus.register(this);

        tVFollowing.setTypeface(typefaceManager.getSemiboldFont());
        tVForYou.setTypeface(typefaceManager.getSemiboldFont());

        loader = new Loader(this);
        //set app badge count
        setBadge(this, 0);

        //get current location details
        new IPAddress().execute();

        BottomNavigationMenuView bottomNavigationMenuView =
                (BottomNavigationMenuView) bottomNavigationView.getChildAt(0);
        View v = bottomNavigationMenuView.getChildAt(3);
        BottomNavigationItemView itemView = (BottomNavigationItemView) v;

        bottomNavigationView.setItemIconTintList(null);

        View badge = LayoutInflater.from(this).inflate(R.layout.notification_badge, itemView, true);

        chatBadge = (TextView) badge.findViewById(R.id.tV_badge);

        /*When open app we create connect fragment this is required
         * for showing bottom badge while user on other fragment*/
        removeShift(bottomNavigationView, 0);

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close) {

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                Log.d("onDrawerSlide", ":" + slideOffset);
            }
        };

        drawerLayout.addDrawerListener(actionBarDrawerToggle);

        if (!AppController.getInstance().isFriendsFetched()) presenter.friends();

        //        socialShareObserver.getObservable().subscribeOn(Schedulers.io())
        //                .observeOn(AndroidSchedulers.mainThread())
        //                .subscribe(new DisposableObserver<ShareWith>() {
        //                    @Override
        //                    public void onNext(ShareWith shareWith) {
        //
        //                        Log.i("SOCIAL - FACEBOOK", "" + shareWith.isFacebook());
        //                        Log.i("SOCIAL - TWITTER", "" + shareWith.isTwitter());
        //                        Log.i("SOCIAL - INSTAGRAM", "" + shareWith.isInstagram());
        //                    }
        //
        //                    @Override
        //                    public void onError(Throwable e) {
        //
        //                    }
        //
        //                    @Override
        //                    public void onComplete() {
        //
        //                    }
        //                });
        presenter.getPendingCalls();
    }

    @OnClick({R.id.iVSettings, R.id.tVSettings})
    public void onSettingsClick() {
    }

    @OnClick(R.id.rLDiscover)
    public void openDiscoverPeople() {
        Intent intent = new Intent(LandingActivity.this, DiscoverActivity.class);
        intent.putExtra("caller", "LandingActivity");
        intent.putExtra("is_contact", false);
        startActivity(intent);
    }

    /**
     * <p>It sets the title of the screen from attached Fragment<p/>
     *
     * @param txt      : title text
     * @param typeface : font name
     */
    public void setTitle(String txt, Typeface typeface) {
        title.setText(txt);
        title.setTypeface(typeface);
    }

    /**
     * opens alert popup dialog when user is blocked from admin
     */
    @Override
    public void userBlocked() {
        dialog.show();
    }

    @OnClick(R.id.iVcamera)
    public void cameraClick() {
        if (AppController.getInstance().isGuest()) {
            AppController.getInstance().openSignInDialog(this);
            return;
        }
        isStar = false;
        openCamera();
    }

    @OnClick({R.id.tVFollowing})
    public void followingClick() {

        if (AppController.getInstance().isGuest()) {
            AppController.getInstance().openSignInDialog(this);
            return;
        }

        if (isPopular)
            changeTab(false, true);
    }

    @OnClick({R.id.tVForYou})
    public void forYouClick() {
        if (!isPopular)
            changeTab(true, true);
    }

    public void changeTab(boolean isPopular, boolean isClickTab) {
        this.isPopular = isPopular;

        if (isClickTab) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            ft = fragmentManager.beginTransaction();
        }

        if (isPopular) {
            /*Popularfragment need to show*/

            homeFragment.resetMediaPlayer();

            tVForYou.setTextColor(ContextCompat.getColor(this, R.color.color_white));
            tVFollowing.setTextColor(ContextCompat.getColor(this, R.color.star_grey));

            if (homeFragment.isAdded()) {
                ft.hide(homeFragment);
            }
            if (!popularFragment.isAdded()) {
                pushFragment(popularFragment);
            } else {
                ft.show(popularFragment);
                ft.commitAllowingStateLoss();
                popularFragment.changeVisibilityOfViews();
                popularFragment.resetPlayBackOnList();
            }

        } else {
            /*Homefragment need to show*/

            popularFragment.resetMediaPlayer();

            tVForYou.setTextColor(ContextCompat.getColor(this, R.color.star_grey));
            tVFollowing.setTextColor(ContextCompat.getColor(this, R.color.color_white));

            if (popularFragment.isAdded()) {
                ft.hide(popularFragment);
            }
            if (!homeFragment.isAdded()) {
                pushFragment(homeFragment);
            } else {
                ft.show(homeFragment);
                ft.commitAllowingStateLoss();
                homeFragment.changeVisibilityOfViews();
                homeFragment.resetPlayBackOnList();
            }
        }


    }

    @Override
    public void removeShift(BottomNavigationView bottomNavigationView, int position) {
        Menu menu = bottomNavigationView.getMenu();
        selectFragment(menu.getItem(position));
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return selectFragment(item);
    }

    @Override
    public boolean selectFragment(MenuItem item) {
        item.setChecked(true);
        FragmentManager fragmentManager = getSupportFragmentManager();
        ft = fragmentManager.beginTransaction();
        isStar = false;
        if (item.getItemId() == R.id.friends) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("isStar", isStar);
            //contactFragment.setArguments(bundle);
            //profileFragment.setArguments(bundle);
        }
        return fragment(item.getItemId());
    }

    @OnClick(R.id.liveStream)
    public void liveStream() {
        isStar = false;
        startActivity(new Intent(this, CameraActivity.class).putExtra("isLiveStream", true)
                .putExtra("fromLandingActivity", true));
        this.overridePendingTransition(R.anim.left_enter, R.anim.left_exit);
        finish();
    }

    @OnClick(R.id.activity)
    public void activity() {
        isStar = false;
        startActivity(new Intent(this, UserActivitiesActivity.class));
    }


    /**
     * <p>It controls bottom navigation menu<p/>
     *
     * @param itemId : ID of selected menu option
     */
    private boolean fragment(int itemId) {
        isConnect = false;
        switch (itemId) {
            case R.id.home:
                isStar = false;

                if (trendingFragment.isAdded()) {
                    ft.hide(trendingFragment);
                }
                if (activitiesFragment.isAdded()) {
                    ft.hide(activitiesFragment);
                }
                if (profileFragment.isAdded()) {
                    ft.hide(profileFragment);
                }

                changeTab(isPopular, false);

                bottomNavigationView.setBackgroundColor(getResources().getColor(R.color.transparent));
                bottomNavigationView.setItemIconTintList(ContextCompat.getColorStateList(this, R.color.landing_icon_all_white));
                sessionManager.setHomeTab(0);
                return true;

            case R.id.trending:

                if (AppController.getInstance().isGuest()) {
                    AppController.getInstance().openSignInDialog(this);
                    return false;
                }

                isStar = false;
                homeFragment.resetMediaPlayer();
                popularFragment.resetMediaPlayer();
                if (homeFragment.isAdded()) {
                    ft.hide(homeFragment);
                }
                if (popularFragment.isAdded()) {
                    ft.hide(popularFragment);
                }
                if (activitiesFragment.isAdded()) {
                    ft.hide(activitiesFragment);
                }
                if (profileFragment.isAdded()) {
                    ft.hide(profileFragment);
                }
                if (!trendingFragment.isAdded()) {
                    pushFragment(trendingFragment);
                } else {
                    ft.show(trendingFragment);
                    ft.commitAllowingStateLoss();
                    trendingFragment.changeVisibilityOfViews();
                }
                bottomNavigationView.setBackgroundColor(getResources().getColor(R.color.color_white));
                bottomNavigationView.setItemIconTintList(null);
                sessionManager.setHomeTab(1);
                return true;

            case R.id.camera:

                if (AppController.getInstance().isGuest()) {
                    AppController.getInstance().openSignInDialog(this);
                    return false;
                }

                isStar = false;
                openCamera();
                return true;
            case R.id.friends:

                if (AppController.getInstance().isGuest()) {
                    AppController.getInstance().openSignInDialog(this);
                    return false;
                }

                homeFragment.resetMediaPlayer();
                popularFragment.resetMediaPlayer();

                if (trendingFragment.isAdded()) {
                    ft.hide(trendingFragment);
                }
                if (activitiesFragment.isAdded()) {
                    ft.hide(activitiesFragment);
                }
                if (homeFragment.isAdded()) {
                    ft.hide(homeFragment);
                }
                if (popularFragment.isAdded()) {
                    ft.hide(popularFragment);
                }
                if (!profileFragment.isAdded()) {
                    pushFragment(profileFragment);
                } else {
                    ft.show(profileFragment);
                    ft.commitAllowingStateLoss();
                    profileFragment.changeVisibilityOfViews();
                }
                bottomNavigationView.setBackgroundColor(getResources().getColor(R.color.color_white));
                bottomNavigationView.setItemIconTintList(null);
                sessionManager.setHomeTab(4);
                return true;
            case R.id.chat:

                if (AppController.getInstance().isGuest()) {
                    AppController.getInstance().openSignInDialog(this);
                    return false;
                }

                isStar = false;
                homeFragment.resetMediaPlayer();
                popularFragment.resetMediaPlayer();
                if (homeFragment.isAdded()) {
                    ft.hide(homeFragment);
                }
                if (popularFragment.isAdded()) {
                    ft.hide(popularFragment);
                }
                if (trendingFragment.isAdded()) {
                    ft.hide(trendingFragment);
                }
                if (profileFragment.isAdded()) {
                    ft.hide(profileFragment);
                }
                if (!activitiesFragment.isAdded()) {

                    pushFragment(activitiesFragment);
                } else {
                    ft.show(activitiesFragment);
                    ft.commitAllowingStateLoss();
                    activitiesFragment.changeVisibilityOfViews();
                }
                bottomNavigationView.setBackgroundColor(getResources().getColor(R.color.color_white));
                bottomNavigationView.setItemIconTintList(null);
                sessionManager.setHomeTab(3);
                return true;
            default:
                sessionManager.setHomeTab(0);
                return false;
        }
    }

    public void fullScreenFrame() {

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        RelativeLayout.LayoutParams params =
                new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT);
        frameLayout.setLayoutParams(params);
    }

    public void removeFullScreenFrame() {

        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        RelativeLayout.LayoutParams params =
                new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT);
        params.addRule(RelativeLayout.BELOW, R.id.actionBarRl);
        params.addRule(RelativeLayout.ABOVE, R.id.bottomNavigationViewLl);
        frameLayout.setLayoutParams(params);
    }

    @Override
    public void pushFragment(Fragment fragment) {
        try {
            ft.add(R.id.frameLayout, fragment);
            ft.commitAllowingStateLoss();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void hideActionBar() {
        actionBarRl.setVisibility(View.GONE);
    }

    @Override
    public void visibleActionBar() {
        actionBarRl.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        profilepic(sessionManager.getUserProfilePic(),
                sessionManager.isStar(),
                sessionManager.getUserName(),
                sessionManager.isBusinessProfileAvailable(),
                sessionManager.isBusinessProfile());

        int menuIndex = getIntent().getIntExtra("menuIndex", sessionManager.getHomeTab());
        removeShift(bottomNavigationView, menuIndex);
        BottomNavigationViewHelper.removeShiftMode(bottomNavigationView);

        AppController.getInstance().setConnectivityListener(presenter);
        String call = getIntent().getStringExtra("caller");
        if (!isMoveDone && call != null && call.equals("PostActivity")) {
            if (homeFragment.isAdded()) {
                if (trendingFragment.isAdded()) {
                    ft.hide(trendingFragment);
                }
                if (activitiesFragment.isAdded()) {
                    ft.hide(activitiesFragment);
                }
                if (profileFragment.isAdded()) {
                    ft.hide(profileFragment);
                }
                if (popularFragment.isAdded()) {
                    ft.hide(popularFragment);
                }
                if (homeFragment.isAdded()) {
                    ft.show(homeFragment);
                }
                sessionManager.setHomeTab(0);
            }
            isMoveDone = true;
            bottomNavigationView.setSelectedItemId(R.id.home);
        }
    }

    @Override
    protected void onDestroy() {
        try {
            bus.unregister(this);
            super.onDestroy();
            if (unbinder != null) unbinder.unbind();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void openCamera() {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            if (AppController.getInstance().isFullScreenCameraTobeOpened()) {
                                startActivity(
                                        new Intent(LandingActivity.this, CameraActivity.class).putExtra("call",
                                                "post"));
                            } else {
                                startActivity(
                                        new Intent(LandingActivity.this, CameraInFragmentsActivity.class).putExtra(
                                                "call", "post"));
                            }
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            showMessage(null, R.string.permission_requires);
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions,
                                                                   PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                })
                .onSameThread()
                .check();
    }

    @Override
    public void launchImagePicker(Intent intent) {
        startActivityForResult(intent, RESULT_LOAD_IMAGE);
    }

    @Override
    public void showSnackMsg(int msgId) {
        String msg = getResources().getString(msgId);
        Snackbar snackbar = Snackbar.make(root, "" + msg, Snackbar.LENGTH_SHORT);
        snackbar.show();
        View view = snackbar.getView();
        ((TextView) view.findViewById(com.google.android.material.R.id.snackbar_text)).setGravity(
                Gravity.CENTER_HORIZONTAL);
    }

    @Override
    public void launchCropImage(Uri data) {
        CropImage.activity(data).start(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
            presenter.instagramShare(data.getStringExtra(Constants.TYPE),
                    data.getStringExtra(Constants.PATH));
        } else if (requestCode == RESULT_LOAD_IMAGE) {
            presenter.parseMedia(requestCode, resultCode, data);
        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            presenter.parseCropedImage(requestCode, resultCode, data);
        } else if (requestCode == 123) {
//            connectFragment.selectTab(1);
            removeShift(bottomNavigationView, 4);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 26) {
            if (grantResults.length == 2
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.checkSelfPermission(LandingActivity.this,
                        Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    presenter.launchImagePicker();
                } else {
                    //Access storage permission denied
                    showSnackMsg(R.string.string_1006);
                }
            } else {
                //Access storage permission denied
                showSnackMsg(R.string.string_1006);
            }
        } else if (requestCode == REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startActivity(new Intent(this, CameraActivity.class));
            }
        }
    }

    @OnClick(R.id.searchIv)
    public void search() {
        Intent intent = new Intent(this, SearchActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.profilePicIv)
    public void profilePic() {
        startActivity(new Intent(LandingActivity.this, ProfileActivity.class));
    }

    @OnClick(R.id.iV_plus)
    public void chatOptions() {
    }

    /**
     * <p>It handles back press. If first item is selected and click back twice it will exit the app
     * otherwise redirects to first item<p/>
     */
    @Override
    public void onBackPressed() {
        if (homeFragment.isVisible() || popularFragment.isVisible()) {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();

                supportFinishAfterTransition();
                //actionBarRl.setVisibility(View.VISIBLE);
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            showMessage(null, R.string.back_press_message);

            new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 2000);
        } else {
            removeShift(bottomNavigationView, 0);
        }
    }

    @Override
    public void showMessage(String msg, int msgId) {
        Toast.makeText(this, msg != null && !msg.isEmpty() ? msg : getResources().getString(msgId),
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void intenetStatusChanged(boolean isConnected) {
        //showSnack(isConnected);
    }

    @Override
    public void profilepic(String profilePic, boolean isStar, String userName,
                           boolean isBusinessProfileActive, boolean businessProfile) {
        try {
            Glide.with(this).load(profilePic).asBitmap().centerCrop()
                    .signature(new StringSignature(
                            AppController.getInstance().getSessionManager().getUserProfilePicUpdateTime()))
                    //    .signature(new StringSignature(String.valueOf(System.currentTimeMillis())))
                    //.diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true)
                    .placeholder(R.drawable.ic_profile_tab)
                    .into(new BitmapImageViewTarget(ivProfilePic) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            RoundedBitmapDrawable circularBitmapDrawable =
                                    RoundedBitmapDrawableFactory.create(getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            ivProfilePic.setImageDrawable(circularBitmapDrawable);
                        }
                    });
        } catch (Exception ignored) {
            ignored.printStackTrace();
        }
    }

    @Override
    public void sessionExpired() {
        sessionManager.sessionExpired(getApplicationContext());
    }

    private class IPAddress extends AsyncTask<Void, Void, IPServicePojo> {
        @Override
        protected IPServicePojo doInBackground(Void... voids) {
            String stringUrl = "https://ipinfo.io/json";
            try {
                URL url = new URL(stringUrl);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                StringBuffer response = new StringBuffer();
                BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String inputLine;
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                Log.d("Update Profile", "ipAddress2: " + response.toString());
                Gson gson = new Gson();
                return gson.fromJson(response.toString(), IPServicePojo.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(IPServicePojo s) {
            super.onPostExecute(s);
            if (s != null) {
                Log.d("Update Profile", "IPADDRESS onPostExecute: " + s);
                Log.d("Update Profile", "IPADDRESS onPostExecute: " + s.getIp());
                sessionManager.setIpAdress(s.getIp());
                sessionManager.setCity(s.getCity());
                sessionManager.setCountry(s.getCountry());
            }
        }
    }

    @Override
    public void isInternetAvailable(boolean flag) {

    }

    @Override
    public void reload() {

    }

    /**
     * to update app badge count
     *
     * @param context : activity context
     * @param count   : badge count (pass 0 if you want to hide)
     */
    public static void setBadge(Context context, int count) {
        String launcherClassName = getLauncherClassName(context);
        if (launcherClassName == null) {
            return;
        }
        Intent intent = new Intent("android.intent.action.BADGE_COUNT_UPDATE");
        intent.putExtra("badge_count", count);
        intent.putExtra("badge_count_package_name", context.getPackageName());
        intent.putExtra("badge_count_class_name", launcherClassName);
        context.sendBroadcast(intent);
    }

    /**
     * to get launcher class name
     *
     * @param context : activity context
     */
    public static String getLauncherClassName(Context context) {

        PackageManager pm = context.getPackageManager();

        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);

        List<ResolveInfo> resolveInfos = pm.queryIntentActivities(intent, 0);
        for (ResolveInfo resolveInfo : resolveInfos) {
            String pkgName = resolveInfo.activityInfo.applicationInfo.packageName;
            if (pkgName.equalsIgnoreCase(context.getPackageName())) {
                String className = resolveInfo.activityInfo.name;
                return className;
            }
        }
        return null;
    }

    public void updateChatBadge(boolean visible, int count) {

        try {
            if (visible) {
                chatBadge.setText(String.valueOf(count));
                chatBadge.setVisibility(View.VISIBLE);
                //unreadCount.setText(String.valueOf(count));
                //chatBadge_rl.setVisibility(View.VISIBLE);
            } else {
                chatBadge.setText(getString(R.string.double_inverted_comma));
                chatBadge.setVisibility(View.GONE);
                //chatBadge_rl.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void showBalance(WalletResponse.Data.Wallet data) {

    }

    @Override
    public void showLoader() {
        loader.show();
    }

    @Override
    public void hideLoader() {
        if (loader != null && loader.isShowing())
            loader.dismiss();
    }

    @Override
    public void gotoWalletDashboard(Integer verificationStatus) {

    }
}
