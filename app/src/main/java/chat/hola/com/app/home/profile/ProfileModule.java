package chat.hola.com.app.home.profile;

import chat.hola.com.app.dagger.FragmentScoped;
import dagger.Binds;
import dagger.Module;

/**
 * Created by ankit on 22/2/18.
 */

//@ActivityScoped
@Module
public interface ProfileModule {



    @FragmentScoped
    @Binds
    ProfileContract.Presenter presenter(ProfilePresenter presenter);

//    @ActivityScoped
//    @Binds
//    ProfileContract.View view(ProfileActivity profileActivity);

}
