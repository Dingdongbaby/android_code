package chat.hola.com.app.home.activity.followingTab;

import java.util.ArrayList;

import chat.hola.com.app.Utilities.BasePresenter;
import chat.hola.com.app.Utilities.BaseView;
import chat.hola.com.app.home.activity.followingTab.model.Following;

/**
 * @author 3Embed.
 * @since 21/2/18.
 */

public interface FollowingContract {

    interface View extends BaseView {

        void initPostRecycler();

        void showFollowings(ArrayList<Following> followings);

        void onUserClicked(String userId);

        void onMediaClick(int position, android.view.View view);

        void loading(boolean isLoading);
    }

    interface Presenter extends BasePresenter<FollowingContract.View> {

        void init();

        void loadFollowing();
    }
}
