package chat.hola.com.app.Utilities;

import android.annotation.SuppressLint;

import com.amazonaws.regions.Regions;

import java.util.Locale;

/**
 * @author 3Embed
 * @since 2/26/2018.
 */

public interface Constants {

    String EMPTY = "";
    String AUTH = Utilities.getThings();
    @SuppressLint("ConstantLocale")
    String LANGUAGE = Locale.getDefault().getLanguage();
    String TYPE = "type";
    String PATH = "path";
    String DEFAULT_PROFILE_PIC_LINK = "https://res.cloudinary.com/dafszph29/image/upload/c_thumb,w_200,g_face/v1551361911/default/profile_pic.png";//"https://res.cloudinary.com/dqodmo1yc/image/upload/c_thumb,w_200,g_face/v1536126847/default/profile_one.png";
    String IMAGE_QUALITY = "upload/q_80/";
    String PROFILE_PIC_SHAPE = "upload/w_90,h_90,c_thumb,g_face,r_max/";
    int PAGE_SIZE = 20;
    String BUSINESS = "business";
    CharSequence IMAGE_EXT = ".webp";
    String APP_TYPE = "user";
    String URL = "url";
    String GUEST_USER = "is_guest_user";

    interface AmazonS3 {

        String PROFILE_PIC_FOLDER = "profile_image";
        String COVER_PIC_FOLDER = "background_image";

        String BASE_URL = "https://s3.ap-south-1.amazonaws.com";
        String BUCKET = "dingdong3";
        String POOL_ID = "ap-south-1:f625e2ca-3b8b-42d3-8bc1-b435d3b9e46f";
        Regions REGION = Regions.AP_SOUTH_1;
    }

    interface SocialFragment {
        String USERID = "userId";
        String DATA = "data";
    }

    interface Verification {
        int EMAIL_VERIFICATION_REQ = 554;
        int NUMBER_VERIFICATION_REQ = 552;
    }

    interface Camera {
        int FILE_SIZE = 40;
    }

    interface Mqtt {
        String EVENT_NAME = "eventName";
    }

    interface DiscoverContact {
        String CONTACT_IDENTIFIRE = "contactIdentifier";
        String CONTACT_PICTURE = "contactPicUrl";
        String CONTACT_NAME = "contactName";
        String CONTACTS = "contacts";
        String PRIVATE = "private";
        String LOCAL_NUMBER = "localNumber";
        String USER_NAME = "userName";
        String ID = "_id";
        String NUMBER = "number";
        String PROFILE_PICTURE = "profilePic";
        String SOCIAL_STATUS = "socialStatus";
        String FOLLOWING = "followStatus";
        String PROFILE_PICTURE_URL = "contactPicUrl";
        String CONTACT_ID = "contactId";
        String TYPE = "type";
        String CONTACT_LOCAL_NUMBER = "contactLocalNumber";
        String CONTACT_LOCAL_ID = "contactLocalId";
        String CONTACT_UID = "contactUid";
        String CONTACT_TYPE = "contactType";
        String CONTACT_STATUS = "contactStatus";
        String PHONE_NUMBER = "phoneNumber";
        String FRIEND_STATUS_CODE = "friendStatusCode";
    }

    interface Google {
        String GOOGLE_API_KEY = "AIzaSyC510jlZhSuTTFXLDJ5k0qO2e93jkHGeZY";
    }

    interface Post {
        String PATH_FILE = "file://";
        String PATH = "path";
        String TYPE = "type";
        String IMAGE = "image";
        String VIDEO = "video";

        String RESOURCE_TYPE = "resource_type";
        String URL = "url";
        String PUBLIC_ID = "public_id";
        String FOLDER = "folder";
        String LIVE_STREAM = "live_stream";
    }


    interface PostUpdate {
        String HOME_FRAGMENT = "home_fragment";
        String POPULAR_FRAGMENT = "popular_fragment";
        String SOCIAL_DETAIL = "social_detail_activity";
    }
}
