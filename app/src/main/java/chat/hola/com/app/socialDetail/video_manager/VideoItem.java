package chat.hola.com.app.socialDetail.video_manager;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.util.DisplayMetrics;
import android.view.View;

import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;

import chat.hola.com.app.AppController;
import chat.hola.com.app.Utilities.CountFormat;
import chat.hola.com.app.Utilities.TagSpannable;
import chat.hola.com.app.Utilities.Utilities;
import chat.hola.com.app.home.model.Data;
import chat.hola.com.app.models.Business;
import chat.hola.com.app.socialDetail.ViewHolder;

import com.bumptech.glide.DrawableRequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.signature.StringSignature;
import com.lighthusky.dingdong.R;
import com.volokh.danylo.video_player_manager.manager.VideoPlayerManager;
import com.volokh.danylo.video_player_manager.meta.MetaData;
import com.volokh.danylo.video_player_manager.ui.VideoPlayerView;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <h1></h1>
 *
 * @author DELL
 * @version 1.0
 * @since 11/30/2018.
 */

public class VideoItem extends BaseVideoItem {

    private String POST_TYPE_REGULAR = "Regular";
    //    private final Picasso mImageLoader;

    private final RequestManager mImageLoader;
    private final Data data;
    private int likes;
    private String userId;
    private String postId;
    private String musicId;
    private String channelId;
    private String categoryId;
    private Context context;

    private DisplayMetrics displayMetrics = Resources.getSystem().getDisplayMetrics();
    private double screenRatio = ((double) displayMetrics.heightPixels / displayMetrics.widthPixels);

    @SuppressWarnings("unchecked")
    public VideoItem(VideoPlayerManager videoPlayerManager, RequestManager imageLoader, Data data,
                     Context context) {
        //    public VideoItem(VideoPlayerManager videoPlayerManager, Picasso imageLoader, Data data, Context context) {
        super(videoPlayerManager);
        mImageLoader = imageLoader;
        this.data = data;
        this.context = context;
    }

    @SuppressWarnings("TryWithIdenticalCatches")
    @Override
    public void update(int position, ViewHolder viewHolder, VideoPlayerManager videoPlayerManager) {
        viewHolder.mCover.setVisibility(View.VISIBLE);

        try {

            double ratio = ((double) Integer.parseInt(data.getImageUrl1Height())) / Integer.parseInt(
                    data.getImageUrl1Width());

            if (ratio > screenRatio) {
                try {
                    DrawableRequestBuilder<String> thumbnailRequest = Glide.with(context)
                            .load(data.getThumbnailUrl1()
                                    .replace("upload", "upload/t_media_lib_thumb")
                                    .replace(".jpg", ".webp")
                                    .replace(".jpeg", ".webp")
                                    .replace(".png", ".webp"))
                            .centerCrop()
                            .diskCacheStrategy(DiskCacheStrategy.SOURCE);

                    mImageLoader.load(
                            data.getThumbnailUrl1().replace("upload", "upload/so_0.01").replace(".jpg", ".webp"))
                            .thumbnail(thumbnailRequest)
                            .dontAnimate()
                            .centerCrop()
                            .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            .into(viewHolder.mCover);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    DrawableRequestBuilder<String> thumbnailRequest = Glide.with(context)
                            .load(data.getThumbnailUrl1()
                                    .replace("upload", "upload/t_media_lib_thumb")
                                    .replace(".jpg", ".webp")
                                    .replace(".jpeg", ".webp")
                                    .replace(".png", ".webp"))
                            .fitCenter()
                            .diskCacheStrategy(DiskCacheStrategy.SOURCE);

                    mImageLoader.load(
                            data.getThumbnailUrl1().replace("upload", "upload/so_0.01").replace(".jpg", ".webp"))
                            .thumbnail(thumbnailRequest)
                            .dontAnimate()
                            .fitCenter()
                            .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            .into(viewHolder.mCover);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {

            try {
                DrawableRequestBuilder<String> thumbnailRequest = Glide.with(context)
                        .load(data.getThumbnailUrl1()
                                .replace("upload", "upload/t_media_lib_thumb")
                                .replace(".jpg", ".webp")
                                .replace(".jpeg", ".webp")
                                .replace(".png", ".webp"))
                        .fitCenter()
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE);

                mImageLoader.load(
                        data.getThumbnailUrl1().replace("upload", "upload/so_0.01").replace(".jpg", ".webp"))
                        .thumbnail(thumbnailRequest)
                        .dontAnimate()
                        .fitCenter()
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .into(viewHolder.mCover);
            } catch (IllegalArgumentException ef) {
                e.printStackTrace();
            } catch (NullPointerException ef) {
                e.printStackTrace();
            }
        }

        displayData(viewHolder, data);
    }

    @Override
    public void playNewVideo(MetaData currentItemMetaData, VideoPlayerView player,
                             VideoPlayerManager<MetaData> videoPlayerManager) {
        if (data.getMediaType1() == 1) {
            //            videoPlayerManager.playNewVideo(currentItemMetaData, player, data.getImageUrl1());
            videoPlayerManager.playNewVideo(currentItemMetaData, player,
                    Utilities.getModifiedVideoLink(data.getImageUrl1()));
        } else {
            videoPlayerManager.stopAnyPlayback(true);
        }
    }

    @Override
    public void stopPlayback(VideoPlayerManager videoPlayerManager) {
        videoPlayerManager.stopAnyPlayback(true);
    }

    //display data
    @SuppressLint("ClickableViewAccessibility")
    public void displayData(ViewHolder holder, Data data) {
        userId = data.getUserId();
        likes = Integer.parseInt(data.getLikesCount());
        postId = data.getPostId();
        boolean isChannel = data.getChannelId() != null && !data.getChannelId().isEmpty();

        holder.mPlayer.setRotation(data.getOrientation() == null ? 0 : data.getOrientation());

        /* BUSINESS POST START*/
        Business business = data.getBusiness();
        boolean isBusiness =
                business != null && !business.getBusinessPostType().equalsIgnoreCase("regular");
        holder.rlAction.setVisibility(
                isBusiness && !business.getBusinessPostTypeLabel().equalsIgnoreCase(POST_TYPE_REGULAR)
                        ? View.VISIBLE : View.GONE);
        if (isBusiness) {
            if (business.getBusinessButtonColor() != null && !business.getBusinessButtonColor()
                    .isEmpty()) {
                holder.rlAction.setBackgroundColor(Color.parseColor(business.getBusinessButtonColor()));
            }
            holder.tvActionButton.setText(business.getBusinessButtonText());
            if (business.getBusinessPrice() != null) {
                String currencySymbol = business.getBusinessCurrency();
                holder.tvPrice.setText(currencySymbol + " " + business.getBusinessPrice());
            }
        }
        /* BUSINESS POST END*/

        if (isBusiness) {
            holder.tvUserName.setText(business.getBusinessName());
            Glide.with(context)
                    .load(business.getBusinessProfilePic())
                    .asBitmap()
                    .centerCrop()
                    .signature(new StringSignature(
                            AppController.getInstance().getSessionManager().getUserProfilePicUpdateTime()))
                    //.diskCacheStrategy(DiskCacheStrategy.NONE)
                    //    .skipMemoryCache(true)
                    .into(new BitmapImageViewTarget(holder.ivProfilePic) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            RoundedBitmapDrawable circularBitmapDrawable =
                                    RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            holder.ivProfilePic.setImageDrawable(circularBitmapDrawable);
                        }
                    });
        } else {
            String pic = isChannel ? data.getChannelImageUrl() : data.getProfilepic();
            Glide.with(context)
                    .load(pic)
                    .asBitmap()
                    .signature(new StringSignature(
                            AppController.getInstance().getSessionManager().getUserProfilePicUpdateTime()))
                    .centerCrop()
                    //.diskCacheStrategy(DiskCacheStrategy.NONE)
                    //.skipMemoryCache(true)
                    .into(new BitmapImageViewTarget(holder.ivProfilePic) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            RoundedBitmapDrawable circularBitmapDrawable =
                                    RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            holder.ivProfilePic.setImageDrawable(circularBitmapDrawable);
                        }
                    });

            holder.tvUserName.setText(isChannel ? data.getChannelName() : data.getUsername());
        }
        //        holder.ivProfilePic.setImageURI(data.getProfilepic());

        holder.ivStarBadge.setVisibility(data.isStar() ? View.VISIBLE : View.GONE);
        //        holder.tvUserName.setText(data.getUsername());
        holder.tvViewCount.setText(CountFormat.format(Long.parseLong(data.getDistinctViews())));
        holder.tvLikeCount.setText(CountFormat.format(Long.parseLong(String.valueOf(likes))));
        try {
            holder.tvCommentCount.setText(CountFormat.format(Long.parseLong(data.getCommentCount())));
        } catch (NumberFormatException e) {
            holder.tvCommentCount.setText("0");
        }
        if (AppController.getInstance().getUserId() != null)
            holder.ibFollow.setVisibility(AppController.getInstance().getUserId().equals(userId) ? View.GONE : View.VISIBLE);
        //        holder.cbFollow.setChecked(data.getFollowStatus() != 0);
        //        holder.ivLike.setChecked(data.isLiked());

        //music
        if (data.getDub() != null && data.getDub().getId() != null && !data.getDub()
                .getId()
                .isEmpty()) {
            holder.llMusic.setVisibility(View.VISIBLE);
            holder.tvMusic.setText(data.getDub().getName());
            musicId = data.getDub().getId();
        } else {
            holder.llMusic.setVisibility(View.GONE);
        }

        //title
        if (data.getTitle() != null && !data.getTitle().trim().equals("")) {
            holder.tvTitle.setVisibility(View.VISIBLE);
            SpannableString spanString = new SpannableString(data.getTitle());
            Matcher matcher = Pattern.compile("#([A-Za-z0-9_-]+)").matcher(spanString);
            findMatch(spanString, matcher);
            Matcher userMatcher = Pattern.compile("@([A-Za-z0-9_-]+)").matcher(spanString);
            findMatch(spanString, userMatcher);
            holder.tvTitle.setText(spanString);
            holder.tvTitle.setMovementMethod(LinkMovementMethod.getInstance());
        } else {
            holder.tvTitle.setVisibility(View.GONE);
        }

        //location
        if (data.getPlace() != null && !data.getPlace().trim().equals("")) {
            holder.tvLocation.setVisibility(View.VISIBLE);
            holder.tvLocation.setText(data.getPlace());
        } else {
            holder.tvLocation.setVisibility(View.GONE);
        }
        //divide
        if (!data.getChannelName().isEmpty() && !data.getCategoryName().isEmpty()) {
            holder.tvDivide.setVisibility(View.VISIBLE);
        } else {
            holder.tvDivide.setVisibility(View.GONE);
        }

        //category
        if (data.getCategoryName() != null && !data.getCategoryName().trim().equals("")) {
            holder.tvCategory.setVisibility(View.VISIBLE);
            holder.tvCategory.setText(data.getCategoryName());
            categoryId = data.getCategoryId();
        } else {

            holder.tvCategory.setVisibility(View.GONE);
        }

        //channel
        //        if (data.getChannelImageUrl() != null) {
        //            holder.tvChannel.setVisibility(View.VISIBLE);
        //            holder.tvChannel.setText(data.getChannelName());
        //            channelId = data.getChannelId();
        //        } else {
        //
        //            holder.tvChannel.setVisibility(View.GONE);
        //        }

        if (data.getBookMarked()) {
            holder.ibSaved.setImageDrawable(context.getDrawable(R.drawable.ic_saved));
        } else {
            holder.ibSaved.setImageDrawable(context.getDrawable(R.drawable.ic_unsaved));
        }
    }

    private void findMatch(SpannableString spanString, Matcher matcher) {
        while (matcher.find()) {
            final String tag = matcher.group(0);
            spanString.setSpan(new TagSpannable(context, tag, R.color.color_white), matcher.start(),
                    matcher.end(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
    }
}
