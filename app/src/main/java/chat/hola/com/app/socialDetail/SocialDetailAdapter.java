package chat.hola.com.app.socialDetail;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.lighthusky.dingdong.R;
import com.volokh.danylo.video_player_manager.manager.VideoPlayerManager;

import java.util.List;

import chat.hola.com.app.AppController;
import chat.hola.com.app.Utilities.OnSwipeTouchListener;
import chat.hola.com.app.Utilities.TextSpannable;
import chat.hola.com.app.home.model.Data;
import chat.hola.com.app.models.Business;
import chat.hola.com.app.socialDetail.video_manager.BaseVideoItem;
import chat.hola.com.app.socialDetail.video_manager.ClickListner;

/**
 * <h1></h1>
 *
 * @author DELL
 * @version 1.0
 * @since 11/27/2018.
 */
public class SocialDetailAdapter extends RecyclerView.Adapter<ViewHolder> {
    private final VideoPlayerManager mVideoPlayerManager;
    private List<BaseVideoItem> baseVideoItems;
    private List<Data> dataList;
    private Context context;
    private ClickListner clickListner;
    private int height, width;
    private static final DecelerateInterpolator DECCELERATE_INTERPOLATOR =
            new DecelerateInterpolator();
    private static final AccelerateInterpolator ACCELERATE_INTERPOLATOR =
            new AccelerateInterpolator();

    public SocialDetailAdapter(VideoPlayerManager mVideoPlayerManager, Context context,
                               List<BaseVideoItem> baseVideoItems, List<Data> dataList, int width, int height) {
        this.mVideoPlayerManager = mVideoPlayerManager;
        this.context = context;
        this.baseVideoItems = baseVideoItems;
        this.dataList = dataList;
        this.height = height;
        this.width = width;
    }

    public void setDataList(List<BaseVideoItem> baseVideoItems, List<Data> dataList) {
        this.dataList = dataList;
        this.baseVideoItems = baseVideoItems;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        BaseVideoItem videoItem = baseVideoItems.get(viewType);
        View itemView = videoItem.createView(parent, height, width);
        return new ViewHolder(itemView);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        try {
            BaseVideoItem videoItem = baseVideoItems.get(position);
            videoItem.update(position, holder, mVideoPlayerManager);
            Data data = dataList.get(position);
            holder.ibFollow.setSelected(data.getFollowStatus() != 0);
            holder.ibLike.setSelected(data.isLiked());

            Business business = data.getBusiness();
            holder.rlAction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (business != null && business.getBusinessButtonText() != null) {
                        clickListner.onActionButtonClick(business.getBusinessButtonText(),
                                business.getBusinessUrl());
                    }
                }
            });

            //holder.mCover.setVisibility(data.hideCover() ? View.GONE : View.VISIBLE);
            holder.llMusic.setVisibility(data.getDub() != null ? View.VISIBLE : View.GONE);

            if (data.getDub() != null) holder.tvMusic.setText(data.getDub().getName());

            boolean isChannel = data.getChannelId() != null && !data.getChannelId().isEmpty();
            boolean isBusiness =
                    business != null && !business.getBusinessPostType().equalsIgnoreCase("regular");

//            if (AppController.getInstance().isGuest()) {
//                AppController.getInstance().openSignInDialog(context);
//            }

            //comment
            holder.ivComment.setOnClickListener(
                    view -> {
                        if (!AppController.getInstance().isGuest())
                            clickListner.comment(data.getPostId(), position, data.getCommentCount());
                        else
                            AppController.getInstance().openSignInDialog(context);
                    });

            //share
            holder.ivShare.setOnClickListener(view -> {
                if (!AppController.getInstance().isGuest())
                    clickListner.send(position);
                else
                    AppController.getInstance().openSignInDialog(context);
            });

            //profile
            holder.ivProfilePic.setOnClickListener(
                    view -> {
                        if (!AppController.getInstance().isGuest())
                            clickListner.profile(isChannel ? data.getChannelId() : data.getUserId(), isChannel, isBusiness);
                        else
                            AppController.getInstance().openSignInDialog(context);
                    });
            holder.tvUserName.setOnClickListener(
                    view -> {
                        if (!AppController.getInstance().isGuest())
                            clickListner.profile(isChannel ? data.getChannelId() : data.getUserId(),
                                    isChannel, isBusiness);
                        else
                            AppController.getInstance().openSignInDialog(context);
                    });

            //channel
            holder.tvChannel.setOnClickListener(
                    view -> {
                        if (!AppController.getInstance().isGuest())
                            clickListner.channel(data.getChannelId(), data.getChannelName());
                        else
                            AppController.getInstance().openSignInDialog(context);
                    });

            //category
            holder.tvCategory.setOnClickListener(
                    view -> {
                        if (!AppController.getInstance().isGuest())
                            clickListner.category(data.getCategoryId(), data.getCategoryName());
                        else
                            AppController.getInstance().openSignInDialog(context);
                    });

            //music
            holder.tvMusic.setOnClickListener(
                    view -> {
                        if (!AppController.getInstance().isGuest())
                            clickListner.music(data.getDub().getId(), data.getDub().getName(),
                                    data.getDub().getPath());
                        else
                            AppController.getInstance().openSignInDialog(context);
                    });

            holder.ibLike.setOnClickListener(v -> {
                holder.ibLike.setSelected(!holder.ibLike.isSelected());

                if (!AppController.getInstance().isGuest())
                    like(holder.tvLikeCount, position);
            });

            holder.ibFollow.setOnClickListener(v -> {

                if (data.getFollowStatus() != 0) {
                    holder.ibFollow.setSelected(false);
                } else {
                    holder.ibFollow.setSelected(true);
                }
                if (!AppController.getInstance().isGuest())
                    follow(position);
            });

            //share
            holder.ibShare.setOnClickListener(view -> {
                if (!AppController.getInstance().isGuest())
                    clickListner.share(data);
                else
                    AppController.getInstance().openSignInDialog(context);
            });

            //likers
            holder.tvLikeCount.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!AppController.getInstance().isGuest())
                        clickListner.openLikers(data);
                }
            });

            //viewer
            holder.tvViewCount.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!AppController.getInstance().isGuest())
                        clickListner.openViewers(data);
                }
            });
            holder.ivView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!AppController.getInstance().isGuest())
                        clickListner.openViewers(data);
                }
            });

            //menu
            holder.ibMenu.setOnClickListener(view -> {
                if (!AppController.getInstance().isGuest())
                    clickListner.openMenu(data, holder.ibMenu);
            });

            //location
            holder.tvLocation.setOnClickListener(view -> {
                if (!AppController.getInstance().isGuest())
                    clickListner.location(data);
            });

            //      heart animation
            holder.flMediaContainer.setOnTouchListener(new OnSwipeTouchListener(context) {
                public void onDoubleTaps() {

                    if (!AppController.getInstance().isGuest()) {
                        if (!data.isLiked()) {
                            like(holder.tvLikeCount, position);
                        }
                    }
                    animatePhotoLike(holder);
                }

                @Override
                public void onSingleTap() {
                }
            });
            holder.ibSaved.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!AppController.getInstance().isGuest()) {
                        if (data.getBookMarked()) {
                            holder.ibSaved.setImageDrawable(context.getDrawable(R.drawable.ic_unsaved));
                            holder.rL_save.setVisibility(View.GONE);
                        } else {
                            holder.ibSaved.setImageDrawable(context.getDrawable(R.drawable.ic_saved));
                            holder.rL_save.setVisibility(View.VISIBLE);
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    holder.rL_save.setVisibility(View.GONE);
                                }
                            }, 4000);
                        }
                        clickListner.savedClick(position, data.getBookMarked());
                    } else {
                        AppController.getInstance().openSignInDialog(context);
                    }
                }
            });

            holder.ibSaved.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if (!AppController.getInstance().isGuest()) {
                        if (data.getBookMarked()) {
                            holder.rL_save.setVisibility(View.VISIBLE);
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    holder.rL_save.setVisibility(View.GONE);
                                }
                            }, 4000);
                            clickListner.savedLongCick(position, data.getBookMarked());
                            return true;
                        } else {
                            return false;
                        }
                    } else {
                        AppController.getInstance().openSignInDialog(context);
                        return false;
                    }
                }
            });

            holder.tV_savedView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!AppController.getInstance().isGuest()) {
                        holder.rL_save.setVisibility(View.GONE);
                    }
                }
            });

            holder.tV_saveTo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!AppController.getInstance().isGuest()) {
                        clickListner.onSaveToCollectionClick(position, data);
                    }
                }
            });

            //            if (data.getTitle().length() > 20)
            //                makeTextViewResizable(holder.tvTitle, 3, "View More", true);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateTextureViewScaling(View view, int viewWidth, int viewHeight) {
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) view.getLayoutParams();
        params.width = viewWidth;
        params.height = viewHeight;
        view.setLayoutParams(params);
    }

    private void like(TextView view, int position) {
        try {
            Data data = dataList.get(position);
            boolean alreadyLiked = data.isLiked();
            int likes = Integer.parseInt(data.getLikesCount());

            if (alreadyLiked) {
                likes--;
            } else {
                likes++;
            }
            data.setLiked(!alreadyLiked);
            data.setLikesCount(String.valueOf(likes));
            dataList.set(position, data);
            view.setText(String.valueOf(likes));
            clickListner.like(!alreadyLiked, data.getPostId());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void follow(int position) {
        try {
            Data data = dataList.get(position);
            String userId = data.getUserId();
            int followingStatus;
            if (data.getFollowStatus() != 0) {
                followingStatus = 0;
                data.setFollowStatus(followingStatus);
                clickListner.follow(false, userId);
            } else {
                followingStatus = 1;
                data.setFollowStatus(followingStatus);
                clickListner.follow(true, userId);
            }

            dataList.set(position, data);

            updateFollowStatusForPosts(userId, data.getPostId(), followingStatus);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return baseVideoItems.size();
    }

    public void setClickListner(ClickListner clickListner) {
        this.clickListner = clickListner;
    }

    public void updateCommentCount(String count, String postId, int position,
                                   RecyclerView recyclerView) {
        try {
            if (dataList.get(position).getPostId().equals(postId)) {
                Data data = dataList.get(position);
                data.setCommentCount(count);
                dataList.set(position, data);

                try {
                    ((ViewHolder) recyclerView.findViewHolderForAdapterPosition(
                            position)).tvCommentCount.setText(count);
                } catch (Exception e) {
                    notifyItemChanged(position);
                }
            } else {
                for (int i = 0; i < dataList.size(); i++) {

                    if (dataList.get(i).getPostId().equals(postId)) {

                        Data data = dataList.get(position);
                        data.setCommentCount(count);
                        dataList.set(position, data);
                        try {
                            ((ViewHolder) recyclerView.findViewHolderForAdapterPosition(
                                    position)).tvCommentCount.setText(count);
                        } catch (Exception e) {
                            notifyItemChanged(position);
                        }

                        break;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void makeTextViewResizable(final TextView tv, final int maxLine,
                                             final String expandText, final boolean viewMore) {

        if (tv.getTag() == null) {
            tv.setTag(tv.getText());
        }
        ViewTreeObserver vto = tv.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {
                String text;
                int lineEndIndex;
                ViewTreeObserver obs = tv.getViewTreeObserver();
                obs.removeGlobalOnLayoutListener(this);
                if (maxLine == 0) {
                    lineEndIndex = tv.getLayout().getLineEnd(0);
                    text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1)
                            + " "
                            + expandText;
                } else if (maxLine > 0 && tv.getLineCount() >= maxLine) {
                    lineEndIndex = tv.getLayout().getLineEnd(maxLine - 1);
                    text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1)
                            + " "
                            + expandText;
                } else {
                    lineEndIndex = tv.getLayout().getLineEnd(tv.getLayout().getLineCount() - 1);
                    text = tv.getText().subSequence(0, lineEndIndex) + " " + expandText;
                }
                tv.setText(text);
                tv.setMovementMethod(LinkMovementMethod.getInstance());
                tv.setText(addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv,
                        lineEndIndex, expandText, viewMore), TextView.BufferType.SPANNABLE);
            }
        });
    }

    private static SpannableStringBuilder addClickablePartTextViewResizable(final Spanned strSpanned,
                                                                            final TextView tv, final int maxLine, final String spanableText, final boolean viewMore) {
        String str = strSpanned.toString();
        SpannableStringBuilder ssb = new SpannableStringBuilder(strSpanned);

        if (str.contains(spanableText)) {
            ssb.setSpan(new TextSpannable(false) {
                @Override
                public void onClick(View widget) {
                    tv.setLayoutParams(tv.getLayoutParams());
                    tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                    tv.invalidate();
                    if (viewMore) {
                        makeTextViewResizable(tv, -1, "View Less", false);
                    } else {
                        makeTextViewResizable(tv, 3, "View More", true);
                    }
                }
            }, str.indexOf(spanableText), str.indexOf(spanableText) + spanableText.length(), 0);
        }
        return ssb;
    }

    private void updateFollowStatusForPosts(String userId, String postId, int followingStaus) {

        Data data;
        for (int i = 0; i < dataList.size(); i++) {
            data = dataList.get(i);
            if (data.getUserId().equals(userId)) {

                if (!data.getPostId().equals(postId)) {
                    data.setFollowStatus(followingStaus);
                    dataList.set(i, data);
                }
            }
        }
    }

    // heart animation
    private void animatePhotoLike(ViewHolder holder) {
        holder.vBgLike.setVisibility(View.VISIBLE);
        holder.ivLikeIt.setVisibility(View.VISIBLE);

        holder.vBgLike.setScaleY(0.1f);
        holder.vBgLike.setScaleX(0.1f);
        holder.vBgLike.setAlpha(1f);
        holder.ivLikeIt.setScaleY(0.1f);
        holder.ivLikeIt.setScaleX(0.1f);

        AnimatorSet animatorSet = new AnimatorSet();

        ObjectAnimator bgScaleYAnim = ObjectAnimator.ofFloat(holder.vBgLike, "scaleY", 0.1f, 1f);
        bgScaleYAnim.setDuration(200);
        bgScaleYAnim.setInterpolator(DECCELERATE_INTERPOLATOR);
        ObjectAnimator bgScaleXAnim = ObjectAnimator.ofFloat(holder.vBgLike, "scaleX", 0.1f, 1f);
        bgScaleXAnim.setDuration(200);
        bgScaleXAnim.setInterpolator(DECCELERATE_INTERPOLATOR);
        ObjectAnimator bgAlphaAnim = ObjectAnimator.ofFloat(holder.vBgLike, "alpha", 1f, 0f);
        bgAlphaAnim.setDuration(200);
        bgAlphaAnim.setStartDelay(150);
        bgAlphaAnim.setInterpolator(DECCELERATE_INTERPOLATOR);

        ObjectAnimator imgScaleUpYAnim = ObjectAnimator.ofFloat(holder.ivLikeIt, "scaleY", 0.1f, 1f);
        imgScaleUpYAnim.setDuration(300);
        imgScaleUpYAnim.setInterpolator(DECCELERATE_INTERPOLATOR);
        ObjectAnimator imgScaleUpXAnim = ObjectAnimator.ofFloat(holder.ivLikeIt, "scaleX", 0.1f, 1f);
        imgScaleUpXAnim.setDuration(300);
        imgScaleUpXAnim.setInterpolator(DECCELERATE_INTERPOLATOR);

        ObjectAnimator imgScaleDownYAnim = ObjectAnimator.ofFloat(holder.ivLikeIt, "scaleY", 1f, 0f);
        imgScaleDownYAnim.setDuration(300);
        imgScaleDownYAnim.setInterpolator(ACCELERATE_INTERPOLATOR);
        ObjectAnimator imgScaleDownXAnim = ObjectAnimator.ofFloat(holder.ivLikeIt, "scaleX", 1f, 0f);
        imgScaleDownXAnim.setDuration(300);
        imgScaleDownXAnim.setInterpolator(ACCELERATE_INTERPOLATOR);

        animatorSet.playTogether(bgScaleYAnim, bgScaleXAnim, bgAlphaAnim, imgScaleUpYAnim,
                imgScaleUpXAnim);
        animatorSet.play(imgScaleDownYAnim).with(imgScaleDownXAnim).after(imgScaleUpYAnim);

        animatorSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                resetLikeAnimationState(holder);
            }
        });
        animatorSet.start();
        holder.ibLike.setSelected(true);
    }

    private void resetLikeAnimationState(ViewHolder holder) {
        holder.vBgLike.setVisibility(View.INVISIBLE);
        holder.ivLikeIt.setVisibility(View.INVISIBLE);
    }
}
