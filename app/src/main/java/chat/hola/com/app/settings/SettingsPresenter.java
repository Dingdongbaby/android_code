package chat.hola.com.app.settings;

import android.content.Intent;
import android.os.Handler;
import android.util.Log;

import com.lighthusky.dingdong.R;
import com.google.android.gms.appinvite.AppInviteInvitation;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import chat.hola.com.app.AppController;
import chat.hola.com.app.Database.CouchDbController;
import chat.hola.com.app.Networking.HowdooService;
import chat.hola.com.app.Utilities.Constants;
import chat.hola.com.app.Utilities.SessionApiCall;
import chat.hola.com.app.manager.session.SessionManager;
import chat.hola.com.app.models.LiveStream;
import chat.hola.com.app.models.Login;
import chat.hola.com.app.models.SessionObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * <h1>SettingsPresenter</h1>
 *
 * @author 3Embed
 * @version 1.0
 * @since 24/2/18.
 */

public class SettingsPresenter implements SettingsContract.Presenter {

    @Inject
    HowdooService service;
    @Inject
    SettingsContract.View mView;
    @Inject
    SessionManager sessionManager;
    private SessionApiCall sessionApiCall = new SessionApiCall();
    CouchDbController db;

    @Inject
    SettingsPresenter() {
        db = AppController.getInstance().getDbController();
    }

    @Override
    public void init() {
        mView.applyFont();
    }

    @Override
    public void logout() {
        mView.showLoader();
        service.logout(AppController.getInstance().getApiToken(), Constants.LANGUAGE)
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> response) {
                        switch (response.code()) {
                            case 200:
                                mView.logout();
                                break;
                            case 406:
                                SessionObserver sessionObserver = new SessionObserver();
                                sessionObserver.getObservable().subscribeOn(Schedulers.io())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(new DisposableObserver<Boolean>() {
                                            @Override
                                            public void onNext(Boolean flag) {
                                                Handler handler = new Handler();
                                                handler.postDelayed(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        logout();
                                                    }
                                                }, 1000);
                                            }

                                            @Override
                                            public void onError(Throwable e) {

                                            }

                                            @Override
                                            public void onComplete() {
                                            }
                                        });
                                sessionApiCall.getNewSession(service, sessionObserver);
                                break;
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.hideLoader();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void switchBusiness(boolean isBusiness, String businessCategoryId) {
        Map<String, Object> params = new HashMap<>();
        params.put("businessCategoryId", businessCategoryId);
        params.put("isActiveBusinessProfile", isBusiness);
        service.switchBusiness(AppController.getInstance().getApiToken(), Constants.LANGUAGE, params)
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<Login>>() {
                    @Override
                    public void onNext(Response<Login> response) {
                        switch (response.code()) {
                            case 200:
                                setData(response.body().getResponse(), isBusiness);
                                break;

                            case 406:
                                SessionObserver sessionObserver = new SessionObserver();
                                sessionObserver.getObservable().subscribeOn(Schedulers.io())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(new DisposableObserver<Boolean>() {
                                            @Override
                                            public void onNext(Boolean flag) {
                                                Handler handler = new Handler();
                                                handler.postDelayed(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        switchBusiness(isBusiness, businessCategoryId);
                                                    }
                                                }, 1000);
                                            }

                                            @Override
                                            public void onError(Throwable e) {

                                            }

                                            @Override
                                            public void onComplete() {
                                            }
                                        });
                                sessionApiCall.getNewSession(service, sessionObserver);
                                break;
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.i("error", "");
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data, int RESULT_OK) {
        if (requestCode == 111) {
            if (resultCode == RESULT_OK) {
                // Get the invitation IDs of all sent messages
                String[] ids = AppInviteInvitation.getInvitationIds(resultCode, data);
            } else {
                mView.showMessage(null, R.string.inviteFailedMsg);
            }
        }
    }

    private void setData(Login.LoginResponse response, boolean isBusiness) {
        try {
            Map<String, Object> map = new HashMap<>();
            map.put("userImageUrl", response.getProfilePic() == null ? "" : response.getProfilePic());
            map.put("userName", response.getUserName());
            map.put("firstName", response.getFirstName());
            map.put("lastName", response.getLastName());
            map.put("userId", response.getUserId());
            map.put("private", response.get_private());
            map.put("socialStatus", "");
            map.put("userIdentifier", response.getUserName());
            map.put("apiToken", response.getToken());
            sessionManager.setRefreshToken(response.getRefreshToken());

            AppController.getInstance().getSharedPreferences().edit().putString("token", response.getToken()).apply();
            map.put("userLoginType", 1);
            map.put("excludedFilterIds", new ArrayList<Integer>());
            if (!db.checkUserDocExists(AppController.getInstance().getIndexDocId(), response.getUserId())) {
                String userDocId = db.createUserInformationDocument(map);
                db.addToIndexDocument(AppController.getInstance().getIndexDocId(), response.getUserId(), userDocId);

            } else {
                db.updateUserDetails(db.getUserDocId(response.getUserId(), AppController.getInstance().getIndexDocId()), map);
            }

            db.updateIndexDocumentOnSignIn(AppController.getInstance().getIndexDocId(), response.getUserId(), 1, true);


            AppController.getInstance().setSignedIn(true, response.getUserId(), response.getUserName(), response.getPhoneNumber(), 1);
            AppController.getInstance().setSignStatusChanged(true);

            String topic = "/topics/" + response.getUserId();
            FirebaseMessaging.getInstance().subscribeToTopic(topic);

            sessionManager.setUserName(response.getUserName());
            sessionManager.setFirstName(response.getFirstName());
            sessionManager.setLastsName(response.getLastName());
//            sessionManager.setFacebookAccessToken(AccessToken.getCurrentAccessToken());
            sessionManager.setUserProfilePic(response.getProfilePic(), true);

            LiveStream stream = response.getStream();
            String countryCode = response.getCountryCode();
            String phone = response.getPhoneNumber();

            //  sessionManager.setFcmTopic(stream.getFcmTopic());
            sessionManager.setUserId(stream.getId());
            sessionManager.setEmail(response.getEmail());
            sessionManager.setMobileNumber(phone.replace(countryCode, ""));
            sessionManager.setFirstName(stream.getFirstName());
            sessionManager.setCountryCode(countryCode);

            mView.hideLoader();
            mView.gotoProfile(isBusiness);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
