package chat.hola.com.app.dagger;


import chat.hola.com.app.Activities.MainActivity;
import chat.hola.com.app.DublyCamera.CameraActivity;
import chat.hola.com.app.DublyCamera.live_stream.CameraStreamModule;
import chat.hola.com.app.NumberVerification.VerifyPhoneModule;
import chat.hola.com.app.NumberVerification.VerifyPhoneNumber;
import chat.hola.com.app.activities_user.UserActivitiesActivity;
import chat.hola.com.app.activities_user.UserActivitiesModule;
import chat.hola.com.app.activities_user.UserActivitiesUtilModule;
import chat.hola.com.app.authentication.forgotpassword.ForgotPasswordActivity;
import chat.hola.com.app.authentication.forgotpassword.ForgotPasswordModule;
import chat.hola.com.app.authentication.login.LoginActivity;
import chat.hola.com.app.authentication.login.LoginModule;
import chat.hola.com.app.authentication.newpassword.NewPasswordActivity;
import chat.hola.com.app.authentication.newpassword.NewPasswordModule;
import chat.hola.com.app.authentication.signup.SignUpActivity;
import chat.hola.com.app.authentication.signup.SignUpModule;
import chat.hola.com.app.authentication.signup.SignUpUtilModule;
import chat.hola.com.app.authentication.verifyEmail.VerifyEmailActivity;
import chat.hola.com.app.authentication.verifyEmail.VerifyEmailModule;
import chat.hola.com.app.blockUser.BlockUserActivity;
import chat.hola.com.app.blockUser.BlockUserModule;
import chat.hola.com.app.blockUser.BlockUserUtilModule;
import chat.hola.com.app.category.CategoryActivity;
import chat.hola.com.app.category.CategoryModule;
import chat.hola.com.app.category.CategoryUtilModule;
import chat.hola.com.app.comment.CommentActivity;
import chat.hola.com.app.comment.CommentModule;
import chat.hola.com.app.comment.CommentUtilModule;
import chat.hola.com.app.dubly.DubsActivity;
import chat.hola.com.app.dubly.DubsModule;
import chat.hola.com.app.dubly.DubsUtilModule;
import chat.hola.com.app.dublycategory.DubCategoryActivity;
import chat.hola.com.app.dublycategory.DubCategoryModule;
import chat.hola.com.app.dublycategory.DubCategoryUtilModule;
import chat.hola.com.app.friends.FriendsActivity;
import chat.hola.com.app.friends.FriendsModule;
import chat.hola.com.app.friends.FriendsUtilModule;
import chat.hola.com.app.home.LandingActivity;
import chat.hola.com.app.home.LandingModule;
import chat.hola.com.app.home.LandingUtilModule;
import chat.hola.com.app.home.activity.youTab.channelrequesters.ChannelRequestersActivity;
import chat.hola.com.app.home.activity.youTab.channelrequesters.ChannelRequestersModule;
import chat.hola.com.app.home.activity.youTab.channelrequesters.ChannelRequestersUtilModule;
import chat.hola.com.app.home.activity.youTab.followrequest.FollowRequestActivity;
import chat.hola.com.app.home.activity.youTab.followrequest.FollowRequestModule;
import chat.hola.com.app.home.activity.youTab.followrequest.FollowRequestUtilModule;
import chat.hola.com.app.location.LocSearchModule;
import chat.hola.com.app.location.LocSearchUtilModule;
import chat.hola.com.app.location.Location_Search_Activity;
import chat.hola.com.app.music.MusicActivity;
import chat.hola.com.app.music.MusicModule;
import chat.hola.com.app.music.MusicUtilModule;
import chat.hola.com.app.my_qr_code.MyQRCodeActivity;
import chat.hola.com.app.my_qr_code.MyQRCodeModule;
import chat.hola.com.app.post.PostActivity;
import chat.hola.com.app.post.PostModule;
import chat.hola.com.app.post.PostUtilModule;
import chat.hola.com.app.post.location.LocationActivity;
import chat.hola.com.app.post.location.LocationModule;
import chat.hola.com.app.post.location.LocationUtilModule;
import chat.hola.com.app.poststatus.PostStatusActivity;
import chat.hola.com.app.poststatus.PostStatusDaggerModule;
import chat.hola.com.app.preview.PreviewActivity;
import chat.hola.com.app.preview.PreviewModule;
import chat.hola.com.app.preview.PreviewUtilModule;
import chat.hola.com.app.profileScreen.ProfileActivity;
import chat.hola.com.app.profileScreen.ProfileModule;
import chat.hola.com.app.profileScreen.ProfileUtilModule;
import chat.hola.com.app.profileScreen.addChannel.AddChannelActivity;
import chat.hola.com.app.profileScreen.addChannel.AddChannelModule;
import chat.hola.com.app.profileScreen.addChannel.AddChannelUtilModule;
import chat.hola.com.app.profileScreen.discover.DiscoverActivity;
import chat.hola.com.app.profileScreen.discover.DiscoverModule;
import chat.hola.com.app.profileScreen.discover.DiscoverUtilModule;
import chat.hola.com.app.profileScreen.editProfile.BusinessBioActivity;
import chat.hola.com.app.profileScreen.editProfile.EditProfileActivity;
import chat.hola.com.app.profileScreen.editProfile.EditProfileModule;
import chat.hola.com.app.profileScreen.editProfile.EditProfileUtilModule;
import chat.hola.com.app.profileScreen.editProfile.changeEmail.ChangeEmail;
import chat.hola.com.app.profileScreen.editProfile.changeEmail.ChangeEmailModule;
import chat.hola.com.app.profileScreen.followers.FollowersActivity;
import chat.hola.com.app.profileScreen.followers.FollowersModule;
import chat.hola.com.app.profileScreen.followers.FollowersUtilModule;
import chat.hola.com.app.search.SearchActivity;
import chat.hola.com.app.search.SearchModule;
import chat.hola.com.app.search_user.SearchUserActivity;
import chat.hola.com.app.search_user.SearchUserModule;
import chat.hola.com.app.search_user.SearchUserUtilModule;
import chat.hola.com.app.settings.SettingsActivity;
import chat.hola.com.app.settings.SettingsModule;
import chat.hola.com.app.socialDetail.SocialDetailActivity;
import chat.hola.com.app.socialDetail.SocialDetailModule;
import chat.hola.com.app.socialDetail.SocialDetailUtilModule;
import chat.hola.com.app.trendingDetail.TrendingDetail;
import chat.hola.com.app.trendingDetail.TrendingDtlModule;
import chat.hola.com.app.trendingDetail.TrendingDtlUtilModule;
import chat.hola.com.app.user_verification.phone.VerifyNumberModule;
import chat.hola.com.app.user_verification.phone.VerifyNumberOTPActivity;
import chat.hola.com.app.webScreen.WebActivity;
import chat.hola.com.app.welcomeScreen.WelcomeActivity;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by ankit on 20/2/18.
 */

@Module
public interface ActivityBindingModule {

    @ActivityScoped
    @ContributesAndroidInjector()
    BusinessBioActivity businessBioActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = LoginModule.class)
    LoginActivity loginActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = ForgotPasswordModule.class)
    ForgotPasswordActivity forgotPasswordActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = VerifyEmailModule.class)
    VerifyEmailActivity verifyEmailActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = NewPasswordModule.class)
    NewPasswordActivity newPasswordActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {SignUpModule.class, SignUpUtilModule.class})
    SignUpActivity signUpActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {LocSearchModule.class, LocSearchUtilModule.class})
    Location_Search_Activity locationSearchActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {CategoryModule.class, CategoryUtilModule.class})
    CategoryActivity categoryActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {FollowersModule.class, FollowersUtilModule.class})
    FollowersActivity followerActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = ChangeEmailModule.class)
    ChangeEmail changeEmail();

    @ActivityScoped
    @ContributesAndroidInjector()
    WebActivity webActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {DiscoverModule.class, DiscoverUtilModule.class})
    DiscoverActivity discoverActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = SearchModule.class)
    SearchActivity searchActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {PostModule.class, PostUtilModule.class})
    PostActivity postActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {EditProfileModule.class, EditProfileUtilModule.class})
    EditProfileActivity editProfileActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {AddChannelModule.class, AddChannelUtilModule.class})
    AddChannelActivity addChannelActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {ProfileModule.class, ProfileUtilModule.class})
    ProfileActivity profileActivity();

    @ActivityScoped
    @ContributesAndroidInjector()
    WelcomeActivity welcomeActivity();

    @ActivityScoped
    @ContributesAndroidInjector()
    MainActivity mainActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {LandingModule.class, LandingUtilModule.class})
    LandingActivity contactSyncLandingPage();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {UserActivitiesModule.class, UserActivitiesUtilModule.class})
    UserActivitiesActivity userActivitiesActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {TrendingDtlModule.class, TrendingDtlUtilModule.class})
    TrendingDetail trendingDetail();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {SocialDetailModule.class, SocialDetailUtilModule.class})
    SocialDetailActivity socialDetailPresenterImpl();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {VerifyPhoneModule.class})
    VerifyPhoneNumber verifyNumber();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {SettingsModule.class})
    SettingsActivity settingsActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {CommentModule.class, CommentUtilModule.class})
    CommentActivity commentActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {SearchUserModule.class, SearchUserUtilModule.class})
    SearchUserActivity searchUserActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {FriendsModule.class, FriendsUtilModule.class})
    FriendsActivity friendsActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {MusicModule.class, MusicUtilModule.class})
    MusicActivity musicActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {BlockUserModule.class, BlockUserUtilModule.class})
    BlockUserActivity blockUserActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {DubsModule.class, DubsUtilModule.class})
    DubsActivity dubsActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {DubCategoryModule.class, DubCategoryUtilModule.class})
    DubCategoryActivity dubCategoryActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {PreviewModule.class, PreviewUtilModule.class})
    PreviewActivity previewActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {ChannelRequestersModule.class, ChannelRequestersUtilModule.class})
    ChannelRequestersActivity channelRequestersActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {FollowRequestModule.class, FollowRequestUtilModule.class})
    FollowRequestActivity followRequestActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = PostStatusDaggerModule.class)
    PostStatusActivity providePostStatus();

    @ActivityScoped
    @ContributesAndroidInjector(modules = MyQRCodeModule.class)
    MyQRCodeActivity proMyQrCode();


    @ActivityScoped
    @ContributesAndroidInjector(modules = CameraStreamModule.class)
    CameraActivity cameraActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {LocationModule.class, LocationUtilModule.class})
    LocationActivity locationActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = VerifyNumberModule.class)
    VerifyNumberOTPActivity verifyNumberOTPActivity();
}
