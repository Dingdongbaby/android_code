package chat.hola.com.app.DublyCamera;

import android.view.View;

import com.lighthusky.dingdong.R;

import androidx.recyclerview.widget.RecyclerView;

class ViewHolderProgressVideoItem extends RecyclerView.ViewHolder {
    View progressView;

    ViewHolderProgressVideoItem(View view) {
        super(view);

        progressView = (View) view.findViewById(R.id.progressView);
    }
}
