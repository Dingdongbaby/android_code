package chat.hola.com.app.cameraActivities.manager.listener;

import java.io.File;

import chat.hola.com.app.cameraActivities.utils.Size;

/**
 * Created by Arpit Gandhi on 8/14/16.
 */
public interface CameraVideoListener {
    void onVideoRecordStarted(Size videoSize);

    void onVideoRecordStopped(File videoFile);

    void onVideoRecordError();
}
