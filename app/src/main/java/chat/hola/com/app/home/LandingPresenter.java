package chat.hola.com.app.home;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;

import chat.hola.com.app.AppController;
import chat.hola.com.app.ImageCropper.CropImage;
import chat.hola.com.app.Networking.HowdooService;
import chat.hola.com.app.Utilities.ConnectivityReceiver;
import chat.hola.com.app.Utilities.Constants;
import chat.hola.com.app.Utilities.ImageFilePath;
import chat.hola.com.app.Utilities.SessionApiCall;
import chat.hola.com.app.Utilities.SocialShare;
import chat.hola.com.app.home.contact.Friend;
import chat.hola.com.app.home.contact.GetFriends;
import chat.hola.com.app.models.Error;
import chat.hola.com.app.models.SessionObserver;

import com.lighthusky.dingdong.R;

import chat.hola.com.app.models.WalletResponse;
import chat.hola.com.app.profileScreen.model.Data;
import chat.hola.com.app.profileScreen.model.Profile;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

/**
 * <h1>LandingPresenter</h1>
 *
 * <p></p>
 *
 * @author Shaktisinh
 * @version 1.0
 * @since 21/2/18.
 */
public class LandingPresenter
        implements LandingContract.Presenter, ConnectivityReceiver.ConnectivityReceiverListener {

    @Inject
    LandingContract.View view;
    @Inject
    SocialShare socialShare;
    @Inject
    Context context;
    @Inject
    HowdooService service;
    SessionApiCall sessionApiCall = new SessionApiCall();
    private String picturePath;
    private LandingActivity contactSyncLandingPage;

    @Inject
    public LandingPresenter(LandingActivity context) {
        contactSyncLandingPage = context;
    }

    @Override
    public void instagramShare(String type, String path) {
        if (socialShare.checkAppinstall(socialShare.INSTA_PACKAGE)) {
            socialShare.createInstagramIntent(type, null, path);
        } else {
            view.showMessage(null, R.string.installInstaMsg);
        }
    }

    @Override
    public LandingActivity getActivity() {
        return contactSyncLandingPage;
    }

    @Override
    public void parseMedia(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            try {
                Uri uri = data.getData();
                if (uri == null) return;

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    picturePath = ImageFilePath.getPathAboveN(context, uri);
                } else {

                    picturePath = ImageFilePath.getPath(context, uri);
                }

                if (picturePath != null) {
                    String extension = picturePath.substring(picturePath.lastIndexOf(".") + 1);
                    if (extension.equalsIgnoreCase("mp4")) {
                        File file = new File(picturePath);

                    } else {
                        //image
                        parseSelectedImage(uri, picturePath);
                    }
                } else {
                    //image can't be selected try another
                    view.showSnackMsg(R.string.string_31);
                }
            } catch (OutOfMemoryError e) {
                //out of mem try again
                view.showSnackMsg(R.string.string_15);
            }
        } else if (resultCode == Activity.RESULT_CANCELED) {
            //image selection canceled.
            view.showSnackMsg(R.string.string_16);
        } else {
            //failed to select image.
            view.showSnackMsg(R.string.string_113);
        }
    }

    @Override
    public void parseSelectedImage(Uri uri, String picturePath) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(picturePath, options);

        if (options.outWidth > 0 && options.outHeight > 0) {
            //launch crop image
            view.launchCropImage(uri);
        } else {
            //image can't be selected try another
            view.showSnackMsg(R.string.string_31);
        }
    }

    @Override
    public void parseCropedImage(int requestCode, int resultCode, Intent data) {
        try {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                //  picturePath = ImageFilePath.getPathAboveN(context, result.getUri());
                //} else {

                picturePath = ImageFilePath.getPath(context, result.getUri());
                //}

                if (picturePath != null) {
                    Bitmap bitmapToUpload = BitmapFactory.decodeFile(picturePath);
                    Bitmap bitmap = getCircleBitmap(bitmapToUpload);
                    if (bitmap != null && bitmap.getWidth() > 0 && bitmap.getHeight() > 0) {
                    } else {
                        picturePath = null;
                        view.showSnackMsg(R.string.string_19);
                    }
                } else {
                    picturePath = null;
                    view.showSnackMsg(R.string.string_19);
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                picturePath = null;
                view.showSnackMsg(R.string.string_19);
            }
        } catch (OutOfMemoryError e) {
            picturePath = null;
            view.showSnackMsg(R.string.string_15);
        }
    }

    private Bitmap getCircleBitmap(Bitmap bitmap) {
        try {
            final Bitmap circuleBitmap =
                    Bitmap.createBitmap(bitmap.getWidth(), bitmap.getWidth(), Bitmap.Config.ARGB_8888);
            final Canvas canvas = new Canvas(circuleBitmap);

            final int color = Color.GRAY;
            final Paint paint = new Paint();
            final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getWidth());
            final RectF rectF = new RectF(rect);

            paint.setAntiAlias(true);
            canvas.drawARGB(0, 0, 0, 0);
            paint.setColor(color);
            canvas.drawOval(rectF, paint);

            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
            canvas.drawBitmap(bitmap, rect, rect, paint);

            return circuleBitmap;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public void launchImagePicker() {
        Intent intent = null;
        intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        intent.setType("*/*");

        intent.putExtra(Intent.EXTRA_MIME_TYPES, new String[]{"image/*", "video/*"});
        view.launchImagePicker(intent);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        view.intenetStatusChanged(isConnected);
    }

    @Override
    public void friends() {
        if(AppController.getInstance().isGuest())return;
        service.getFollowersFollowee(AppController.getInstance().getApiToken(), Constants.LANGUAGE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<GetFriends>>() {
                    @Override
                    public void onNext(Response<GetFriends> response) {

                        switch (response.code()) {
                            case 200:
                                //                                model.clearList();

                                assert response.body() != null;
                                if (response.body().getData() != null) {

                                    //                                    model.setFriendList(response.body().getData());
                                    saveFriendsToDb(response.body().getData());
                                }

                                if (!AppController.getInstance().isFriendsFetched()) {
                                    AppController.getInstance().setFriendsFetched(true);
                                }

                                if (!AppController.getInstance().getChatSynced()) {

                                    try {
                                        JSONObject object = new JSONObject();

                                        object.put("eventName", "SyncChats");

                                        AppController.getBus().post(object);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }

                                break;
                            case 204:

                                //                                if (!AppController.getInstance().getChatSynced()) {

                                try {
                                    JSONObject object = new JSONObject();

                                    object.put("eventName", "SyncChats");

                                    AppController.getBus().post(object);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                //                                }

                                //                                if (!AppController.getInstance().isFriendsFetched()) {
                                AppController.getInstance().setFriendsFetched(true);
                                //                                }
                                break;
                            case 401:
                                if (view != null) view.sessionExpired();
                                break;
                            case 406:
                                SessionObserver sessionObserver = new SessionObserver();
                                sessionObserver.getObservable()
                                        .subscribeOn(Schedulers.io())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(new DisposableObserver<Boolean>() {
                                            @Override
                                            public void onNext(Boolean flag) {
                                                Handler handler = new Handler();
                                                handler.postDelayed(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        friends();
                                                    }
                                                }, 1000);
                                            }

                                            @Override
                                            public void onError(Throwable e) {

                                            }

                                            @Override
                                            public void onComplete() {
                                            }
                                        });
                                sessionApiCall.getNewSession(sessionObserver);
                                break;
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void getUserProfile() {
        if(AppController.getInstance().isGuest())return;
        service.getUserProfile(AppController.getInstance().getApiToken(), Constants.LANGUAGE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<Profile>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Response<Profile> response) {

                        switch (response.code()) {
                            case 200:
                                if (response.body() != null && view != null) {
                                    Data data = response.body().getData().get(0);
                                    view.profilepic(data.getProfilePic(), data.isStar(), data.getUserName(), data.isActiveBussinessProfile(), data.isBusinessProfileApproved());
                                }
                                break;
                            case 401:
                                if (view != null) view.sessionExpired();
                                break;
                            case 403:
                                break;
                            case 406:
                                SessionObserver sessionObserver = new SessionObserver();
                                sessionObserver.getObservable()
                                        .subscribeOn(Schedulers.io())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(new DisposableObserver<Boolean>() {
                                            @Override
                                            public void onNext(Boolean flag) {
                                                Handler handler = new Handler();
                                                handler.postDelayed(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        getUserProfile();
                                                    }
                                                }, 1000);
                                            }

                                            @Override
                                            public void onError(Throwable e) {

                                            }

                                            @Override
                                            public void onComplete() {
                                            }
                                        });
                                sessionApiCall.getNewSession(service, sessionObserver);
                                break;
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.showMessage(e.getMessage(), 0);
                        }
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    private void saveFriendsToDb(List<Friend> data) {

        try {

            ArrayList<Map<String, Object>> friends = new ArrayList<>();

            for (Friend f : data) {

                Map<String, Object> friend = new HashMap<>();

                friend.put("userId", f.getId());
                friend.put("userName", f.getUserName());
                friend.put("countryCode", f.getCountryCode());
                friend.put("number", f.getNumber());
                friend.put("profilePic", f.getProfilePic());
                friend.put("firstName", f.getFirstName());
                friend.put("lastName", f.getLastName());
                friend.put("socialStatus", f.getStatus());
                friend.put("isStar", f.isStar());
                friend.put("starRequest", f.getStarData());
                friend.put("private", f.getPrivate());
                friend.put("friendStatusCode", f.getFriendStatusCode());
                friend.put("followStatus", f.getFollowStatus());
                friend.put("timestamp", f.getTimestamp());
                friend.put("message", f.getMessage());

                //            Log.d("log1","friends data-->"+f.isStar());

                friends.add(friend);

                friend = null;
            }

            AppController.getInstance()
                    .getDbController()
                    .insertFriendsInfo(friends, AppController.getInstance().getFriendsDocId());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void kycVerification() {
        if(AppController.getInstance().isGuest())return;
        view.showLoader();
        service.kycVerification(AppController.getInstance().getApiToken(), Constants.LANGUAGE)
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<Error>>() {
                    @Override
                    public void onNext(Response<Error> response) {
                        view.hideLoader();
                        switch (response.code()) {
                            case 200:
                                //applied for kyc
                                view.gotoWalletDashboard(response.body().getVerificationStatus());
                                break;
                            case 404:
                                //not applied for kyc
                                view.gotoWalletDashboard(-1);
                                break;
                            case 406:
                                SessionObserver sessionObserver = new SessionObserver();
                                sessionObserver.getObservable().subscribeOn(Schedulers.io())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(new DisposableObserver<Boolean>() {
                                            @Override
                                            public void onNext(Boolean flag) {
                                                Handler handler = new Handler();
                                                handler.postDelayed(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        kycVerification();
                                                    }
                                                }, 1000);
                                            }

                                            @Override
                                            public void onError(Throwable e) {

                                            }

                                            @Override
                                            public void onComplete() {
                                            }
                                        });
                                sessionApiCall.getNewSession(service, sessionObserver);
                                break;
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.hideLoader();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void getWalletBalance() {
        if(AppController.getInstance().isGuest())return;
        service.walletBalance(AppController.getInstance().getApiToken(), Constants.LANGUAGE,
                AppController.getInstance().getUserId(), Constants.APP_TYPE)
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<WalletResponse>>() {
                    @Override
                    public void onNext(Response<WalletResponse> response) {
                        if (response.code() == 200) {
                            try {
                                assert response.body() != null;
                                view.showBalance(response.body().getData().getWalletData().get(0));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void getPendingCalls() {
        if(AppController.getInstance().isGuest())return;
    }
}
