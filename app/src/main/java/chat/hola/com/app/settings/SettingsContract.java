package chat.hola.com.app.settings;

import chat.hola.com.app.Utilities.BaseView;

/**
 * <h1>SettingsContract</h1>
 *
 * @author 3Embed
 * @version 1.0
 * @since 24/2/18.
 */

public interface SettingsContract {

    interface View extends BaseView {

        /**
         * <p>to apply fonts</p>
         */
        void applyFont();

        void logout();

        void gotoProfile(boolean isBusiness);

        void showLoader();

        void hideLoader();
    }

    interface Presenter {
        /**
         * <p>to initialize</p>
         */
        void init();

        void logout();

        void switchBusiness(boolean isBusiness, String businessCategoryId);
    }

}
