package chat.hola.com.app.Service;

import android.annotation.TargetApi;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.ComponentName;
import android.content.Context;
import android.os.PersistableBundle;

import chat.hola.com.app.AppController;

/**
 * Created by moda on 05/09/18.
 */
@TargetApi(21)
public class OreoJobService extends JobService {

    private JobParameters mParams;

    //Assuming it takes maximum 5 seconds for
    private static long mRetryInterval = 2500;


    public OreoJobService() {
    }

    public boolean onStartJob(JobParameters params) {


        this.mParams = params;

        String userId = AppController.getInstance().getUserId();

        if (userId == null) return false;


        String command = params.getExtras().getString("command");
        return false;
    }


    public void scheduleReconnect() {
        //To automatically retry after mRetryInterval

        if (mRetryInterval < 60000L) {
            mRetryInterval = Math.min(mRetryInterval * 2L, 60000L);
        }


        this.scheduleJob(mRetryInterval);
    }


    void endJob() {
        this.jobFinished(this.mParams, false);
    }


    private void connect() {


        this.scheduleReconnect();

    }


    void scheduleJob(long interval) {


        ComponentName serviceName = new ComponentName(this.getPackageName(), OreoJobService.class.getName());


        PersistableBundle extras = new PersistableBundle();
        extras.putString("command", "start");
        extras.putInt("fromJobScheduler", 1);
        try {
            JobScheduler jobScheduler = (JobScheduler) this.getSystemService(Context.JOB_SCHEDULER_SERVICE);
            this.endJob();
        } catch (NullPointerException ignored) {
        }
    }

    public boolean onStopJob(JobParameters params) {
        return false;
    }


}
