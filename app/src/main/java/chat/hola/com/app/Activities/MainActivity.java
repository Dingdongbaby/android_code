package chat.hola.com.app.Activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;

import com.lighthusky.dingdong.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;

import javax.inject.Inject;

import chat.hola.com.app.AppController;
import chat.hola.com.app.Networking.HowdooService;
import chat.hola.com.app.Networking.observer.SocialShareObserver;
import chat.hola.com.app.Utilities.SocialShare;
import chat.hola.com.app.home.LandingActivity;
import chat.hola.com.app.models.SocialObserver;
import chat.hola.com.app.socialDetail.SocialDetailActivity;
import chat.hola.com.app.welcomeScreen.StartActivity;
import chat.hola.com.app.welcomeScreen.WelcomeActivity;
import dagger.android.DaggerActivity;
import dagger.android.support.DaggerAppCompatActivity;


public class MainActivity extends DaggerAppCompatActivity {

    @Inject
    HowdooService howdooService;
    @Inject
    SocialObserver socialShareObserver;
    private String postId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Request republish of posts
        AppController.getInstance().createNewPostListener(howdooService, socialShareObserver);

        FirebaseDynamicLinks.getInstance()
                .getDynamicLink(getIntent())
                .addOnSuccessListener(this, new OnSuccessListener<PendingDynamicLinkData>() {
                    @Override
                    public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
                        // Get deep link from result (may be null if no link is found)
                        Uri deepLink = null;
                        if (pendingDynamicLinkData != null) {
                            deepLink = pendingDynamicLinkData.getLink();
                            postId = deepLink.getLastPathSegment();
                        }
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                    }
                });

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (AppController.getInstance().getSignedIn() && AppController.getInstance().profileSaved()) {
                    Intent i2;
                    if (postId != null && !postId.isEmpty()) {
                        i2 = new Intent(MainActivity.this, SocialDetailActivity.class);
                        i2.putExtra("postId", postId);
                    } else {
                        i2 = new Intent(MainActivity.this, LandingActivity.class);
                        i2.putExtra("userId", AppController.getInstance().getUserId());
                    }

                    i2.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    startActivity(i2);
                    finish();
                    overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                } else {
                    if(AppController.getInstance().isGuest()){
                        Intent i = new Intent(MainActivity.this, LandingActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(i);
                        supportFinishAfterTransition();
                    }else {
                        Intent intent2 = new Intent(MainActivity.this, StartActivity.class);
                        intent2.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent2);
                        finish();
                        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                    }
                }
            }
        }, 1500);
    }

    public void forceCrash() {
        throw new RuntimeException("This is a crash");
    }


    @Override
    protected void onResume() {
        super.onResume();
    }
}
