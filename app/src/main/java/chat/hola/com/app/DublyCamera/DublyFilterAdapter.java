package chat.hola.com.app.DublyCamera;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lighthusky.dingdong.R;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import chat.hola.com.app.AppController;
import chat.hola.com.app.Utilities.TextDrawable;
import chat.hola.com.app.Utilities.Utilities;

public class DublyFilterAdapter extends RecyclerView.Adapter<ViewHolderDublyFilters> {


    private ArrayList<DublyFilterModel> mListData;
    private int density;
    private Context mContext;

    public DublyFilterAdapter(Context activity, ArrayList<DublyFilterModel> mListData) {

        this.mContext = activity;
        this.mListData = mListData;

        density = (int) activity.getResources().getDisplayMetrics().density;
    }

    @Override
    public int getItemCount() {
        return mListData.size();
    }

    @NonNull
    @Override
    public ViewHolderDublyFilters onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.video_filter_item, parent, false);

        return new ViewHolderDublyFilters(itemView);
    }

    @SuppressWarnings("TryWithIdenticalCatches")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolderDublyFilters vh, int position) {


        final DublyFilterModel callItem = mListData.get(position);


        if (callItem != null) {


            vh.filterName.setText(callItem.getFilterName());
            if (callItem.isSelected()) {
                vh.filterName.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));

                vh.selectedSign.setVisibility(View.VISIBLE);

            } else {
                vh.filterName.setTextColor(Color.BLACK);

                vh.selectedSign.setVisibility(View.GONE);
            }

            vh.filterImage.setImageResource(Utilities.getDrawableByString(mContext, "effect_" + callItem.getFilterName().toLowerCase()));

//            try {
//                vh.filterImage.setImageDrawable(TextDrawable.builder()
//
//
//                        .beginConfig()
//                        .textColor(Color.WHITE)
//                        .useFont(Typeface.DEFAULT)
//                        .fontSize(24 * density) /* size in px */
//                        .bold()
//                        .toUpperCase()
//                        .endConfig()
//
//
//                        .buildRound((callItem.getFilterName().trim()).charAt(0) + "", Color.parseColor(AppController.getInstance().getColorCode(vh.getAdapterPosition() % 19))));
//
//            } catch (Exception e) {
//            }
        }
    }
}