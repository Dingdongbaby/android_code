package chat.hola.com.app.authentication.login;

import chat.hola.com.app.models.Login;

public interface LoginContract {

    interface View {
        void message(String message);

        void loginSuccessfully();

        void numberNotRegistered(String error);

        void blocked(String errorMessage);

        void verifyOtp(String phoneNumber, String countryCode);

        void showLoader();

        void hideLoader();

        void forgotPassword();
    }

    interface Presenter {
        void loginByUserName(String userName, String password, String appVersion, String country);

        void loginByPassword(String countryCode, String phoneNumber, String password, String appVersion, String country);

        void loginByOtp(Login login);

        void requestOtp(String phoneNumber, String countryCode);

        void verifyIsMobileRegistered(String phone, String countryCode, boolean isForgotPassword);

    }
}
