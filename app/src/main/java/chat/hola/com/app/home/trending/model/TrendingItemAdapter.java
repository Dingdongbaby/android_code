package chat.hola.com.app.home.trending.model;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.DrawableRequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.lighthusky.dingdong.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import chat.hola.com.app.Utilities.CommonClass;
import chat.hola.com.app.Utilities.Utilities;
import chat.hola.com.app.home.model.Data;

public class TrendingItemAdapter extends RecyclerView.Adapter<TrendingItemAdapter.ViewHolder> {

    private Context context;
    private List<Data> dataList;
    private long totalPosts;
    private String hashtag;
    private ClickListner listner;

    public TrendingItemAdapter(Context context, List<Data> posts, long totalPosts, String hashtag, ClickListner listner) {
        this.context = context;
        this.dataList = posts;
        this.totalPosts = totalPosts;
        this.listner = listner;
        this.hashtag = hashtag;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.trending_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Data data = dataList.get(position);
        if (data.getMediaType1() == 1) {
            //video
            DrawableRequestBuilder<String> thumbnail = Glide.with(context).load(Utilities.getModifiedThumbnailLink(data.getImageUrl1())).diskCacheStrategy(DiskCacheStrategy.SOURCE);

            Glide.with(context).load(Utilities.getModifiedImageLink(data.getImageUrl1()))
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .dontAnimate()
                    .fitCenter()
                    .placeholder(context.getResources().getDrawable(R.drawable.ic_default))
                    .thumbnail(thumbnail)
                    .into(holder.imageView);
        } else {
            //image

            DrawableRequestBuilder<String> thumbnail = Glide.with(context).load(Utilities.getModifiedThumbnailLink(data.getImageUrl1())).diskCacheStrategy(DiskCacheStrategy.SOURCE);


            Glide.with(context).load(Utilities.getModifiedImageLink(data.getImageUrl1()))
                .thumbnail(thumbnail)
                    .dontAnimate()
                    .placeholder(context.getResources().getDrawable(R.drawable.ic_default))
                    .into(holder.imageView);
        }


        holder.imageButton.setVisibility(data.getMediaType1() == 0 ? View.GONE : View.VISIBLE);
        holder.overlay.setVisibility(position == dataList.size() - 1 && totalPosts > 8 ? View.VISIBLE : View.GONE);
        holder.tvViewAll.setVisibility(position == dataList.size() - 1 && totalPosts > 8 ? View.VISIBLE : View.GONE);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listner.postClick(dataList, position);
            }
        });

        if (position == dataList.size() - 1 && totalPosts > 8)
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listner.viewAll(hashtag, String.valueOf(totalPosts));
                }
            });
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.ivPostImage)
        ImageView imageView;
        @BindView(R.id.ibCam)
        ImageView imageButton;
        @BindView(R.id.overlay)
        View overlay;
        @BindView(R.id.tvViewAll)
        TextView tvViewAll;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.getLayoutParams().width = CommonClass.getDeviceWidth(context)/3;
        }
    }

    public interface ClickListner {
        void viewAll(String hashtag, String totalPosts);

        void postClick(List<Data> dataList, int position);
    }
}
