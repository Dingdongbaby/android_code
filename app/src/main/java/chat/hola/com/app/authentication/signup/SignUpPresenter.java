package chat.hola.com.app.authentication.signup;

import android.util.Log;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import chat.hola.com.app.Networking.HowdooService;
import chat.hola.com.app.Utilities.Constants;
import chat.hola.com.app.Utilities.Error;
import chat.hola.com.app.Utilities.SessionApiCall;
import chat.hola.com.app.Utilities.UploadFileAmazonS3;
import chat.hola.com.app.Utilities.Utilities;
import chat.hola.com.app.models.Login;
import chat.hola.com.app.models.Register;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

public class SignUpPresenter implements SignUpContract.Presenter {

    private SessionApiCall sessionApiCall = new SessionApiCall();

    @Inject
    HowdooService service;
    @Inject
    SignUpContract.View view;
    @Inject
    SignUpModel model;

    @Inject
    SignUpPresenter() {
    }

    @Override
    public void register(Register register, UploadFileAmazonS3 amazonS3, File mFileTemp) {
        view.enableSave(false);
        view.showLoader();
        service.signUp(Utilities.getThings(),register)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<Login>>() {
                    @Override
                    public void onNext(Response<Login> response) {

                        switch (response.code()) {
                            case 201:
                                if (mFileTemp != null) {
                                    amazonUpload(amazonS3, mFileTemp, register.getMobileNumber(), register.getCountryCode(), response.body().getResponse().getUserId(), register.getUserName(), response);
                                } else {
                                    model.setData(response.body().getResponse());
                                    view.registered(response.body().getResponse());
                                }
                                break;
                            case 409:
                            case 400:
                            case 422:
                                view.message(Error.getErrorMessage(response.errorBody()));
                                view.completed();
                                view.enableSave(true);
                                view.hideLoader();
                                break;
                            case 406:
                                view.hideLoader();
                                break;
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.hideLoader();
                            ;
                            view.completed();
                            view.enableSave(false);
                        }
                    }

                    @Override
                    public void onComplete() {
                        if (view != null)
                            view.hideLoader();
                        ;
                        view.completed();
                    }
                });
    }

    @Override
    public void amazonUpload(UploadFileAmazonS3 amazonS3, File mFileTemp, String phone, String countryCode, String userId, String userName, Response<Login> response) {
        final String imageUrl = Constants.AmazonS3.BASE_URL + "/" + Constants.AmazonS3.BUCKET + "/" + Constants.AmazonS3.PROFILE_PIC_FOLDER + "/" + userId;
        Log.d("amazon", "amzonUpload: " + imageUrl);

        amazonS3.Upload_data(Constants.AmazonS3.BUCKET, Constants.AmazonS3.PROFILE_PIC_FOLDER + "/" + userId, mFileTemp, new UploadFileAmazonS3.UploadCallBack() {
            @Override
            public void sucess(String success) {
                view.setProfilePic(imageUrl);
                model.setData(response.body().getResponse());
                view.registered(response.body().getResponse());
                //AppController.getInstance().setSignedIn(false, userId, userName, phone, 1);
                //AppController.getInstance().setSignStatusChanged(false);
                //view.registered();
            }

            @Override
            public void error(String errormsg) {
                view.hideLoader();
                ;
                view.message("Profile pic is not updated");
                view.completed();
                view.enableSave(true);
            }
        });
    }

    @Override
    public void validate(Register register) {
        view.showLoader();
        Map<String, Object> map = new HashMap<>();
        map.put("userName", register.getUserName());
        map.put("email", register.getEmail());
        map.put("userReferralCode", register.getReferralCode());
        map.put("number", register.getMobileNumber());
        map.put("countryCode", register.getCountryCode());
        service.verifySignUp(Utilities.getThings(),map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> response) {
                        view.hideLoader();
                        switch (response.code()) {
                            case 200:
                                view.verifyPhone();
                                break;
                            case 406:
                                view.message("Invalid referral code");
                                break;
                            default:
                                try {
                                    assert response.errorBody() != null;
                                    view.message(Error.getErrorMessage(response.errorBody()));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                break;
                        }
                        view.enableSave(true);
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.enableSave(true);
                            view.hideLoader();
                        }
                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }

    @Override
    public void signUp(Register register, UploadFileAmazonS3 uploadFileAmazonS3, String profilePic) {
        register(register, uploadFileAmazonS3, profilePic != null ? new File(profilePic) : null);
    }

}
