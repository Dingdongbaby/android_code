package chat.hola.com.app.authentication.signup;

import java.io.File;

import chat.hola.com.app.Utilities.UploadFileAmazonS3;
import chat.hola.com.app.models.Login;
import chat.hola.com.app.models.Register;
import retrofit2.Response;

public interface SignUpContract {

    interface View {
        void registered(Login.LoginResponse response);

        void message(String message);

        void completed();

        void setProfilePic(String url);

        void enableSave(boolean enable);

        void showProgress(boolean show);

        void verifyPhone();

        void showLoader();

        void hideLoader();
    }

    interface Presenter {
        void register(Register register, UploadFileAmazonS3 amazonS3, File mFileTemp);

        void amazonUpload(UploadFileAmazonS3 amazonS3, File mFileTemp, String phone, String countryCode, String userId, String userName, Response<Login> response);

        void validate(Register register);

        void signUp(Register register, UploadFileAmazonS3 uploadFileAmazonS3, String profilePic);
    }
}
