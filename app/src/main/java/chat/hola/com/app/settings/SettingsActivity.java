package chat.hola.com.app.settings;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.lighthusky.dingdong.R;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import chat.hola.com.app.Activities.MainActivity;
import chat.hola.com.app.AppController;
import chat.hola.com.app.Database.PostDb;
import chat.hola.com.app.Dialog.BlockDialog;
import chat.hola.com.app.Utilities.Loader;
import chat.hola.com.app.Utilities.TypefaceManager;
import chat.hola.com.app.authentication.newpassword.NewPasswordActivity;
import chat.hola.com.app.blockUser.BlockUserActivity;
import chat.hola.com.app.manager.session.SessionManager;
import chat.hola.com.app.profileScreen.discover.DiscoverActivity;
import chat.hola.com.app.webScreen.WebActivity;
import dagger.android.support.DaggerAppCompatActivity;

/**
 * <h1>SettingsActivity</h1>
 *
 * @author 3Embed
 * @version 1.0
 * @since 24/2/18.
 */
public class SettingsActivity extends DaggerAppCompatActivity implements SettingsContract.View {

    private Unbinder unbinder;
    private String version = "";

    @Inject
    SessionManager sessionManager;
    @Inject
    SettingsPresenter presenter;
    @Inject
    TypefaceManager typefaceManager;

    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.tvChangePassword)
    TextView tvChangePassword;
    @BindView(R.id.tvFindContacts)
    TextView tvFindContacts;
    @BindView(R.id.tvTitleSettings)
    TextView tvTitleSettings;
    @BindView(R.id.tvDataUses)
    TextView tvDataUses;
    @BindView(R.id.tvBlockedUsers)
    TextView tvBlockedUsers;
    @BindView(R.id.tvTitleSupport)
    TextView tvTitleSupport;
    @BindView(R.id.tvReportAProb)
    TextView tvReportAProb;
    @BindView(R.id.tvTitleAbout)
    TextView tvTitleAbout;
    @BindView(R.id.tvAbout)
    TextView tvAbout;
    @BindView(R.id.tvPrivacy)
    TextView tvPrivacy;
    @BindView(R.id.tvTermsOfService)
    TextView tvTermsOfService;
    @BindView(R.id.tvVersion)
    TextView tvVersion;
    @BindView(R.id.tvLogout)
    TextView tvLogout;
    @BindView(R.id.root)
    RelativeLayout rlRoot;
    @Inject
    BlockDialog dialog;
    @Inject
    PostDb postDb;
    private Loader loader;

    @Override
    public void userBlocked() {
        dialog.show();
    }


    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_new);
        unbinder = ButterKnife.bind(this);
        //Thread.setDefaultUncaughtExceptionHandler(new MyExceptionHandler(this));

        loader = new Loader(this);
        presenter.init();

        try {
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionName;
            String v = "v" + version;
            tvVersion.setText(v);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void showMessage(String msg, int msgId) {

    }

    @Override
    public void sessionExpired() {
        sessionManager.sessionExpired(getApplicationContext());
    }

    @Override
    public void isInternetAvailable(boolean flag) {

    }


    @OnClick(R.id.tvLogout)
    public void callLogout() {
        presenter.logout();
    }

    @OnClick(R.id.tvChangePassword)
    public void changePassword() {
        startActivity(new Intent(this, NewPasswordActivity.class).putExtra("call", "setting"));
    }

    @Override
    public void applyFont() {
        tvTitle.setTypeface(typefaceManager.getSemiboldFont());
        tvFindContacts.setTypeface(typefaceManager.getRegularFont());
        tvTitleSettings.setTypeface(typefaceManager.getMediumFont());
        tvDataUses.setTypeface(typefaceManager.getRegularFont());
        tvBlockedUsers.setTypeface(typefaceManager.getRegularFont());
        tvChangePassword.setTypeface(typefaceManager.getRegularFont());
        tvTitleSupport.setTypeface(typefaceManager.getMediumFont());
        tvReportAProb.setTypeface(typefaceManager.getRegularFont());
        tvTitleAbout.setTypeface(typefaceManager.getMediumFont());
        tvAbout.setTypeface(typefaceManager.getRegularFont());
        tvPrivacy.setTypeface(typefaceManager.getRegularFont());
        tvTermsOfService.setTypeface(typefaceManager.getRegularFont());
        tvLogout.setTypeface(typefaceManager.getMediumFont());
    }

    @Override
    public void logout() {
        postDb.delete();
        sessionManager.logOut(this);
        finish();
    }

    @Override
    public void gotoProfile(boolean isBusiness) {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finishAffinity();
    }

    @Override
    public void showLoader() {
        loader.show();
    }

    @Override
    public void hideLoader() {
        if (loader != null && loader.isShowing()) loader.dismiss();
    }

    @OnClick(R.id.tvReportAProb)
    public void report() {

        String stringBuilder = "\n\n\n\n\n\n--------------------------------------------------\n\n"
                + "Device Id: "
                + AppController.getInstance().getDeviceId()
                + "\n"
                + "Device Name: "
                + Build.DEVICE
                + "\n"
                + "Device OS: "
                + Build.VERSION.RELEASE
                + "\n"
                + "Model Number: "
                + Build.MODEL
                + "\n"
                + "Device Type: "
                + "Android"
                + "\n"
                + "App Version: "
                + version;

        Intent i = new Intent(Intent.ACTION_SENDTO);
        i.setType("message/rfc822");
        i.setData(Uri.parse("mailto:appscripdev@gmail.com"));
        i.putExtra(Intent.EXTRA_EMAIL, new String[]{"appscripdev@gmail.com"});
        i.putExtra(Intent.EXTRA_SUBJECT, "Report a problem");
        i.putExtra(Intent.EXTRA_TEXT, stringBuilder);
        try {
            startActivity(Intent.createChooser(i, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        presenter.onActivityResult(requestCode, requestCode, data, RESULT_OK);
        if (resultCode == 555) {
            onBackPressed();
        }
    }


    /**
     * <p>redirects to DiscoverActivity's contact tab to follow facebook friend</p>
     */
    @OnClick(R.id.tvFindContacts)
    public void followContact() {
        Intent intent = new Intent(SettingsActivity.this, DiscoverActivity.class);
        intent.putExtra("caller", "SettingsActivity");
        intent.putExtra("is_contact", true);
        startActivity(intent);
    }

    /**
     * <p>redirects to DataUsage's activity</p>
     */
    @OnClick(R.id.tvDataUses)
    public void dataUses() {
    }

    /**
     * <p>redirects to BlockedUser's activity</p>
     */
    @OnClick(R.id.tvBlockedUsers)
    public void blockedUsers() {
        Intent intent = new Intent(SettingsActivity.this, BlockUserActivity.class);
        startActivity(intent);
    }

    /**
     * <p>redirects to WebActivity's activity and opens privacy policy link</p>
     */
    @OnClick(R.id.tvPrivacy)
    public void privacyPolicy() {
        Intent intent = new Intent(SettingsActivity.this, WebActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("url", getResources().getString(R.string.privacyPolicyUrl));
        bundle.putString("title", getResources().getString(R.string.privacyPolicy));
        intent.putExtra("url_data", bundle);
        intent.putExtra("clear", true);
        startActivity(intent);
    }

    /**
     * <p>redirects to WebActivity's activity and opens terms of service link</p>
     */
    @OnClick(R.id.tvTermsOfService)
    public void terms() {
        Intent intent = new Intent(SettingsActivity.this, WebActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("url", getResources().getString(R.string.termsUrl));
        bundle.putString("title", getResources().getString(R.string.termsOfServiceTitle));
        intent.putExtra("url_data", bundle);
        intent.putExtra("clear", false);
        startActivity(intent);
    }

    /**
     * <p>redirects to WebActivity's activity and opens about us link</p>
     */
    @OnClick(R.id.tvAbout)
    public void about() {
        Intent intent = new Intent(SettingsActivity.this, WebActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("url", getResources().getString(R.string.aboutUsUrl));
        bundle.putString("title", getResources().getString(R.string.about));
        intent.putExtra("url_data", bundle);
        startActivity(intent);
    }

    @OnClick(R.id.ivBack)
    public void ivBack() {
        onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (unbinder != null) unbinder.unbind();
    }

    @Override
    public void reload() {

    }
}
