package chat.hola.com.app.blockUser.model;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;
import androidx.recyclerview.widget.RecyclerView;
import chat.hola.com.app.AppController;
import chat.hola.com.app.Utilities.TypefaceManager;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.signature.StringSignature;
import com.lighthusky.dingdong.R;
import java.util.List;
import javax.inject.Inject;

/**
 * <h1>BlockUserAdapter</h1>
 *
 * @author 3embed
 * @version 1.0
 * @since 4/9/2018
 */

public class BlockUserAdapter extends RecyclerView.Adapter<ViewHolder> {
  private TypefaceManager typefaceManager;
  private ClickListner clickListner;
  private List<User> comments;
  private Context context;

  @Inject
  public BlockUserAdapter(List<User> comments, Activity mContext, TypefaceManager typefaceManager) {
    this.typefaceManager = typefaceManager;
    this.comments = comments;
    this.context = mContext;
  }

  @Override
  public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View itemView =
        LayoutInflater.from(parent.getContext()).inflate(R.layout.block_item, parent, false);
    return new ViewHolder(itemView, typefaceManager, clickListner);
  }

  @Override
  public void onBindViewHolder(final ViewHolder holder,
      @SuppressLint("RecyclerView") final int position) {
    if (getItemCount() > 0) {
      final User comment = comments.get(position);
      holder.tvUserName.setText(comment.getUserName());

      Glide.with(context).load(comment.getProfilePic()).asBitmap().centerCrop()
          //.signature(new StringSignature(String.valueOf(System.currentTimeMillis())))
          .signature(new StringSignature(
              AppController.getInstance().getSessionManager().getUserProfilePicUpdateTime()))
          //.diskCacheStrategy(DiskCacheStrategy.NONE)
          //.skipMemoryCache(true)
          .into(new BitmapImageViewTarget(holder.ivProfilePic) {
            @Override
            protected void setResource(Bitmap resource) {
              RoundedBitmapDrawable circularBitmapDrawable =
                  RoundedBitmapDrawableFactory.create(context.getResources(), resource);
              circularBitmapDrawable.setCircular(true);
              holder.ivProfilePic.setImageDrawable(circularBitmapDrawable);
            }
          });
    }
  }

  @Override
  public int getItemCount() {
    return comments.size();
  }

  public void setListener(ClickListner clickListner) {
    this.clickListner = clickListner;
  }
}