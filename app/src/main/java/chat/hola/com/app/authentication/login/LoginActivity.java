package chat.hola.com.app.authentication.login;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.InputFilter;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.lighthusky.dingdong.R;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import chat.hola.com.app.NumberVerification.ChooseCountry;
import chat.hola.com.app.Utilities.Loader;
import chat.hola.com.app.Utilities.TypefaceManager;
import chat.hola.com.app.authentication.forgotpassword.ForgotPasswordActivity;
import chat.hola.com.app.models.Login;
import chat.hola.com.app.profileScreen.discover.DiscoverActivity;
import chat.hola.com.app.user_verification.phone.VerifyNumberOTPActivity;
import chat.hola.com.app.webScreen.WebActivity;
import dagger.android.support.DaggerAppCompatActivity;

import static chat.hola.com.app.Utilities.CountryCodeUtil.readEncodedJsonString;

public class LoginActivity extends DaggerAppCompatActivity implements LoginContract.View {

    private static final int FACEBOOK_APP_REQUEST_CODE = 6135;

    @Inject
    TypefaceManager typefaceManager;
    @Inject
    LoginContract.Presenter presenter;

    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.textAccount)
    TextView textAccount;
    @BindView(R.id.textRegion)
    TextView textRegion;
    @BindView(R.id.tvRegion)
    TextView tvRegion;
    @BindView(R.id.textPhone)
    TextView textPhone;
    @BindView(R.id.textPassword)
    TextView textPassword;
    @BindView(R.id.textOtp)
    TextView textOtp;
    @BindView(R.id.loginSwitch)
    TextView loginSwitch;
    @BindView(R.id.tvNotMember)
    TextView tvNotMember;
    @BindView(R.id.tvSignUp)
    TextView tvSignUp;
    @BindView(R.id.tvSend)
    TextView tvSend;
    @BindView(R.id.loginByStarChatId)
    TextView loginByStarChatId;
    @BindView(R.id.done)
    Button done;

    @BindView(R.id.etAccount)
    EditText etAccount;
    @BindView(R.id.etPhone)
    EditText etPhone;
    @BindView(R.id.etPassword)
    EditText etPassword;
    @BindView(R.id.etOtp)
    EditText etOtp;

    @BindView(R.id.llUserName)
    LinearLayout llUserName;
    @BindView(R.id.llRegion)
    LinearLayout llRegion;
    @BindView(R.id.llPhone)
    LinearLayout llPhone;
    @BindView(R.id.llPassword)
    LinearLayout llPassword;
    @BindView(R.id.llotp)
    LinearLayout llotp;

    @BindView(R.id.otpDivider)
    View otpDivider;
    @BindView(R.id.passwordDivider)
    View passwordDivider;
    @BindView(R.id.phoneDivider)
    View phoneDivider;
    @BindView(R.id.userNameDivider)
    View userNameDivider;
    @BindView(R.id.btnForgotPassword)
    Button btnForgotPassword;
    @BindView(R.id.tbPassword)
    ToggleButton tbPassword;

    private int login_mode;
    private String countryCode;
    private String country;
    private Login login;
    private int max_digits = 20;
    private boolean isLogin;
    private boolean isForgotPassword = false;
    private String call;
    private Loader loader;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        loader = new Loader(this);
//        presenter.test();

        call = getIntent().getStringExtra("call");
        if (call == null)
            call = "";

        login = new Login();
        Intent intent = getIntent();
        if (intent != null && intent.getStringExtra("countryCode") != null) {
            etPhone.setText(intent.getStringExtra("phoneNumber"));
            countryCode = intent.getStringExtra("countryCode");
        } else {
            loadCurrentCountryCode();
        }
        applyFont();

        tbPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                etPassword.setTransformationMethod(!isChecked ? PasswordTransformationMethod.getInstance() : HideReturnsTransformationMethod.getInstance());
                etPassword.setSelection(etPassword.getText().toString().length());
            }
        });

        loginMode(etPhone.getText().toString().trim().isEmpty() ? 0 : 2);
    }

    @OnClick(R.id.btnForgotPassword)
    public void forgotPasswordButton() {
        if (login_mode == 0)
            isLogin = false;
        isForgotPassword = true;
        verifyPhone();
    }

    @OnClick(R.id.tvSend)
    public void send() {
        tvSend.setText(getString(R.string.TryAgain));
        tvSend.setEnabled(false);
        tvSend.setTextColor(getResources().getColor(R.color.star_grey));
//        presenter.requestOtp(etPhone.getText().toString(), countryCode);

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(() -> {
                    tvSend.setText(getString(R.string.send));
                    tvSend.setEnabled(true);
                    tvSend.setTextColor(getResources().getColor(R.color.base_color));
                });
            }
        }, 30000);
    }

    @OnClick(R.id.ivBack)
    public void back() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        if (!call.equals(getString(R.string.action_settings)))
            super.onBackPressed();
    }

    @SuppressLint("SetTextI18n")
    private void loadCurrentCountryCode() {
        String locale;
        TelephonyManager tm = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        String countryCodeValue = tm.getNetworkCountryIso();
        if (countryCodeValue == null || countryCodeValue.isEmpty()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                locale = getResources().getConfiguration().getLocales().get(0).getCountry().toUpperCase();
            } else {
                locale = getResources().getConfiguration().locale.getCountry().toUpperCase();
            }
        } else {
            locale = countryCodeValue.toUpperCase();
        }

        try {
            String allCountriesCode = readEncodedJsonString(this);
            JSONArray countryArray = new JSONArray(allCountriesCode);
            for (int i = 0; i < countryArray.length(); i++) {
                if (locale.equals(countryArray.getJSONObject(i).getString("code"))) {
                    countryCode = countryArray.getJSONObject(i).getString("dial_code");
                    country = countryArray.getJSONObject(i).getString("name");
//                    max_digits = countryArray.getJSONObject(i).getInt("max_digits");
                    etPhone.setText(getString(R.string.double_inverted_comma));
                    etPhone.setFilters(new InputFilter[]{new InputFilter.LengthFilter(max_digits)});
                    tvRegion.setText(country + " (" + countryCode + ")");
                    return;
                }
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.done)
    public void login() {
        String appVersion;
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
            appVersion = version;
        } catch (PackageManager.NameNotFoundException e) {
            appVersion = "123";
        }

        switch (login_mode) {
            case 0:
                if (etPhone.getText().toString().trim().isEmpty() || etPhone.getText().toString().trim().length() > max_digits)
                    message("Please enter valid mobile number");
                else if (etPassword.getText().toString().trim().isEmpty())
                    message("Please enter password");
                else if (countryCode != null && countryCode.isEmpty())
                    message("Please select region");
                else {
                    presenter.loginByPassword(countryCode, etPhone.getText().toString().trim(), etPassword.getText().toString().trim(), appVersion, country);
                }
                break;
            case 1:
                if (etAccount.getText().toString().trim().isEmpty())
                    message("Please enter DingDong ID");
                else if (etPassword.getText().toString().trim().isEmpty())
                    message("Please enter password");
                else {
                    presenter.loginByUserName(etAccount.getText().toString().trim(), etPassword.getText().toString().trim(), appVersion, country);
                }
                break;
            case 2:
                if (etPhone.getText().toString().trim().isEmpty())
                    message("Please enter valid mobile number");
                else if (countryCode == null || countryCode.isEmpty())
                    message("Please select region");
                else {
                    isLogin = true;
                    isForgotPassword = false;
                    verifyPhone();
                }
                break;
        }
    }

    @OnClick(R.id.tvRegion)
    public void selectRegion() {
        Intent intent = new Intent(LoginActivity.this, ChooseCountry.class);
        startActivityForResult(intent, 0);
    }


    @OnClick(R.id.loginByStarChatId)
    public void changeLoginMode() {
        if (login_mode == 1)
            loginMode(0);
        else
            loginMode(1);
    }

    @OnClick(R.id.loginSwitch)
    public void loginMode() {
        if (login_mode == 2)
            loginMode(0);
        else
            loginMode(2);
    }

    /**
     * @param login_mode : 0=phone number & password, 1=username & password , 2= phone number and otp
     */
    private void loginMode(int login_mode) {
        this.login_mode = login_mode;

        switch (login_mode) {
            case 0:
                done.setText(getString(R.string.login));
                llRegion.setVisibility(View.VISIBLE);
                llPhone.setVisibility(View.VISIBLE);
                llPassword.setVisibility(View.VISIBLE);
                llUserName.setVisibility(View.GONE);
                otpDivider.setVisibility(View.GONE);
                userNameDivider.setVisibility(View.GONE);
                phoneDivider.setVisibility(View.VISIBLE);
                passwordDivider.setVisibility(View.VISIBLE);
                btnForgotPassword.setVisibility(View.VISIBLE);

                loginSwitch.setText(getString(R.string.login_via_otp));
                loginSwitch.setVisibility(View.VISIBLE);
                loginByStarChatId.setVisibility(View.VISIBLE);
                etPhone.requestFocus();
                break;
            case 1:
                done.setText(getString(R.string.login));
                llRegion.setVisibility(View.GONE);
                llPhone.setVisibility(View.GONE);
                llPassword.setVisibility(View.VISIBLE);
                llUserName.setVisibility(View.VISIBLE);
                otpDivider.setVisibility(View.GONE);
                userNameDivider.setVisibility(View.VISIBLE);
                phoneDivider.setVisibility(View.GONE);
                passwordDivider.setVisibility(View.VISIBLE);
                btnForgotPassword.setVisibility(View.VISIBLE);

                loginSwitch.setText(getString(R.string.login_via_phone_no));
                loginSwitch.setVisibility(View.VISIBLE);
                loginByStarChatId.setVisibility(View.GONE);
                etAccount.requestFocus();
                break;
            case 2:
                done.setText(getString(R.string.next));
                llRegion.setVisibility(View.VISIBLE);
                llPhone.setVisibility(View.VISIBLE);
                llPassword.setVisibility(View.GONE);
                llUserName.setVisibility(View.GONE);
                otpDivider.setVisibility(View.VISIBLE);
                userNameDivider.setVisibility(View.GONE);
                phoneDivider.setVisibility(View.VISIBLE);
                passwordDivider.setVisibility(View.VISIBLE);
                btnForgotPassword.setVisibility(View.GONE);

                loginSwitch.setText(R.string.login_via_password);
                loginSwitch.setVisibility(View.VISIBLE);
                loginByStarChatId.setVisibility(View.VISIBLE);
                etPhone.requestFocus();
                break;
        }
    }

    @OnClick(R.id.tvSignUp)
    public void signUp() {
        Intent intent = new Intent(LoginActivity.this, WebActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("url", getResources().getString(R.string.privacyPolicyUrl));
        bundle.putString("title", getResources().getString(R.string.privacyPolicy));
        bundle.putString("action", "accept");
        startActivity(intent.putExtra("url_data", bundle));
    }

    private void applyFont() {
        tvTitle.setTypeface(typefaceManager.getSemiboldFont());
        textAccount.setTypeface(typefaceManager.getSemiboldFont());
        textRegion.setTypeface(typefaceManager.getSemiboldFont());
        textPhone.setTypeface(typefaceManager.getSemiboldFont());
        textPassword.setTypeface(typefaceManager.getSemiboldFont());
        textOtp.setTypeface(typefaceManager.getSemiboldFont());
        loginSwitch.setTypeface(typefaceManager.getSemiboldFont());
        tvNotMember.setTypeface(typefaceManager.getSemiboldFont());
        tvSignUp.setTypeface(typefaceManager.getSemiboldFont());
        tvSend.setTypeface(typefaceManager.getSemiboldFont());
        loginByStarChatId.setTypeface(typefaceManager.getSemiboldFont());
        btnForgotPassword.setTypeface(typefaceManager.getSemiboldFont());
        done.setTypeface(typefaceManager.getSemiboldFont());
        tvRegion.setTypeface(typefaceManager.getRegularFont());
        etAccount.setTypeface(typefaceManager.getRegularFont());
        etPhone.setTypeface(typefaceManager.getRegularFont());
        etPassword.setTypeface(typefaceManager.getRegularFont());
        etOtp.setTypeface(typefaceManager.getRegularFont());
    }

    @Override
    public void message(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void loginSuccessfully() {
        Intent i = new Intent(this, DiscoverActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.putExtra("caller", "SaveProfile");
        startActivity(i);
        finish();
    }

    @Override
    public void numberNotRegistered(String message) {
        if(!isFinishing()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(false);
            builder.setMessage(message + "\n\nDo you want to SignUp with this number ?");
            builder.setPositiveButton("SignUp", (dialog, which) ->
            {
                Intent intent = new Intent(LoginActivity.this, WebActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("url", getResources().getString(R.string.privacyPolicyUrl));
                bundle.putString("title", getResources().getString(R.string.privacyPolicy));
                bundle.putString("action", "accept");
                startActivity(intent.putExtra("url_data", bundle));
            });
            builder.setNegativeButton("Change", (dialog, which) -> dialog.cancel());
            builder.show();
        }
    }

    @Override
    public void blocked(String errorMessage) {
        AlertDialog.Builder confirm = new AlertDialog.Builder(this);
        confirm.setCancelable(false);
        confirm.setMessage(errorMessage);
        confirm.setPositiveButton(R.string.ok, (dialog, w) -> finish());
        confirm.create().show();
    }

    @Override
    public void verifyOtp(String phoneNumber, String countryCode) {

        Intent intent = new Intent(this, VerifyNumberOTPActivity.class);
        intent.putExtra("phoneNumber", phoneNumber);
        intent.putExtra("countryCode", countryCode);
        intent.putExtra("country", country);
        intent.putExtra("isLogin", isLogin);
        intent.putExtra("isForgotPassword", isForgotPassword);
        startActivity(intent);
    }

    @Override
    public void showLoader() {
        if (loader != null && loader.isShowing())
            loader.show();
    }

    @Override
    public void hideLoader() {
        if (loader != null && loader.isShowing())
            loader.hide();
    }

    @Override
    public void forgotPassword() {
        Intent intent1 = new Intent(this, ForgotPasswordActivity.class);
        intent1.putExtra("userName", etAccount.getText().toString());
        startActivity(intent1);
    }

    public void verifyPhone() {
        String phone = etPhone.getText().toString();
        if (verifyFields(phone, countryCode))
            presenter.verifyIsMobileRegistered(phone, countryCode, isForgotPassword);
//            presenter.requestOtp(phone, countryCode);
    }

    private boolean verifyFields(String phone, String countryCode) {

        if (countryCode == null || countryCode.isEmpty()) {
            message("Please select a country");
            return false;
        }

        if (phone == null || phone.isEmpty()) {
            message("Please enter a valid phone number");
            return false;
        }

        return true;
    }


    @SuppressLint("SetTextI18n")
    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if (requestCode == 0 && resultCode == RESULT_OK) {
            country = data.getStringExtra("MESSAGE");
            String code = data.getStringExtra("CODE");
            countryCode = "+" + code.substring(1);
//            max_digits = data.getIntExtra("MAX", 20);
            etPhone.setText(getString(R.string.double_inverted_comma));
            etPhone.setFilters(new InputFilter[]{new InputFilter.LengthFilter(max_digits)});
            tvRegion.setText(country + " (" + countryCode + ")");
        }
    }
}
