package chat.hola.com.app.authentication.forgotpassword;

public interface ForgotPasswordContract {

    interface View {
        void message(String message);

        void success();

        void progress(boolean b);

    }

    interface Presenter {
        void forgotPassword(String userName, String phone, String email);

        void requestOtp(String email);
    }
}
